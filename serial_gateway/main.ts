import * as Http       from 'http'
import * as Net        from 'net'
import * as SerialPort from 'serialport'
import Config          from './config'

const cmd            = require('command-line-args')
const config: Config = require('./config.json')

const cmd_params_def = [
    { name: 'mote_server_host',  alias: 'a', type: String, defaultValue: config.mote_server_host  },
    { name: 'mote_server_port',  alias: 'p', type: Number, defaultValue: config.mote_server_port  },
    { name: 'http_gateway_port', alias: 'g', type: Number, defaultValue: config.http_gateway_port },
    { name: 'serial_port',       alias: 'e', type: String, defaultValue: config.serial_port       },
    { name: 'serial_baud_rate',  alias: 'r', type: Number, defaultValue: config.serial_baud_rate  },
]
const options = cmd(cmd_params_def)


function log (msg: string) {
    let now = new Date()
    console.log(
        `{ ${now.toLocaleDateString()} ${now.toLocaleTimeString()} }  ${msg}`
    )
}


/**
 * Forward to the mote server a packet received from a mote
 *
 * @param packet packet received from a mote
 * @param socket connection to server
 */
function forward_to_server (packet: Buffer, socket: Net.Socket) {
    log(`forwarding ${packet.length} bytes of data to \u001b[1m\u001b[4mserver\u001b[0m\u001b[0m`)
    socket.write(packet)
}


/**
 * Forward a packet received from the server to the mote
 *
 * @param packet packet received from server
 * @param socket serial connection to mote
 */
function forward_to_mote (packet: Buffer, serial_conn: SerialPort) {
    log(`forwarding ${packet.length} bytes of data to \u001b[1m\u001b[4mmote\u001b[0m\u001b[0m`)
    serial_conn.write(packet, (error, bytes_written) => {
        if (error) {
            log(`failed to forward data to mote: ${error}`)
        } else {
            log(`forwarded ${bytes_written} to mote`)
        }
    })
}



let serial_conn = new SerialPort(options.serial_port, {
    baudRate: options.serial_baud_rate,
    lock    : false
})
log(`connected to mote @ serial port ${options.serial_port}`)
let socket = Net.createConnection({
    port: options.mote_server_port, host: options.mote_server_host
}, () => {
    log(`connected to mote server @ ${options.mote_server_host}:${options.mote_server_port}`)
})

socket.on('error', (error) => {
    log(`error in socket: ${error}`)
    process.exit(1)
})
socket.on('data', (data) => forward_to_mote(data, serial_conn))

serial_conn.on('error', (error) => {
    log(`error in serial connection: ${error}`)
    process.exit(1)
})
serial_conn.on('data', (data) => forward_to_server(data, socket))
