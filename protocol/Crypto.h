#ifndef __OURICO_CRYPTO_HEAD__
#define __OURICO_CRYPTO_HEAD__


#include <stdexcept>
#include <cstdlib>

#include <cryptopp/ccm.h>
#include <cryptopp/secblock.h>
#include <cryptopp/osrng.h>
#include <cryptopp/filters.h>
#include <cryptopp/aes.h>
#include <cryptopp/cryptlib.h>

#include "util/Logger.h"
#include "util/Misc.h"


namespace Crypto {


class Exception : public std::exception {
private:
    std::string err_msg;
public:
    Exception (std::string em) : err_msg(em) {};
    char const *what () const throw() {
        return err_msg.c_str();
    }
};

/**
 * Supported algorithms
 */
namespace Algorithm {
    extern std::string AES_CCM;
}

namespace {
    const size_t AES_CCM_MAC_SIZE    = 4;
    const size_t AES_CCM_IV_SIZE     = 12;
    const size_t AES_CCM_HEADER_SIZE = 2;
    const size_t AES_CCM_KEY_SIZE    = 16;
}


/**
 * Decrypt a packet using AES-CCM
 * Packet format:
 *
 * +--------+-------------------+-------------+
 * | header | encrypted payload | mac | nonce |
 * +--------+-------------------+-------------+
 *
 * @param packet     the packet to decrypt
 * @param packet_len length of `packet` in bytes (including packet and nonce)
 * @param key        key to decrypt
 *
 * @return the decrypted data
 */
std::string aes_ccm_decrypt (const char* packet, size_t packet_len, std::string key);



std::string aes_ccm_encrypt (const char* packet, size_t packet_len, std::string key);


};


#endif // __OURICO_CRYPTO_HEAD__
