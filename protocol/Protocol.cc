#include "Protocol.h"

const size_t Protocol::MOTE_PACKET_HEAD_LEN         = 6;
const size_t Protocol::MOTE_PACKET_LEN_FIELD_OFFSET = 4;
const size_t Protocol::MOTE_PACKET_LEN_FIELD_LEN    = 2;

const size_t Protocol::SERVER_PACKET_HEAD_LEN         = 6;
const size_t Protocol::SERVER_PACKET_LEN_FIELD_OFFSET = 4;
const size_t Protocol::SERVER_PACKET_LEN_FIELD_LEN    = 2;

const size_t Protocol::MAX_PACKET_LEN                 = 512;

using namespace std::string_literals;


const tsl::ordered_map<std::string, int> Protocol::HEADER_FIELDS  = {
    // mote id
    { "id",         2 },
    // packet number. necessary to sync acks and nacks
    { "packet_no",  1 },
    // packet type
    { "type",       1 },
    // packet length in bytes, excluding header
    { "packet_len", 2 },
    // last byte unused. necessary for encryption purposes
};

const int Protocol::ACK_PAYLOAD_LEN = 0;
const tsl::ordered_map<std::string, int> Protocol::ACK_FIELDS = {};
const int Protocol::NACK_PAYLOAD_LEN = 0;
const tsl::ordered_map<std::string, int> Protocol::NACK_FIELDS = {};

const static int ERROR_PACKET_PAYLOAD_LEN = 6;
const tsl::ordered_map<std::string, int> Protocol::ERROR_FIELDS = {
    { "date_time", 5 },
    { "err_code",  1 },
};

const tsl::ordered_map<std::string, int> Protocol::STATUS_REPORT_FIELDS = {
    { "start_date_time", 5 },
    { "end_date_time",   5 },
    { "nr_tx_packets",   2 },
    { "nr_lost_packets", 2 },
    { "nr_tx_bytes",     4 },
    { "nr_lost_bytes",   4 },
};

const tsl::ordered_map<std::string, int> Protocol::INIT_FIELDS = {
    { "current_ip", 4 },
};

const int Protocol::MEAS_PAYLOAD_NO_MEAS_LEN = 6;
const tsl::ordered_map<std::string, int> Protocol::MEASUREMENTS_FIELDS = {
    { "flags",          1 },
    { "date_time",      5 },
    // next 2 fields are repeated multiple times
    // { "interface",      1 }
    // { "measurement",   -1 },
};

// currently defined flags for measuremet packet
const tsl::ordered_map<std::string, int> Protocol::MEASUREMENTS_PACKET_FLAGS = {
    { "async", 0x01 },
};

const tsl::ordered_map<std::string, int> Protocol::CONF_CHANGE_FIELDS = {
    { "current_ip", 4 },
};

const int Protocol::ALERT_PAYLOAD_NO_MEAS_LEN = 9;
const tsl::ordered_map<std::string, int> Protocol::ALERT_FIELDS = {
    { "date_time",      5 },
    { "alert_num",      4 },
    // plus measuremet fields except flags and date_time
};

const int Protocol::TRIGGER_PAYLOAD_NO_MEAS_LEN = 15;
const tsl::ordered_map<std::string, int> Protocol::TRIGGER_FIELDS = {
    { "date_time",        5 },
    { "interface",        1 }, // actuator_interface
    { "action_num",       4 },
    { "result",           1 },
    { "trigger_num",      4 },
    // plus measuremet fields, except flags and date_time
};

const tsl::ordered_map<std::string, int> Protocol::ACTION_RESULT_FIELDS = {
    { "date_time",        5 },
    { "interface",        1 }, // actuator_interface
    { "action_num",       4 },
    { "result",           1 },
};


// payload len only for when not sending sensors/actuators to activate
const int Protocol::CONF_SET_PAYLOAD_LEN = 13;
const tsl::ordered_map<std::string, int> Protocol::CONF_SET_FIELDS = {
    { "measurement_period", 4 },
    { "send_data_period",   4 },
    { "report_period",      4 },
    { "comm_encrypt",       1 },
};


const int Protocol::ACTION_INSTR_PAYLOAD_LEN = 5;
const tsl::ordered_map<std::string, int> Protocol::ACTION_INSTR_FIELDS = {
    { "interface", 1 }, // actuator_interface
    { "action",    4 },
};

const int Protocol::ALERT_SET_PAYLOAD_LEN = 4;
const tsl::ordered_map<std::string, int> Protocol::ALERT_SET_FIELDS = {
    { "alert_num", 4 },
};

const int Protocol::ALERT_REMOVE_PAYLOAD_LEN = 4;
const tsl::ordered_map<std::string, int> Protocol::ALERT_REMOVE_FIELDS = {
    { "alert_num", 4 },
};

const int Protocol::TRIGGER_SET_PAYLOAD_LEN = 4;
const tsl::ordered_map<std::string, int> Protocol::TRIGGER_SET_FIELDS = {
    { "trigger_num", 4 },
};

const int Protocol::TRIGGER_REMOVE_PAYLOAD_LEN = 4;
const tsl::ordered_map<std::string, int> Protocol::TRIGGER_REMOVE_FIELDS = {
    { "trigger_num", 4 },
};

const int Protocol::SENSOR_ACTIVATE_PAYLOAD_LEN = 1;
const tsl::ordered_map<std::string, int> Protocol::SENSOR_ACTIVATE_FIELDS = {
    { "interface", 1 }, // sensor_interface
};

const int Protocol::SENSOR_DEACTIVATE_PAYLOAD_LEN = 1;
const tsl::ordered_map<std::string, int> Protocol::SENSOR_DEACTIVATE_FIELDS = {
    { "interface", 1 }, // sensor_interface
};

const int Protocol::ACTUATOR_ACTIVATE_PAYLOAD_LEN = 1;
const tsl::ordered_map<std::string, int> Protocol::ACTUATOR_ACTIVATE_FIELDS = {
    { "interface", 1 }, // actuator_interface
};

const int Protocol::ACTUATOR_DEACTIVATE_PAYLOAD_LEN = 1;
const tsl::ordered_map<std::string, int> Protocol::ACTUATOR_DEACTIVATE_FIELDS = {
    { "interface", 1 }, // actuator_interface
};

const int Protocol::REQUEST_STATUS_REPORT_PAYLOAD_LEN = 0;
const tsl::ordered_map<std::string, int> Protocol::REQUEST_STATUS_REPORT_FIELDS = {};
const int Protocol::REQUEST_MEASUREMENTS_PAYLOAD_LEN = 0;
const tsl::ordered_map<std::string, int> Protocol::REQUEST_MEASUREMENTS_FIELDS = {};
const int Protocol::RESET_PAYLOAD_LEN = 0;
const tsl::ordered_map<std::string, int> Protocol::RESET_FIELDS = {};


Protocol::Protocol () { }

Protocol::Packet Protocol::parse_mote_packet (const char *packet, size_t packet_len) {
    Packet pt;
    parse_mote_packet(packet, packet_len, pt);
    return pt;
}

void Protocol::parse_mote_packet (const char* packet, size_t packet_len, Packet& pt) {

    if (packet == NULL)
        throw std::runtime_error("[Protocol] Null packet");

    if (packet_len < MOTE_PACKET_HEAD_LEN)
        throw std::runtime_error(
            "[Protocol] Packet should have at least "s +
            std::to_string(MOTE_PACKET_HEAD_LEN) + " bytes"
        );

    std::string encrypted = decrypt_packet(packet, packet_len);
    const char* __packet = encrypted.data();
    size_t __packet_len = encrypted.size();

    extract_mote_header(packet, pt);

#ifdef DEBUG
    Logger::debug(
        "[Protocol] Packet { mote_id =  " + std::to_string(pt.mote_id) +
        ", type = " + packet_types_str[pt.type] +
        ", payload_len = " + std::to_string(pt.payload_len) + " }"
    );
#endif

    switch (pt.type) {
    case ACK: case NACK:
        // no fiels in ack packet
        break;

    case ERROR:
        extract_fields(__packet, __packet_len, ERROR_FIELDS, pt);
        break;

    case MEASUREMENTS:
        // extract_measurement_fields(__packet, __packet_len, pt);
        extract_fields(__packet, __packet_len, MEASUREMENTS_FIELDS, pt);
        extract_measurements(
            __packet, __packet_len,
            MOTE_PACKET_HEAD_LEN + MEAS_PAYLOAD_NO_MEAS_LEN,
            pt
        );
        break;

    case STATUS_REPORT:
        extract_fields(__packet, __packet_len, STATUS_REPORT_FIELDS, pt);
        break;

    case ALERT:
        extract_fields(__packet, __packet_len, ALERT_FIELDS, pt);
        extract_measurements(
            __packet, __packet_len,
            MOTE_PACKET_HEAD_LEN + ALERT_PAYLOAD_NO_MEAS_LEN,
            pt);
        break;

    case TRIGGER:
        extract_fields(__packet, __packet_len, TRIGGER_FIELDS, pt);
        extract_measurements(
            __packet, __packet_len,
             MOTE_PACKET_HEAD_LEN + TRIGGER_PAYLOAD_NO_MEAS_LEN,
             pt);
        break;

    case ACTION_RESULT:
        extract_fields(
            __packet, __packet_len, ACTION_RESULT_FIELDS, pt);
        break;

    case INIT:
        extract_fields(__packet, __packet_len, INIT_FIELDS, pt);
        break;

    case CONF_CHANGE:
        extract_fields(__packet, __packet_len, CONF_CHANGE_FIELDS, pt);
        break;

    case CONF_SET:
        extract_fields(__packet, __packet_len, CONF_SET_FIELDS, pt);
        break;

    case ACTION_INSTR:
        extract_fields(__packet, __packet_len, ACTION_INSTR_FIELDS, pt);
        break;

    case ALERT_SET:
        extract_fields(__packet, __packet_len, ALERT_SET_FIELDS, pt);
        break;

    case ALERT_REMOVE:
        extract_fields(__packet, __packet_len, ALERT_REMOVE_FIELDS, pt);
        break;

    case TRIGGER_SET:
        extract_fields(__packet, __packet_len, TRIGGER_SET_FIELDS, pt);
        break;

    case TRIGGER_REMOVE:
        extract_fields(__packet, __packet_len, TRIGGER_REMOVE_FIELDS, pt);
        break;

    case SENSOR_ACTIVATE:
        extract_fields(__packet, __packet_len, SENSOR_ACTIVATE_FIELDS, pt);
        break;

    case SENSOR_DEACTIVATE:
        extract_fields(__packet, __packet_len, SENSOR_DEACTIVATE_FIELDS, pt);
        break;

    case ACTUATOR_ACTIVATE:
        extract_fields(__packet, __packet_len, ACTUATOR_ACTIVATE_FIELDS, pt);
        break;

    case ACTUATOR_DEACTIVATE:
        extract_fields(__packet, __packet_len, ACTUATOR_DEACTIVATE_FIELDS, pt);
        break;

    case RESET:
        extract_fields(__packet, __packet_len, RESET_FIELDS, pt);
        break;

    default:
        throw std::runtime_error(
            "[Protocol] Undefined parsing for mote packet of type "s +
            packet_types_str[pt.type]
        );
    }
}

void Protocol::extract_mote_header (const char *packet, Packet& pt) {
    pt.mote_id = 0;
    pt.payload_len = 0;
    // first two bytes of mote packet are the id
    pt.mote_id += (0xff & packet[0]) << 8;
    pt.mote_id += (0xff & packet[1]);

    pt.nr = (0xff & packet[2]);

    // third byte is the packet type
    uint pt_type = (0xff & packet[3]);
    if (pt_type >= PacketType::NUMBER_OF_PACKET_TYPES)
        throw std::runtime_error(
            "[Protocol] Invalid packet type received: " + std::to_string(pt_type)
        );
    pt.type = static_cast<PacketType>(pt_type);

    // fourth and fifth bytes tell the packet length, excluding header
    pt.payload_len += (0xff & packet[4]) << 8;
    pt.payload_len += (0xff & packet[5]);
}

void Protocol::extract_fields (
    const char *packet,
    size_t packet_len,
    const tsl::ordered_map<std::string, int> fields,
    Packet& pt
) {

    int pos = MOTE_PACKET_HEAD_LEN;
    for (auto const& fl : fields) {
        int nr_bytes = fl.second;
        std::string field_name = fl.first;

        if (nr_bytes < 1) // probably -1
            throw std::runtime_error(
                "[Protocol] extract_fields called on field of variable size: "s
                + field_name + ", " + std::to_string(pos+1) + "th field"
            );
        if (pos + nr_bytes > packet_len)
            throw std::runtime_error(
                "[Protocol] Packet size is incompatible with its type. "s +
                "Less than expected"
            );

        std::vector<char> field_value(nr_bytes);
        for (int b = 0; b < nr_bytes; b++) {
            field_value[b] = packet[pos+b];
        }

        pos += nr_bytes;

        pt.fields.insert(std::pair<std::string, boost::any>(
            field_name, field_value
        ));
    }
}


void Protocol::extract_measurements (
    const char *packet,
    size_t packet_len,
    int from,
    Packet& pt
) {

    std::string out_size = "out_size";
    std::string sql_to_out_size =
        // get the output_size of the sensor in this mote
        // installed in the interface indicated
        "select sensor.output_size, sensor_use.num, sensor.id from "s +
        "(sensor_use join sensor on sensor.id = sensor_use.sensor_id) "s +
        " where sensor_use.mote_id = "s + std::to_string(pt.mote_id) +
        " and sensor_use.interface_num = $1";
    db_con.prepare(out_size, sql_to_out_size);


    int int_num_len = 1; // number of bytes used for interface number
    // sensor_id, sensor_use_num and measuremet tuples
    std::vector<std::tuple<int, int, std::vector<char>>> measurements;

    for (int i = from; i < packet_len;) {
        // measurements come in groups of
        // interface_num|measuremet,
        // where the interface_num is the origin of the measuremet.
        // we get, from the database, the output_size of the
        // sensor that is installed
        // in that interface and then read output_size bytes
        // as the measuremet

        // get the interface number
        int interface = 0;
#ifdef DEBUG
        Logger::debug(
            "[Protocol] starting to read new measurement at position " + std::to_string(i)
        );
#endif
        for (int k = 0; k < int_num_len; k++) {
            interface += ((0xff & packet[i+k]) << (8*(int_num_len-k-1)));
        }
        i += int_num_len;

        pqxx::result r = db_con.exec_prepared(out_size, interface);
        if (r.size() == 0)
            throw std::runtime_error(
                "[Protocol] No sensor was found in interface "s +
                std::to_string(interface)
            );

        if (r.size() != 1)
            Logger::warning(
                "[Protocol] More than one sensor found in interface "s +
                std::to_string(interface)
            );

        int output_size = r[0][0].as<int>();
        int sns_use_num = r[0][1].as<int>();
        int sensor_id   = r[0][2].as<int>();

#ifdef DEBUG
        Logger::debug(
            "[Protocol] sensor_use = " + std::to_string(sns_use_num) + ", " +
            "interface = " + std::to_string(interface) + ", " +
            "sensor_id = " + std::to_string(sensor_id) + ", " +
            "output_size = " + std::to_string(output_size)
        );
#endif

        if (packet_len < i + output_size)
            throw std::runtime_error(
                "[Protocol] Sensor measurement size less than expeceted. "s +
                "For sensor_use.id = "s + std::to_string(sensor_id) +
                ". Expected "s + std::to_string(output_size) + " bytes"
            );

        std::vector<char> ms(output_size);
        for (int k = 0; k < output_size; k++) {
            ms[k] = packet[i+k];
        }
        measurements.push_back(std::tuple<int, int, std::vector<char>>(
            sns_use_num, sensor_id, ms
        ));
        i += output_size;
    }

    pt.fields.insert(std::pair<std::string, boost::any>(
        "measurements", measurements
    ));
}

std::string Protocol::create_server_packet (
    int mote_id,
    Protocol::PacketType type,
    std::map<std::string, std::vector<char>> fields,
    int packet_no
) {


    size_t packet_len = SERVER_PACKET_HEAD_LEN;
    char *packet;
    size_t buffer_len;

    switch (type) {

    case ACK: case NACK:
        packet = (char*)malloc(SERVER_PACKET_HEAD_LEN);
        // nothing to do
        break;

    case ERROR:
        packet = (char*)malloc(SERVER_PACKET_HEAD_LEN + ERROR_PACKET_PAYLOAD_LEN);
        buffer_len = SERVER_PACKET_HEAD_LEN + ERROR_PACKET_PAYLOAD_LEN;
        packet_len += insert_fields(
            type, fields, ERROR_FIELDS, packet, buffer_len
        );
        break;

    case CONF_SET:
        packet = (char*)malloc(SERVER_PACKET_HEAD_LEN + CONF_SET_PAYLOAD_LEN);
        buffer_len = SERVER_PACKET_HEAD_LEN + CONF_SET_PAYLOAD_LEN;
        packet_len += insert_fields(
            type, fields, CONF_SET_FIELDS, packet, buffer_len
        );
        break;

    case ACTION_INSTR:
        packet = (char*)malloc(SERVER_PACKET_HEAD_LEN + ACTION_INSTR_PAYLOAD_LEN);
        buffer_len = SERVER_PACKET_HEAD_LEN + ACTION_INSTR_PAYLOAD_LEN;
        packet_len += insert_fields(
            type, fields, ACTION_INSTR_FIELDS, packet, buffer_len
        );
        break;

    case REQUEST_STATUS_REPORT:
        packet = (char*)malloc(SERVER_PACKET_HEAD_LEN + REQUEST_STATUS_REPORT_PAYLOAD_LEN);
        buffer_len = SERVER_PACKET_HEAD_LEN + REQUEST_STATUS_REPORT_PAYLOAD_LEN;
        packet_len += insert_fields(
            type, fields, REQUEST_STATUS_REPORT_FIELDS, packet, buffer_len
        );
        break;

    case REQUEST_MEASUREMENTS:
        packet = (char*)malloc(SERVER_PACKET_HEAD_LEN + REQUEST_STATUS_REPORT_PAYLOAD_LEN);
        buffer_len = SERVER_PACKET_HEAD_LEN + REQUEST_MEASUREMENTS_PAYLOAD_LEN;
        packet_len += insert_fields(
            type, fields, REQUEST_MEASUREMENTS_FIELDS, packet, buffer_len
        );
        break;

    case ALERT_SET:
        packet = (char*)malloc(SERVER_PACKET_HEAD_LEN + ALERT_SET_PAYLOAD_LEN);
        buffer_len = SERVER_PACKET_HEAD_LEN + ALERT_SET_PAYLOAD_LEN;
        packet_len += insert_fields(
            type, fields, ALERT_SET_FIELDS, packet, buffer_len
        );
        break;

    case ALERT_REMOVE:
        packet = (char*)malloc(SERVER_PACKET_HEAD_LEN + ALERT_REMOVE_PAYLOAD_LEN);
        buffer_len = SERVER_PACKET_HEAD_LEN + ALERT_REMOVE_PAYLOAD_LEN;
        packet_len += insert_fields(
            type, fields, ALERT_REMOVE_FIELDS, packet, buffer_len
        );
        break;

    case TRIGGER_SET:
        packet = (char*)malloc(SERVER_PACKET_HEAD_LEN + TRIGGER_SET_PAYLOAD_LEN);
        buffer_len = SERVER_PACKET_HEAD_LEN + TRIGGER_SET_PAYLOAD_LEN;
        packet_len += insert_fields(
            type, fields, TRIGGER_SET_FIELDS, packet, buffer_len
        );
        break;

    case TRIGGER_REMOVE:
        packet = (char*)malloc(SERVER_PACKET_HEAD_LEN + TRIGGER_REMOVE_PAYLOAD_LEN);
        buffer_len = SERVER_PACKET_HEAD_LEN + TRIGGER_REMOVE_PAYLOAD_LEN;
        packet_len += insert_fields(
            type, fields, TRIGGER_REMOVE_FIELDS, packet, buffer_len
        );
        break;

    case SENSOR_ACTIVATE:
        packet = (char*)malloc(SERVER_PACKET_HEAD_LEN + SENSOR_ACTIVATE_PAYLOAD_LEN);
        buffer_len = SERVER_PACKET_HEAD_LEN + SENSOR_ACTIVATE_PAYLOAD_LEN;
        packet_len += insert_fields(
            type, fields, SENSOR_ACTIVATE_FIELDS, packet, buffer_len
        );
        break;

    case SENSOR_DEACTIVATE:
        packet = (char*)malloc(SERVER_PACKET_HEAD_LEN + SENSOR_DEACTIVATE_PAYLOAD_LEN);
        buffer_len = SERVER_PACKET_HEAD_LEN + SENSOR_DEACTIVATE_PAYLOAD_LEN;
        packet_len += insert_fields(
            type, fields, SENSOR_DEACTIVATE_FIELDS, packet, buffer_len
        );
        break;

    case ACTUATOR_ACTIVATE:
        packet = (char*)malloc(SERVER_PACKET_HEAD_LEN + ACTUATOR_ACTIVATE_PAYLOAD_LEN);
        buffer_len = SERVER_PACKET_HEAD_LEN + ACTUATOR_ACTIVATE_PAYLOAD_LEN;
        packet_len += insert_fields(
            type, fields, ACTUATOR_ACTIVATE_FIELDS, packet, buffer_len
        );
        break;

    case ACTUATOR_DEACTIVATE:
        packet = (char*)malloc(SERVER_PACKET_HEAD_LEN + ACTUATOR_DEACTIVATE_PAYLOAD_LEN);
        buffer_len = SERVER_PACKET_HEAD_LEN + ACTUATOR_DEACTIVATE_PAYLOAD_LEN;
        packet_len += insert_fields(
            type, fields, ACTUATOR_DEACTIVATE_FIELDS, packet, buffer_len
        );
        break;

    case RESET:
        packet = (char*)malloc(SERVER_PACKET_HEAD_LEN + RESET_PAYLOAD_LEN);
        buffer_len = SERVER_PACKET_HEAD_LEN + RESET_PAYLOAD_LEN;
        packet_len += insert_fields(
            type, fields, RESET_FIELDS, packet, buffer_len
        );
        break;

    default:
        throw std::runtime_error(
            "Unable to create server packet of type "s +
            packet_types_str[type]
        );
    }

    size_t plen = packet_len - SERVER_PACKET_HEAD_LEN;
    packet[0] = 0xff & (mote_id >> 8);
    packet[1] = 0xff & (mote_id);
    packet[2] = 0xff & packet_no;
    packet[3] = 0xff & type;
    packet[4] = 0xff & (plen >> 8);
    packet[5] = 0xff & (plen);

    std::string encrypted = encrypt_packet(mote_id, packet, packet_len);
    free(packet);
    return encrypted;
}


std::string Protocol::create_server_ack (int mote_id, int packet_no) {
    return create_server_packet(
        mote_id,
        PacketType::ACK,
        { },
        packet_no
    );
}

std::string Protocol::create_server_nack (int mote_id, int packet_no) {
    return create_server_packet(
        mote_id,
        PacketType::NACK,
        { },
        packet_no
    );
}


size_t Protocol::insert_fields (
    PacketType type,
    std::map<std::string, std::vector<char>> input_fields,
    const tsl::ordered_map<std::string, int> fields,
    char *packet,
    size_t buffer_len
) {

    using namespace std::string_literals;

    int pos = SERVER_PACKET_HEAD_LEN;
    for (auto& field : fields) {
        std::string field_name = field.first;
        int nr_bytes           = field.second;

        if (pos + nr_bytes > MAX_PACKET_LEN)
            throw std::runtime_error(
                "[Protocol] Maximum packet size (" +
                std::to_string(MAX_PACKET_LEN) + ") exceeded"
            );

        // check if the field was passed
        if (input_fields.find(field_name) == input_fields.end())
            throw std::runtime_error(
                "[Protocol] Field " + field_name + " is needed to " +
                "construct packet of type " + packet_types_str[type]
            );

        std::vector<char> bytes = input_fields.at(field_name);
        // check size of bytes passed
        if (nr_bytes != bytes.size())
            throw std::runtime_error(
                "[Protocol] Field " + field_name + " needs " +
                std::to_string(nr_bytes) + ". " + std::to_string(bytes.size()) +
                " were given"
            );

        if (buffer_len < pos + nr_bytes)
            throw std::runtime_error(
                "[Protocol] buffer size is not enough to hold packet"
            );

        for (char b : bytes)
            packet[pos++] = b;

    }

    return pos - SERVER_PACKET_HEAD_LEN;
}


std::string Protocol::decrypt_packet (const char* packet, size_t packet_len) {
    using namespace std::string_literals;
    int mote_id = 0;
    mote_id += (0xff & packet[0]) << 8;
    mote_id += (0xff & packet[1]);
    std::string sql = "select use_crypto from mote_configuration where "s +
        "active = true and mote_id = " + std::to_string(mote_id);

    pqxx::result r = db_con.exec_sql(sql);

    if (r.size() == 0) {
        throw std::runtime_error(
            "[Protocol] failed to decrypt packet. Mote has no active configuration"
        );
    }

    if (r[0][0].as<bool>() == false) {
        Logger::log(
            "[Protocol] no decryption for packet from mote " + std::to_string(mote_id)
        );
        return std::string(packet, packet_len);
    }

    Logger::log("[Protocol] decryption for packet from mote " + std::to_string(mote_id));

    sql = "select crypto_alg, crypto_key from mote where id = " + std::to_string(mote_id);
    r = db_con.exec_sql(sql);
    if (r.size() == 0) {
        throw std::runtime_error(
            "[Protocol] failed to read crypto alg for mote " + std::to_string(mote_id)
        );
    }

    std::string alg = r[0][0].as<std::string>();
    std::string key = r[0][1].as<std::string>();

    if (alg == Crypto::Algorithm::AES_CCM) {
        return Crypto::aes_ccm_decrypt(packet, packet_len, key);
    } else {
        throw std::runtime_error("[Protocol] unsupported crypto alg " + alg);
    }
}


std::string Protocol::encrypt_packet (int mote_id, const char* packet, size_t packet_len) {
    using namespace std::string_literals;

    std::string sql = "select use_crypto from mote_configuration where "s +
        "active = true and mote_id = " + std::to_string(mote_id);

    pqxx::result r = db_con.exec_sql(sql);

    if (r.size() == 0) {
        throw std::runtime_error(
            "[Protocol] failed to encrypt packet. Mote has no active configuration"
        );
    }

    if (r[0][0].as<bool>() == false) {
        Logger::log(
            "[Protocol] no encryption for packet from mote " + std::to_string(mote_id)
        );
        return std::string(packet, packet_len);
    }

    Logger::log("[Protocol] encryption for packet from mote " + std::to_string(mote_id));

    sql = "select crypto_alg, crypto_key from mote where id = " + std::to_string(mote_id);
    r = db_con.exec_sql(sql);
    if (r.size() == 0) {
        throw std::runtime_error(
            "[Protocol] failed to read crypto alg for mote " + std::to_string(mote_id)
        );
    }

    std::string alg = r[0][0].as<std::string>();
    std::string key = r[0][1].as<std::string>();

    if (alg == Crypto::Algorithm::AES_CCM) {
        return Crypto::aes_ccm_encrypt(packet, packet_len, key);
    } else {
        throw std::runtime_error("[Protocol] unsupported crypto alg " + alg);
    }
}
