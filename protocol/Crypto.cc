#include "Crypto.h"


std::string Crypto::Algorithm::AES_CCM = "AES-CCM";

std::string Crypto::aes_ccm_decrypt
(const char* packet, size_t packet_len, std::string key) {

    // there should be a header, a mac, a nonce and at least one byte of payload
    if (packet_len < AES_CCM_HEADER_SIZE + AES_CCM_MAC_SIZE + AES_CCM_IV_SIZE + 1) {
        throw Exception("[Crypto] packet length smaller than header+iv+mac+1");
    }
    if (key.size() != AES_CCM_KEY_SIZE) {
        throw Exception(
            "[Crypto] key must be " + std::to_string(AES_CCM_KEY_SIZE) + "B long"
        );
    }

    CryptoPP::CCM<CryptoPP::AES, AES_CCM_MAC_SIZE>::Decryption decrypt;
    CryptoPP::SecByteBlock _key(
        reinterpret_cast<const CryptoPP::byte*>(&key[0]), key.size()
    );

    size_t payload_size =
        packet_len - AES_CCM_HEADER_SIZE - AES_CCM_IV_SIZE - AES_CCM_MAC_SIZE;

    const CryptoPP::byte* header  =
        reinterpret_cast<const CryptoPP::byte*>(packet);
    const CryptoPP::byte* payload =
        reinterpret_cast<const CryptoPP::byte*>(&packet[AES_CCM_HEADER_SIZE]);
    const CryptoPP::byte* iv      =
        reinterpret_cast<const CryptoPP::byte*>(&packet[packet_len - AES_CCM_IV_SIZE]);
    const CryptoPP::byte* mac     =
        reinterpret_cast<const CryptoPP::byte*>(
            &packet[packet_len - AES_CCM_IV_SIZE - AES_CCM_MAC_SIZE]
        );

#ifdef DEBUG
    Logger::debug("[Crypto] AES CCM Decrypt");
    Logger::debug("[Crypto] Header:", Logger::Modifier::bold);
    Misc::print_bytes((const char*)header, AES_CCM_HEADER_SIZE);
    Logger::debug("[Crypto] Payload:", Logger::Modifier::bold);
    Misc::print_bytes((const char*)payload, payload_size);
    Logger::debug("[Crypto] Mac:");
    Misc::print_bytes((const char*)mac, AES_CCM_MAC_SIZE);
    Logger::debug("[Crypto] Iv:");
    Misc::print_bytes((const char*)iv, AES_CCM_IV_SIZE);
#endif

    decrypt.SetKeyWithIV(_key, _key.size(), iv, AES_CCM_IV_SIZE);
    decrypt.SpecifyDataLengths(AES_CCM_HEADER_SIZE, payload_size, 0);


    CryptoPP::AuthenticatedDecryptionFilter decrypt_filter(
        decrypt, NULL,
        CryptoPP::AuthenticatedDecryptionFilter::MAC_AT_BEGIN   |
        CryptoPP::AuthenticatedDecryptionFilter::THROW_EXCEPTION
    );

    decrypt_filter.ChannelPut(CryptoPP::DEFAULT_CHANNEL, mac, AES_CCM_MAC_SIZE);
    decrypt_filter.ChannelPut(CryptoPP::AAD_CHANNEL, header, AES_CCM_HEADER_SIZE);
    decrypt_filter.ChannelPut(CryptoPP::DEFAULT_CHANNEL, payload, payload_size);

    decrypt_filter.ChannelMessageEnd(CryptoPP::AAD_CHANNEL);
    decrypt_filter.ChannelMessageEnd(CryptoPP::DEFAULT_CHANNEL);

    if (!decrypt_filter.GetLastResult())
        throw Exception("[Crypto] get last result failed");

    decrypt_filter.SetRetrievalChannel(CryptoPP::DEFAULT_CHANNEL);
    size_t plaintext_size = decrypt_filter.MaxRetrievable();
    if (plaintext_size <= 0)
        throw Exception("[Crypto] no plaintext to retrieve");

    std::string plaintext_packet;
    plaintext_packet.resize(AES_CCM_HEADER_SIZE + plaintext_size);
    for (int i = 0; i < AES_CCM_HEADER_SIZE; i++) {
        plaintext_packet[i] = header[i];
    }
    decrypt_filter.Get(
        reinterpret_cast<CryptoPP::byte*>(&plaintext_packet[AES_CCM_HEADER_SIZE]),
        plaintext_size
    );

#ifdef DEBUG
    Logger::debug("[Crypto] Plain Text:", Logger::Modifier::bold);
    Misc::print_bytes(plaintext_packet.data(), plaintext_packet.size());
#endif

    return plaintext_packet;
}



std::string Crypto::aes_ccm_encrypt
(const char* packet, size_t packet_len, std::string key) {

    // there should be a header and at least one byte of payload
    if (packet_len < AES_CCM_HEADER_SIZE + 1)
        throw Exception("[Crypto] packet length smaller that header+1");

    if (key.size() != AES_CCM_KEY_SIZE) {
        throw Exception(
            "[Crypto] key must be " + std::to_string(AES_CCM_KEY_SIZE) + "B long"
        );
    }

    CryptoPP::AutoSeededRandomPool rng;
    CryptoPP::CCM<CryptoPP::AES, AES_CCM_MAC_SIZE>::Encryption encrypt;

    CryptoPP::byte iv[AES_CCM_IV_SIZE];
    rng.GenerateBlock(iv, AES_CCM_IV_SIZE);
    CryptoPP::SecByteBlock _key(
        reinterpret_cast<const CryptoPP::byte*>(&key[0]), key.size()
    );

#ifdef DEBUG
    Logger::debug("[Crypto] AES CCM Encrypt");
    Logger::debug("[Crypto] Input:", Logger::Modifier::bold);
    Misc::print_bytes(packet, packet_len);
    Logger::debug("[Crypto] Generated Iv:");
    Misc::print_bytes(reinterpret_cast<char*>(iv), AES_CCM_IV_SIZE);
#endif

    size_t payload_len = packet_len - AES_CCM_HEADER_SIZE;
    std::string result;
    const CryptoPP::byte* header  =
        reinterpret_cast<const CryptoPP::byte*>(packet);
    const CryptoPP::byte* payload =
        reinterpret_cast<const CryptoPP::byte*>(&packet[AES_CCM_HEADER_SIZE]);

    encrypt.SetKeyWithIV(_key, _key.size(), iv, AES_CCM_IV_SIZE);
    encrypt.SpecifyDataLengths(AES_CCM_HEADER_SIZE, payload_len);

    CryptoPP::AuthenticatedEncryptionFilter encrypt_filter(
        encrypt,
        new CryptoPP::StringSink(result)
    );

    encrypt_filter.ChannelPut(CryptoPP::AAD_CHANNEL, header, AES_CCM_HEADER_SIZE);
    encrypt_filter.ChannelMessageEnd(CryptoPP::AAD_CHANNEL);

    encrypt_filter.ChannelPut(CryptoPP::DEFAULT_CHANNEL, payload, payload_len);
    encrypt_filter.ChannelMessageEnd(CryptoPP::DEFAULT_CHANNEL);

    std::string final_result;
    final_result.resize(AES_CCM_HEADER_SIZE + payload_len + AES_CCM_MAC_SIZE + AES_CCM_IV_SIZE);
    memcpy(&final_result[0], reinterpret_cast<const char*>(header), AES_CCM_HEADER_SIZE);
    memcpy(&final_result[AES_CCM_HEADER_SIZE], &result[0], result.size());
    memcpy(
        &final_result[AES_CCM_HEADER_SIZE + AES_CCM_MAC_SIZE + payload_len],
        reinterpret_cast<char*>(iv),
        AES_CCM_IV_SIZE
    );

#ifdef DEBUG
    Logger::debug("[Crypto] Result:", Logger::Modifier::bold);
    Misc::print_bytes(&final_result[0], final_result.size());
#endif

    return final_result;
}
