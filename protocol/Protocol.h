#ifndef __PROTOCOL_HEAD__
#define __PROTOCOL_HEAD__


#include <map>
#include <string>
#include <stdexcept>
#include <vector>
#include <tuple>
#include <cstddef>

#include <pqxx/pqxx>
#include <boost/any.hpp>
#include <ext_lib/tsl/ordered_map.h>

#include "db/DB.h"
#include "db/MoteConfiguration.h"

#include "Crypto.h"

#include "util/Logger.h"


/**
 * To manipulate the protocol packets
 */
class Protocol {


public:

    DB db_con;

    // length of header in packets sent from mote
    const static size_t MOTE_PACKET_HEAD_LEN;
    // position of the length field in the header
    const static size_t MOTE_PACKET_LEN_FIELD_OFFSET;
    // number of bytes dedicated to the length field
    const static size_t MOTE_PACKET_LEN_FIELD_LEN;

    // length of header in packets sent by server
    const static size_t SERVER_PACKET_HEAD_LEN;
    // position of the length field in the header
    const static size_t SERVER_PACKET_LEN_FIELD_OFFSET;
    // number of bytes dedicated to the length field
    const static size_t SERVER_PACKET_LEN_FIELD_LEN;


    // maximum packet length
    const static size_t MAX_PACKET_LEN;


    enum PacketType {
        ACK                   = 0,
        NACK                  = 1,
        ERROR                 = 2,

        MEASUREMENTS          = 3, // send last measurements
        STATUS_REPORT         = 4, // send status report
        ACTION_RESULT         = 5, // return results of action
        INIT                  = 6, // intialize mote
        CONF_CHANGE           = 7, // change configuration
        ALERT                 = 8,
        TRIGGER               = 9,

        CONF_SET              = 10, // set configuration
        ACTION_INSTR          = 11, // instructions to perform action
        REQUEST_STATUS_REPORT = 12,
        REQUEST_MEASUREMENTS  = 13,
        ALERT_SET             = 14, // set an alert
        ALERT_REMOVE          = 15, // deactivate an alert
        TRIGGER_SET           = 16, // set a trigger
        TRIGGER_REMOVE        = 17, // deactivate a trigger
        SENSOR_ACTIVATE       = 18,
        SENSOR_DEACTIVATE     = 19,
        ACTUATOR_ACTIVATE     = 20,
        ACTUATOR_DEACTIVATE   = 21,
        RESET                 = 22, // reset mote,

        NUMBER_OF_PACKET_TYPES
    };

    const std::string packet_types_str[PacketType::NUMBER_OF_PACKET_TYPES] = {
        "ACK",
        "NACK",
        "ERROR",
        "MEASUREMENTS",
        "STATUS_REPORT",
        "ACTION_RESULT",
        "INIT",
        "CONF_CHANGE",
        "ALERT",
        "TRIGGER",

        "CONF_SET",
        "ACTION_INSTR",
        "REQUEST_STATUS_REPORT",
        "REQUEST_MEASUREMENTS",
        "ALERT_SET",
        "ALERT_REMOVE",
        "TRIGGER_SET",
        "TRIGGER_REMOVE",
        "SENSOR_ACTIVATE",
        "SENSOR_DEACTIVATE",
        "ACTUATOR_ACTIVATE",
        "ACTUATOR_DEACTIVATE",
        "RESET",
    };

    const static tsl::ordered_map<std::string, int> HEADER_FIELDS;
    const static int ACK_PAYLOAD_LEN;
    const static tsl::ordered_map<std::string, int> ACK_FIELDS;
    const static int NACK_PAYLOAD_LEN;
    const static tsl::ordered_map<std::string, int> NACK_FIELDS;
    // const static int ERROR_PACKET_LEN;
    const static tsl::ordered_map<std::string, int> ERROR_FIELDS;

    // length of the measurements packet payload
    // without taking in account
    // the data fields
    const static int MEAS_PAYLOAD_NO_MEAS_LEN;
    const static tsl::ordered_map<std::string, int> MEASUREMENTS_FIELDS;

    const static tsl::ordered_map<std::string, int> STATUS_REPORT_FIELDS;
    const static tsl::ordered_map<std::string, int> INIT_FIELDS;
    const static tsl::ordered_map<std::string, int> CONF_CHANGE_FIELDS;

    // length of the alert payload without taking in account
    // the measuremet fields
    const static int ALERT_PAYLOAD_NO_MEAS_LEN;
    const static tsl::ordered_map<std::string, int> ALERT_FIELDS;

    // length of the trigger payload without taking in account
    // the measuremet fields
    const static int TRIGGER_PAYLOAD_NO_MEAS_LEN;
    const static tsl::ordered_map<std::string, int> TRIGGER_FIELDS;

    const static tsl::ordered_map<std::string, int> ACTION_RESULT_FIELDS;

    const static int ACTION_INSTR_PAYLOAD_LEN;
    const static tsl::ordered_map<std::string, int> ACTION_INSTR_FIELDS;

    const static int CONF_SET_PAYLOAD_LEN;
    const static tsl::ordered_map<std::string, int> CONF_SET_FIELDS;

    const static int REQUEST_STATUS_REPORT_PAYLOAD_LEN;
    const static tsl::ordered_map<std::string, int> REQUEST_STATUS_REPORT_FIELDS;

    const static int REQUEST_MEASUREMENTS_PAYLOAD_LEN;
    const static tsl::ordered_map<std::string, int> REQUEST_MEASUREMENTS_FIELDS;

    const static int ALERT_SET_PAYLOAD_LEN;
    const static tsl::ordered_map<std::string, int> ALERT_SET_FIELDS;

    const static int ALERT_REMOVE_PAYLOAD_LEN;
    const static tsl::ordered_map<std::string, int> ALERT_REMOVE_FIELDS;

    const static int TRIGGER_SET_PAYLOAD_LEN;
    const static tsl::ordered_map<std::string, int> TRIGGER_SET_FIELDS;

    const static int TRIGGER_REMOVE_PAYLOAD_LEN;
    const static tsl::ordered_map<std::string, int> TRIGGER_REMOVE_FIELDS;

    const static int SENSOR_ACTIVATE_PAYLOAD_LEN;
    const static tsl::ordered_map<std::string, int> SENSOR_ACTIVATE_FIELDS;

    const static int SENSOR_DEACTIVATE_PAYLOAD_LEN;
    const static tsl::ordered_map<std::string, int> SENSOR_DEACTIVATE_FIELDS;

    const static int ACTUATOR_ACTIVATE_PAYLOAD_LEN;
    const static tsl::ordered_map<std::string, int> ACTUATOR_ACTIVATE_FIELDS;

    const static int ACTUATOR_DEACTIVATE_PAYLOAD_LEN;
    const static tsl::ordered_map<std::string, int> ACTUATOR_DEACTIVATE_FIELDS;

    const static int RESET_PAYLOAD_LEN;
    const static tsl::ordered_map<std::string, int> RESET_FIELDS;


    const static tsl::ordered_map<std::string, int> MEASUREMENTS_PACKET_FLAGS;


    struct Packet {
        PacketType type;
        // id of the packet origin
        int mote_id;
        // packet number. necessary to sync acks and nacks
        int nr;
        // length of the packet payload in bytes
        size_t payload_len;
        // field values depending of the packet type
        std::map<std::string, boost::any> fields;
    };

    Protocol ();

    Packet parse_mote_packet (const char *packet, size_t packet_len);
    void parse_mote_packet (const char *packet, size_t packet_len, Packet&);
    // creates a packet from #type a #fields and puts result in #packet
    std::string create_server_packet (
        int mote_id,
        PacketType type,
        std::map<std::string, std::vector<char>> fields,
        int packet_no = 0
    );
    std::string create_server_ack (int mote_id, int packet_no = 0);
    std::string create_server_nack (int mote_id, int packet_no = 0);


protected:

    void extract_mote_header (const char* packet, Packet& pt);

    std::string decrypt_packet (const char* packet, size_t packet_len);
    std::string encrypt_packet (int mote_id, const char* packet, size_t packet_len);

    size_t insert_fields (
        PacketType type,
        std::map<std::string, std::vector<char>> input_fields,
        const tsl::ordered_map<std::string, int> fields,
        char *packet,
        size_t buffer_len
    );

    void extract_fields (
        const char *packet,
        size_t packet_len,
        const tsl::ordered_map<std::string, int> fields,
        Packet& pt
    );

    void extract_measurements (
        const char *packet,
        size_t packet_len,
        int from,
        Packet& pt
    );

};


#endif // __PROTOCOL_HEAD__
