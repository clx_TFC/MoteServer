#include "server/Server.h"
#include "mote_status/MoteStatus.h"
#include "inter_comm/ErrorOutChannel.h"
#include "db/DBPool.h"
#include "inter_comm/Redis.h"

#include <boost/thread/thread.hpp>


int main (int argc, char **argv) {

    ErrorOutChannel::init();
    MoteStatus::init();
    DBPool::init();
    Redis::init();

    boost::thread a { ErrorOutChannel::loop };
    boost::thread c { Server::init };

    a.join();
    c.join();

    return 0;
}
