#include <stdexcept>

class InterCommServerExcept : public std::exception {
private:
    std::string err_msg;
public:
    InterCommServerExcept (std::string em) : err_msg(em) {};
    char const *what () const throw() {
        return err_msg.c_str();
    }
};
