#include "InChannel.h"

zmqpp::context *InChannel::zmq_context = new zmqpp::context();

InChannel::InChannel () {

    config = Conf::getInstance();
    auto in_chn = config->get("zmq_interserver_chnin_name");

    if (in_chn.IsNull())
        throw std::runtime_error(
            "[OutChannel] ZMQ connection configurations not set."
            "Field zmq_interserver_chnin_name"
            " must be set in configuration file."
        );

    auto conn_str = in_chn.as<std::string>();
    zmq_socket = new zmqpp::socket(*zmq_context, zmqpp::socket_type::pull);
    zmq_socket->bind(conn_str);

#ifdef DEBUG
    Logger::debug("[InChannel] ZMQ bind - " + conn_str);
#endif

}

InChannel::~InChannel () {
    zmq_socket->close();
    delete zmq_socket;
}

std::string InChannel::read () {
    zmqpp::message msg;
    zmq_socket->receive(msg);
    std::string message;
    msg >> message;
#ifdef DEBUG
    Logger::debug("[InChannel] read msg " + message);
#endif
    return message;
}
