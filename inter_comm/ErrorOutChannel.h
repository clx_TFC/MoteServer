#ifndef __ERROROUTCHANNEL_HEAD__
#define __ERROROUTCHANNEL_HEAD__


#include <queue>
#include <mutex>
#include <stdexcept>
#include <cstring>

#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include "inter_comm/Redis.h"

#include "util/Conf.h"
#include "util/Logger.h"
#include "util/Misc.h"

#include "db/DB.h"
#include "db/MoteError.h"


namespace ErrorOutChannel {

namespace {
    // to store errors that
    // failed to be sent
    std::queue<std::tuple<int, std::string>> error_stack;
    std::mutex error_stack_mutex;


    // period of periodic flushes
    int flush_period;
    boost::asio::io_service *io;
    boost::asio::deadline_timer *timer;

    bool initialized = false;
};

namespace __priv { // FIXME: shold be anonymous

    // called periodically to try to
    // send all the errors
    void flush_errors (const boost::system::error_code&) noexcept;

    // add error to queue
    void push_error (int, std::string) noexcept;
    // remove oldest error from queue
    void pop_error () noexcept;
    // get oldest error without removing it
    std::tuple<int, std::string> front () noexcept;
    bool stack_empty () noexcept;

    void set_timer () noexcept;

    // save the error to the database
    void save_to_db (int, std::string) noexcept;
};

void init ();
void loop ();
/**
 * takes mote id and message
 */
void write (int, std::string) noexcept;

};


#endif // __ERROROUTCHANNEL_HEAD__
