#include "Redis.h"

void Redis::init () {

    using namespace std::string_literals;

    Conf *config = Conf::getInstance();
    auto redis_address = config->get("redis_address");
    auto redis_prt    = config->get("redis_port");

    if (redis_address.IsNull() || redis_prt.IsNull())
        throw std::runtime_error(
            "[Redis] Redis connection configurations not set."
            "Fields redis_address and redis_port"
            " must be set in configuration file."
        );

    redis_addr = redis_address.as<std::string>();
    redis_port = redis_prt.as<int>();
    redis_client.connect(redis_addr, redis_port);
    redis_subscriber.connect(redis_addr, redis_port);

    Logger::log("[Redis] connected to " + redis_addr + ":" + std::to_string(redis_port));
}


void Redis::publish (std::string channel, int mote_id, std::string msg) {
    using namespace std::string_literals;

    std::string chn = channel + ":" + std::to_string(mote_id);

#ifdef DEBUG
    Logger::debug("[Redis] publishing to channel " + chn);
#endif

    redis_client.publish(chn, msg, [chn](cpp_redis::reply& reply) {
        if (reply.is_error()) {
            Logger::err("[Redis] failed to publish to channel " + chn);
        } else {
#ifdef DEBUG
            Logger::debug("[Redis] published to channel " + chn);
#endif
        }
    });
    redis_client.commit();
}


void Redis::subscribe (int mote_id, void(*callback)(const std::string&, const std::string&)) {
#ifdef DEBUG
    Logger::debug("[Redis] subscribing to requests to mote " + std::to_string(mote_id));
#endif
    std::string channel = "mote_conn:" + std::to_string(mote_id);
    redis_subscriber.subscribe(channel, callback);
    redis_subscriber.commit();
}

void Redis::unsubscribe (int mote_id) {
    using namespace std::string_literals;

#ifdef DEBUG
    Logger::debug("[Redis] unsubscribing to requests to mote " + std::to_string(mote_id));
#endif

    std::string channel = "mote_conn:" + std::to_string(mote_id);
    redis_subscriber.unsubscribe(channel);
    redis_subscriber.commit();
}
