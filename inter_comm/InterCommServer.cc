#include "InterCommServer.h"


void handle_msg          (std::string, YAML::Node&);


void InterCommServer::init () {
    Conf *config = Conf::getInstance();
    auto _mote_port = config->get("mote_port");
    if (_mote_port.IsNull())
        throw std::runtime_error(
            "[InterCommServer] mote_port must be set in configuration file"
        );

    MOTE_PORT = _mote_port.as<std::string>();
    Logger::log("[InterCommServer] initialized", Logger::Color::blue);
}

// get the current ip of the mote from the database
std::string get_mote_ip (int mote_id) {
    DB db;
    pqxx::result r = db.exec_sql(
        "select current_ip from mote where id = " + std::to_string(mote_id)
    );
    if (r.size() == 0)
        throw InterCommServerExcept(
            "IP do mote " + std::to_string(mote_id) + " não foi encontrado"
        );
    return r[0][0].as<std::string>();
}

// try to get an ack from the mote
void read_ack (Socket& sk, Protocol& pt) {
    Socket::Resposta res = sk.receber_mensagem(Protocol::MAX_PACKET_LEN);
    Protocol::Packet ack = pt.parse_mote_packet(res.resposta, res.tam_resposta);
    res.dispose();
    if (ack.type != Protocol::PacketType::ACK)
        throw InterCommServerExcept(
            "Esperado ACK mas recebido pacote to tipo " +
            pt.packet_types_str[ack.type]
        );
}

// create the packet and a socket to send the
// data
void send_packet (
    std::map<std::string, std::vector<char>> pk,
    Protocol::PacketType type,
    char *packet,
    int buf_len,
    int mote_id
) {

    Protocol pt;
    size_t plen = pt.create_server_packet(mote_id, type, pk, packet, buf_len);

#ifdef DEBUG
    Logger::debug("[InterCommServer] packet to send");
    Misc::print_bytes(packet, plen);
#endif

    Socket sk = Socket(
        get_mote_ip(mote_id).c_str(),
        InterCommServer::MOTE_PORT.c_str()
    );
    size_t sent = sk.enviar_mensagem(packet, plen);

    if (sent != plen) {
        sk.fechar();
        throw InterCommServerExcept("Erro ao enviar pacote ao mote");
    }

    read_ack(sk, pt);
    sk.fechar();
}


/**
 * Receive messages from other components in the backend
 * and then send them to the appropriate mote
 */
void InterCommServer::server_loop () {

    InChannel in;

    Logger::log("[InterCommServer] Server started ",
        Logger::Color::blue, Logger::Modifier::bold
    );

    while (true) {
        using namespace std::string_literals;
        try {
            // connection id to be used to send
            // response
            std::string con_id = in.read();
            // message should be in yaml format
            std::string msg    = in.read();
#ifdef DEBUG
            Logger::debug("[InterCommServer] connection id: " + con_id);
            Logger::debug("[InterCommServer] received msg: " + msg);
#endif
            YAML::Node yaml = YAML::Load(msg);
            std::thread { std::bind(handle_msg, con_id, yaml) };
        } catch (std::exception& ex) {
            Logger::err("[InterCommServer] main loop error: "s + ex.what());
        }
    }
}


/**
 * Parses the received message according to the specified operation
 * then creates a packet and sends it
 */
void do_op (
    std::string con_id,
    YAML::Node& msg,
    std::string op_name,
    int payload_len,
    Protocol::PacketType p_type,
    const tsl::ordered_map<std::string, int> fields
) {

    using namespace std::string_literals;
    const int buf_len = payload_len + Protocol::SERVER_PACKET_HEAD_LEN;
    char *packet = new char[buf_len];
    OutChannel out;

    try {

        // get destination id
        auto _mote = msg["mote"];
        if (_mote.IsNull())
            throw InterCommServerExcept("Campo mote é necessário");

        int mote = _mote.as<int>();
        // to store the packet fields
        std::map<std::string, std::vector<char>> packet_data;

        // convert the values received to bytes
        // to be added to the packet
        for (auto& f : fields) { // iterate over the packet field spec
            std::string f_name = f.first;
            int nr_bytes       = f.second;

            auto _f = msg[f_name];
            if (_f.IsNull()) // check if present
                throw InterCommServerExcept("Campo " + f_name + " é necessário");

            int f_value;
            // verify that the value is an integer
            try {
                f_value = _f.as<int>();
            } catch (std::exception& ex) {
                throw InterCommServerExcept(
                    "Campo " + f_name + " deve ser um número inteiro"
                );
            }

            packet_data[f_name] = Misc::int_to_bytes(f_value, nr_bytes);
        }

#ifdef DEBUG
        Logger::debug("[InterCommServer] " + op_name + " packet");
        Misc::print_bytes(packet_data);
#endif

        send_packet(packet_data, p_type, packet, buf_len, mote);

        // send an ack to acknowledge the source of the message
        // that it has been sent
        std::string ms = "msg: inter_comm_ack\n"s +
            "op: " + op_name + "\n" +
            "con_id: " + con_id + "\n";
        out.write(ms);


    } catch (InterCommServerExcept& ex) {

        Logger::err(
            "[InterCommServer] failed to execute operation "s +
            op_name + ": " + ex.what()
        );

        std::string ms = "msg: inter_comm_err\n"s +
            "op: " + op_name + "\n" +
            "con_id: " + con_id + "\n" +
            "err: " + ex.what();
        out.write(ms);

    } catch (std::exception& ex) {

        Logger::err(
            "[InterCommServer] failed to execute operation "s +
            op_name + ": " + ex.what()
        );

        std::string ms = "msg: inter_comm_err\n"s +
            "op: " + op_name + "\n" +
            "con_id: " + con_id + "\n" +
            "err: Erro no servidor";
        out.write(ms);

    }

    delete[] packet;
}


/**
 * Checks the operation defined in the message and gets the information
 * on how to parse it. Paramenters should have the names and the sizes (in bytes)
 * as specified in the <op>_FIELDS in protocol/Protocol.h
 * The operation types are:
 *   * request_measurements
 *   * conf_set : change the current configuration values
 *   * action_instr : send an action instruction
 *   * request_status_reports
 *   * set_alert/remove_alert
 *   * set_trigger/remove_trigger
 *   * activate_sensor/deactivate_sensor
 *   * activate_actuator/deactivate_actuator
 *   * reset : to reinitialize the mote
 */
void handle_msg (std::string con_id, YAML::Node& msg) {

    using namespace std::string_literals;

    try {

        std::string msg_type = msg["msg"].as<std::string>();

        if (msg_type == "conf_set") {
            do_op(
                con_id, msg, msg_type,
                Protocol::CONF_SET_PAYLOAD_LEN,
                Protocol::PacketType::CONF_SET,
                Protocol::CONF_SET_FIELDS
            );
        }
        else if (msg_type == "action_instr") {
            do_op(
                con_id, msg, msg_type,
                Protocol::ACTION_INSTR_PAYLOAD_LEN,
                Protocol::PacketType::ACTION_INSTR,
                Protocol::ACTION_INSTR_FIELDS
            );
        }
        else if (msg_type == "request_measurements") {
            do_op(
                con_id, msg, msg_type,
                Protocol::REQUEST_STATUS_REPORT_PAYLOAD_LEN,
                Protocol::PacketType::REQUEST_MEASUREMENTS,
                Protocol::REQUEST_MEASUREMENTS_FIELDS
            );
        }
        else if (msg_type == "request_status_reports") {
            do_op(
                con_id, msg, msg_type,
                Protocol::REQUEST_STATUS_REPORT_PAYLOAD_LEN,
                Protocol::PacketType::REQUEST_STATUS_REPORT,
                Protocol::REQUEST_STATUS_REPORT_FIELDS
            );
        }
        else if (msg_type == "set_alert") {
            do_op(
                con_id, msg, msg_type,
                Protocol::ALERT_SET_PAYLOAD_LEN,
                Protocol::PacketType::ALERT_SET,
                Protocol::ALERT_SET_FIELDS
            );
        }
        else if (msg_type == "remove_alert") {
            do_op(
                con_id, msg, msg_type,
                Protocol::ALERT_REMOVE_PAYLOAD_LEN,
                Protocol::PacketType::ALERT_REMOVE,
                Protocol::ALERT_REMOVE_FIELDS
            );
        }
        else if (msg_type == "set_trigger") {
            do_op(
                con_id, msg, msg_type,
                Protocol::TRIGGER_SET_PAYLOAD_LEN,
                Protocol::PacketType::TRIGGER_SET,
                Protocol::TRIGGER_SET_FIELDS
            );
        }
        else if (msg_type == "remove_trigger") {
            do_op(
                con_id, msg, msg_type,
                Protocol::TRIGGER_REMOVE_PAYLOAD_LEN,
                Protocol::PacketType::TRIGGER_REMOVE,
                Protocol::TRIGGER_REMOVE_FIELDS
            );
        }
        else if (msg_type == "activate_sensor") {
            do_op(
                con_id, msg, msg_type,
                Protocol::SENSOR_ACTIVATE_PAYLOAD_LEN,
                Protocol::PacketType::SENSOR_ACTIVATE,
                Protocol::SENSOR_ACTIVATE_FIELDS
            );
        }
        else if (msg_type == "deactivate_sensor") {
            do_op(
                con_id, msg, msg_type,
                Protocol::SENSOR_DEACTIVATE_PAYLOAD_LEN,
                Protocol::PacketType::SENSOR_DEACTIVATE,
                Protocol::SENSOR_DEACTIVATE_FIELDS
            );
        }
        else if (msg_type == "activate_actuator") {
            do_op(
                con_id, msg, msg_type,
                Protocol::ACTUATOR_ACTIVATE_PAYLOAD_LEN,
                Protocol::PacketType::ACTUATOR_ACTIVATE,
                Protocol::ACTUATOR_ACTIVATE_FIELDS
            );
        }
        else if (msg_type == "deactivate_actuator") {
            do_op(
                con_id, msg, msg_type,
                Protocol::ACTUATOR_DEACTIVATE_PAYLOAD_LEN,
                Protocol::PacketType::ACTUATOR_DEACTIVATE,
                Protocol::ACTUATOR_DEACTIVATE_FIELDS
            );
        }
        else if (msg_type == "reset") {
            do_op(
                con_id, msg, msg_type,
                Protocol::RESET_PAYLOAD_LEN,
                Protocol::PacketType::RESET,
                Protocol::RESET_FIELDS
            );
        }
        else
            throw std::runtime_error("Undefined message type: " + msg_type);

    } catch (std::exception& ex) {
        Logger::err(
            "[InterCommServer] handle message error: "s + ex.what()
        );
    }
}
