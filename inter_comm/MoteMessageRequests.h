#ifndef __MOTE_MESSAGES_REQUEST_HEAD__
#define __MOTE_MESSAGES_REQUEST_HEAD__


#include <stdexcept>

#include "ext_lib/json/json.hpp"
#include "ext_lib/tsl/ordered_map.h"

#include "inter_comm/Redis.h"
#include "inter_comm/ErrorOutChannel.h"
#include "inter_comm/InterCommServerExcept.h"

#include "server/Server.h"

#include "util/Logger.h"
#include "util/Misc.h"


using namespace nlohmann;


/**
 * Handle requests to send messages to motes
 */
namespace MoteMessageRequests {

/**
 * Subscribe to requests
 *
 * @param mote_id the id of the newly connected mote
 */
void mote_connected (int mote_id);


/**
 * Unsubscribe to requests
 *
 * @param mote_id the id of the mote that's going away
 */
void mote_disconnected (int mote_id);


/**
 * Handle request published to redis channel
 */
void on_request (const std::string&, const std::string&);

}


#endif // __MOTE_MESSAGES_REQUEST_HEAD__
