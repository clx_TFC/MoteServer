#include "inter_comm/MoteMessageRequests.h"


std::string make_packet (json data, int mote_id);
int mote_id_from_channel (std::string channel);


void MoteMessageRequests::mote_connected (int mote_id) {
    Redis::subscribe(mote_id, &MoteMessageRequests::on_request);
}

void MoteMessageRequests::mote_disconnected (int mote_id) {
    Redis::unsubscribe(mote_id);
}


void MoteMessageRequests::on_request (const std::string& channel, const std::string& msg) {

    using namespace std::string_literals;

    int mote_id = -1;
    std::string packet;

    try {
        mote_id = mote_id_from_channel(channel);

        json data = json::parse(msg);
        packet = make_packet(data, mote_id);

#ifdef DEBUG
        Logger::debug("[MoteMessageRequests] sending packet");
        Misc::print_bytes(packet.data(), packet.size());
#endif
        Server::send_message_to_mote(mote_id, packet.data(), packet.size());

    } catch (InterCommServerExcept& ex) {

        Logger::err(
            "[MoteMessageRequests] Failed to send request to mote: "s + ex.what()
        );

        if (mote_id != -1) {
            std::string msg = "{\"msg\": \"operation_error\","s +
            "\"err\": \"" + ex.what() + "\"}";
            ErrorOutChannel::write(mote_id, msg);
        }

    } catch (ServerExcept& ex) {

        Logger::err(
            "[MoteMessageRequests] Failed to send request to mote: "s + ex.what()
        );

        if (mote_id != -1) {
            std::string msg = "{\"msg\": \"operation_error\","s +
            "\"err\": \"" + ex.what() + "\"}";
            ErrorOutChannel::write(mote_id, msg);
        }

    } catch (std::exception& ex) {
        Logger::err(
            "[MoteMessageRequests] Failed to send request to mote: "s + ex.what()
        );

        if (mote_id != -1) {
            std::string msg = "{\"msg\": \"operation_error\","s +
            "\"err\": \"Falha a enviar comando a mote\"}";
            ErrorOutChannel::write(mote_id, msg);
        }
    }
}


int mote_id_from_channel (std::string channel) {
    auto parts = Misc::split(channel, ':');
    if (parts.size() < 2)
        throw std::runtime_error("Bad channel name '" + channel + "'");
    return std::stoi(parts[1]);
}


std::string make_packet (json data, int mote_id) {
    // to store the packet fields
    std::map<std::string, std::vector<char>> packet_data;
    const tsl::ordered_map<std::string, int> *fields;

    auto mst = data["msg_type"];
    auto mt  = data["mote_id"];
    if (!mst.is_string()) {
        throw InterCommServerExcept(
            "Campo \"msg_type\" é necessário e deve ser uma string"
        );
    }
    std::string msg_type = mst.get<std::string>();
    Protocol::PacketType type;

    if (msg_type == "conf_set") {
        fields = &Protocol::CONF_SET_FIELDS;
        type = Protocol::PacketType::CONF_SET;
    }
    else if (msg_type == "action_instr") {
        fields = &Protocol::ACTION_INSTR_FIELDS;
        type = Protocol::PacketType::ACTION_INSTR;
    }
    else if (msg_type == "request_measurements") {
        fields = &Protocol::REQUEST_MEASUREMENTS_FIELDS;
        type = Protocol::PacketType::REQUEST_MEASUREMENTS;
    }
    else if (msg_type == "request_status_report") {
        fields = &Protocol::REQUEST_STATUS_REPORT_FIELDS;
        type = Protocol::PacketType::REQUEST_STATUS_REPORT;
    }
    else if (msg_type == "alert_set") {
        fields = &Protocol::ALERT_SET_FIELDS;
        type = Protocol::PacketType::ALERT_SET;
    }
    else if (msg_type == "alert_remove") {
        fields = &Protocol::ALERT_REMOVE_FIELDS;
        type = Protocol::PacketType::ALERT_REMOVE;
    }
    else if (msg_type == "trigger_set") {
        fields = &Protocol::TRIGGER_SET_FIELDS;
        type = Protocol::PacketType::TRIGGER_SET;
    }
    else if (msg_type == "trigger_remove") {
        fields = &Protocol::TRIGGER_REMOVE_FIELDS;
        type = Protocol::PacketType::TRIGGER_REMOVE;
    }
    else if (msg_type == "sensor_activate") {
        fields = &Protocol::SENSOR_ACTIVATE_FIELDS;
        type = Protocol::PacketType::SENSOR_ACTIVATE;
    }
    else if (msg_type == "sensor_deactivate") {
        fields = &Protocol::SENSOR_DEACTIVATE_FIELDS;
        type = Protocol::PacketType::SENSOR_DEACTIVATE;
    }
    else if (msg_type == "actuator_activate") {
        fields = &Protocol::ACTUATOR_ACTIVATE_FIELDS;
        type = Protocol::PacketType::ACTUATOR_ACTIVATE;
    }
    else if (msg_type == "actuator_deactivate") {
        fields = &Protocol::ACTUATOR_DEACTIVATE_FIELDS;
        type = Protocol::PacketType::ACTUATOR_DEACTIVATE;
    }
    else if (msg_type == "reset") {
        fields = &Protocol::RESET_FIELDS;
        type = Protocol::PacketType::RESET;
    }
    else
        throw InterCommServerExcept("Tipo de mensagem desconhecido: " + msg_type);

    // convert the values received to bytes
    // to be added to the packet
    for (auto& f : *fields) { // iterate over the packet field spec
        std::string f_name = f.first;
        int nr_bytes       = f.second;

        auto _f = data[f_name];
        if (_f.is_null()) // check if present
            throw InterCommServerExcept("Campo " + f_name + " é necessário");

        int f_value;
        // verify that the value is an integer
        try {
            f_value = _f.get<int>();
        } catch (std::exception& ex) {
            using namespace std::string_literals;
            Logger::err("[MoteMessageRequests] error converting to int "s + ex.what());
            throw InterCommServerExcept(
                "Campo " + f_name + " deve ser um número inteiro"
            );
        }

        packet_data[f_name] = Misc::int_to_bytes(f_value, nr_bytes);
    }
    Misc::print_bytes(packet_data);

    Protocol pt;
    return pt.create_server_packet(mote_id, type, packet_data);
}
