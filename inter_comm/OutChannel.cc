#include "OutChannel.h"


void OutChannel::write (int mote_id, std::string msg) {

#ifdef DEBUG
    Logger::debug("[OutChannel] msg to write: " + msg);
#endif

    Redis::publish("mote_data", mote_id, msg);
}
