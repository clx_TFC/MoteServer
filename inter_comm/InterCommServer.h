#ifndef __INTERCOMMSERVER_HEAD__
#define __INTERCOMMSERVER_HEAD__

#include "InChannel.h"
#include "OutChannel.h"
#include "InterCommServerExcept.h"
#include "ErrorOutChannel.h"

#include "util/Logger.h"
#include "util/Misc.h"
#include "util/Conf.h"

#include "protocol/Protocol.h"
#include "protocol/Crypto.h"

#include "db/DB.h"

#include "sockets/Socket.h"

#include <yaml-cpp/yaml.h>
#include <ext_lib/tsl/ordered_map.h>

#include <thread>


namespace InterCommServer {

namespace {
    std::string MOTE_PORT;
};

void init ();
void server_loop ();

};


#endif // __INTERCOMMSERVER_HEAD__
