#include "ErrorOutChannel.h"

void ErrorOutChannel::init () {
    if (initialized) return;

    io = new boost::asio::io_service();
    timer = new boost::asio::deadline_timer(*io);

    Conf *config = Conf::getInstance();
    auto period = config->get("error_channel_flush_period");
    if (period.IsNull())
        throw std::runtime_error(
            "[OutChannel] Error channel."
            "Field 'error_channel_flush_period' "
            "must be set in configuration file."
        );
    flush_period = period.as<int>();

#ifdef DEBUG
    Logger::debug(
        "[ErrorOutChannel] flush period: " + std::to_string(flush_period) + " second(s)"
    );
#endif

    __priv::set_timer();
    initialized = true;

    Logger::log(
        "[ErrorOutChannel] initialized", Logger::Color::blue, Logger::Modifier::bold
    );
}

void ErrorOutChannel::write (int mote_id, std::string msg) noexcept {
    using namespace std::string_literals;
    try {
        __priv::push_error(mote_id, msg);
    } catch (std::exception& ex) {
        Logger::err("[ErrorOutChannel] failed to push error");
        Logger::err("[ErrorOutChannel] exception: "s + ex.what());
    }
}

void ErrorOutChannel::loop () {
    if (initialized)
        io->run();
    else throw std::runtime_error("[ErrorOutChannel] not initialized");
}

void ErrorOutChannel::__priv::push_error (int mote_id, std::string msg) noexcept {
    error_stack_mutex.lock();
    error_stack.push(std::tuple<int, std::string>(mote_id, msg));
    error_stack_mutex.unlock();
}

void ErrorOutChannel::__priv::pop_error () noexcept {
    error_stack_mutex.lock();
    error_stack.pop();
    error_stack_mutex.unlock();
}

std::tuple<int, std::string> ErrorOutChannel::__priv::front () noexcept {
    error_stack_mutex.lock();
    std::tuple<int, std::string> err = error_stack.front();
    error_stack_mutex.unlock();
    return err;
}

bool ErrorOutChannel::__priv::stack_empty () noexcept {
    error_stack_mutex.lock();
    bool empty = error_stack.empty();
    error_stack_mutex.unlock();
    return empty;
}


void ErrorOutChannel::__priv::flush_errors (
    const boost::system::error_code& e
) noexcept {

    using namespace std::string_literals;

    if (e) {
        Logger::err("[ErrorOutChannel] timer failed/aborted: " + e.message());
        return;
    }

#ifdef DEBUG
    int n = 0; // nr of flushed errors;
#endif

    try {

        while (!stack_empty()) {
            std::tuple<int, std::string> e = front();

            int mote_id     = std::get<0>(e);
            std::string msg = std::get<1>(e);
            Redis::publish("mote_error", mote_id, msg);
            save_to_db(mote_id, msg);
            pop_error();

#ifdef DEBUG
            n++;
#endif
        }


#ifdef DEBUG
        Logger::debug("[ErrorOutChannel] flushed " + std::to_string(n) + " errors");
#endif

    } catch (std::exception& ex) {
        Logger::err("[ErrorOutChannel] failed to flush errors");
        Logger::err("[ErrorOutChannel] exception: "s + ex.what());
    }

    __priv::set_timer();
}

void ErrorOutChannel::__priv::set_timer () noexcept {
    using namespace std::string_literals;
    try {

        timer->cancel(); // cancel any existing wait
        timer->expires_from_now(boost::posix_time::seconds(flush_period));
        timer->async_wait(&ErrorOutChannel::__priv::flush_errors);

#ifdef DEBUG
        Logger::debug("[ErrorOutChannel] timer set");
#endif

    } catch (std::exception& ex) {
        Logger::err("[ErrorOutChannel] failed to set timer");
        Logger::err("[ErrorOutChannel] exception: "s + ex.what());
    }
}


void ErrorOutChannel::__priv::save_to_db (int mote_id, std::string msg) noexcept {
    try {
        DB db;
        Tables::MoteError error;
        error.date_time = Misc::curr_date_time_str();
        error.err_code = 0;
        error.err_msg = msg;
        error.mote_id = mote_id;
        db.exec_sql(error.write_sql());
    } catch (std::exception& ex) {
        Logger::err("[ErrorOutChannel] failed to save error from mote to database");
    }
}
