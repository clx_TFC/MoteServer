#ifndef __INCHANNEL_HEAD__
#define __INCHANNEL_HEAD__


#include <zmqpp/zmqpp.hpp>

#include "util/Conf.h"
#include "util/Logger.h"


/**
 * Channel to receive messagens from application server.
 * Implementation with ZMQ
 */
class InChannel {

private:
    // context shared among all InChannel instannces
    static zmqpp::context *zmq_context;
    // socket used by this instance
    zmqpp::socket *zmq_socket;

    // to get communication settings
    Conf *config;

public:

    InChannel ();
    ~InChannel ();

    std::string read ();
};


#endif // __INCHANNEL_HEAD__
