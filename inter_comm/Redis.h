#ifndef __OUTCHANNELREDIS__HEAD__
#define __OUTCHANNELREDIS__HEAD__

#include <string>

#include <cpp_redis/cpp_redis>

#include "util/Conf.h"
#include "util/Logger.h"


/**
 * Channel use to write messages to the application server
 * usin redis
 */
namespace Redis {

namespace {
    std::string redis_addr;
    int redis_port;

    cpp_redis::client     redis_client;
    cpp_redis::subscriber redis_subscriber;
}


void init ();

/**
 * Publish mote event
 *
 * @param channel the channel to publish to
 * @param mote_id id of the mote
 * @param message message to publish
 */
void publish (std::string channel, int mote_id, std::string message);


/**
 * Subscribe to messages to a mote.
 *
 * @param mote_id the id of the mote
 * @param callback to be called when a message is published to be sent to the mote
 */
void subscribe (int mote_id, void(*callback)(const std::string&, const std::string&));

/**
 * Unsubscribe to messages to a mote.
 *
 * @param mote_id the id of the mote
 */
void unsubscribe (int mote_id);

};


#endif // __OUTCHANNELREDIS__HEAD__
