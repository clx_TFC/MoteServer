#ifndef __OUTCHANNEL_HEAD__
#define __OUTCHANNEL_HEAD__


#include "Redis.h"

#include "util/Conf.h"
#include "util/Logger.h"


/**
 * Publish a mote message
 */
class OutChannel {

public:
    /**
     * takes mote id and message
     */
    void write (int, std::string);

};


#endif // __OUTCHANNEL_HEAD__
