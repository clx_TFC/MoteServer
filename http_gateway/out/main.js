"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Http = require("http");
var Net = require("net");
var config = require('./config.json');
function log(msg) {
    var now = new Date();
    console.log("{ " + now.toLocaleDateString() + " " + now.toLocaleTimeString() + " }  " + msg);
}
/**
 * Forward to the mote server a packet received from a mote
 *
 * @param packet packet received from a mote
 */
function forward_packet(packet) {
    log("forwarding data to " + config.mote_server_host + ":" + config.mote_server_port);
    var socket = Net.createConnection({
        port: config.mote_server_port, host: config.mote_server_host
    }, function () {
        log('connected to mote server');
        socket.write(packet, function () {
            log("entire packet written to server: " + packet.length + " bytes");
            socket.destroy();
        });
    });
    socket.on('error', function (error) { return log("failed to forward packet: " + error); });
}
/**
 * Handle mote http messages
 *
 * @param request  http request
 * @param response response
 */
function server(request, response) {
    log('new connection from mote');
    var data = [];
    request.on('data', function (chunk) { return data.push(chunk); });
    request.on('end', function () {
        forward_packet(Buffer.concat(data));
        response.writeHead(200, { 'Content-Type': 'text/plain' });
        response.end('ok');
        // request.connection.destroy()
    });
    request.on('close', function () { return log('connection from mote closed'); });
}
Http.createServer(server).listen(config.http_gateway_port);
log("Gateway listening in port " + config.http_gateway_port);
//# sourceMappingURL=main.js.map