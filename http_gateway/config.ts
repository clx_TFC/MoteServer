export default interface Config {
    mote_server_host: string,
    mote_server_port: number,
    http_gateway_port: number
}