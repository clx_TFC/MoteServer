import * as Http    from 'http'
import * as Net     from 'net'
import Config from './config'

const config: Config = require('./config.json')


function log (msg: string) {
    let now = new Date()
    console.log(
        `{ ${now.toLocaleDateString()} ${now.toLocaleTimeString()} }  ${msg}`
    )
}


/**
 * Forward to the mote server a packet received from a mote
 * 
 * @param packet packet received from a mote
 */
function forward_packet (packet: Buffer) {
    log(`forwarding data to ${config.mote_server_host}:${config.mote_server_port}`)
    let socket = Net.createConnection({
        port: config.mote_server_port, host: config.mote_server_host
    }, () => {
        log('connected to mote server')
        socket.write(packet, () => {
            log(`entire packet written to server: ${packet.length} bytes`)
            socket.destroy()
        })
    })
    socket.on('error', (error) => log(`failed to forward packet: ${error}`))
}


/**
 * Handle mote http messages
 * 
 * @param request  http request
 * @param response response
 */
function server (request: Http.IncomingMessage, response: Http.ServerResponse) {
    log('new connection from mote')
    let data = []
    request.on('data', (chunk) => data.push(chunk))
    request.on('end', () => {
        forward_packet(Buffer.concat(data))
        response.writeHead(200, { 'Content-Type': 'text/plain' })
        response.end('ok')
        // request.connection.destroy()
    })
    request.on('close', () => log('connection from mote closed'))
}

Http.createServer(server).listen(config.http_gateway_port)
log(`Gateway listening in port ${config.http_gateway_port}`)