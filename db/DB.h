#ifndef __DB_HEAD__
#define __DB_HEAD__


#include <string>
#include <iostream>
#include <stdexcept>
#include <chrono>

#include <pqxx/pqxx>

#include "util/Conf.h"
#include "util/Logger.h"
#include "Tables.h"
#include "Cache.h"
#include "DBPool.h"


/**
 * Database operations
 */
class DB {

private:
    DBPool::DBConn *db_con = 0; // lazily obtained
    pqxx::work *current_txn = 0; // current transaction

    void set_con ();

public:
    DB ();
    ~DB ();

    // escape a byte sequence to insert in sql
    // second param is index from where to start in the array
    // third param is where to stop (exclusive)
    std::string escape_raw (std::vector<char>, int, int);
    // escape standard stirng to insert in sql
    std::string escape (std::string);

    // execute a comand without a
    // a transaction is initilized and closed in the function
    pqxx::result exec_sql (std::string sql);
    // prepare a statement. takes the name and
    // the sql statement
    void prepare (std::string, std::string);

    // intialize a transaction
    DB& init_txn ();
    // execute an sql command inside the open transaction
    DB& exec_sql_txn (std::string sql);
    DB& exec_sql_txn (std::string sql, pqxx::result&);
    // end the transaction
    void commit_txn ();

    template<typename... T>
    pqxx::result exec_prepared (std::string name, T... args) {
        if (db_con == 0)
            throw std::runtime_error("[DB] no statement was prepared");

        pqxx::work work(*(db_con->conn));
        pqxx::prepare::invocation i = work.prepared(name);

        for (auto& a : { args... })
            i(a);

        pqxx::result r = i.exec();

        return r;
    }

    template<typename... T>
    DB& exec_prepared_txn (std::string name, T... args) {
        if (current_txn == 0)
            throw std::runtime_error("[DB] exec_sql - No transaction was started");

        pqxx::prepare::invocation i = current_txn->prepared(name);

        for (auto& a : { args... })
            i(a); // add arguments to query

        i.exec();

        return *this;
    }

    template<typename... T>
    DB& exec_prepared_txn (std::string name, pqxx::result& r, T... args) {
        if (current_txn == 0)
            throw std::runtime_error("[DB] exec_sql - No transaction was started");

        pqxx::prepare::invocation i = current_txn->prepared(name);

        for (auto& a : { args... })
            i(a); // add arguments to query

        r = i.exec();

        return *this;
    }
};


#endif // __DB_HEAD__
