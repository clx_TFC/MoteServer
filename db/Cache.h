#ifndef __XPTO_CACHE_HEAD__
#define __XPTO_CACHE_HEAD__


#include <mutex>
#include <hiredis/hiredis.h>

#include "util/Logger.h"
#include "util/Conf.h"

namespace Cache {

namespace {
    bool inited = false;
    redisContext *redis_context; // not thread safe
    std::mutex cache_mutex;
};

void init ();

bool get (int, std::string&) noexcept;
bool set (int, std::string) noexcept;

};


#endif // __XPTO_CACHE_HEAD__
