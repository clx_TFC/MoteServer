#include "Cache.h"


void Cache::init () {
    using namespace std::string_literals;

    Conf *config = Conf::getInstance();
    auto redis_address = config->get("redis_address");
    auto redis_port    = config->get("redis_port");
    auto cache_chn     = config->get("redis_cache_db_id");

    if (redis_address.IsNull() || redis_port.IsNull() || cache_chn.IsNull())
        throw std::runtime_error(
            "[OutChannel] Redis connection configurations not set."
            "Fields redis_address, redis_port and redis_cache_db_id"
            " must be set in configuration file."
        );

    auto ra_str = redis_address.as<std::string>();
    auto rp_int = redis_port.as<int>();
    redis_context = redisConnect(ra_str.c_str(), rp_int);

    if (redis_context == NULL || redis_context->err) {

        if (redis_context)
            Logger::debug("[Cache] Redis connection error: "s +
                redis_context->errstr);
        else
            Logger::debug("[Cache] Redis complete fail");

        throw std::runtime_error(
            "[Cache] Failed to connect to Redis("s +
            ra_str + ", "s + std::to_string(rp_int) + ")"
        );

    }

    int cache_db = cache_chn.as<int>();
    void *r = redisCommand(redis_context, "select %d", cache_db);
    redisReply *reply = static_cast<redisReply *>(r);

    if (!reply)
        throw std::runtime_error("[Cache] Failed select redis database");
    else if (reply->type == REDIS_REPLY_ERROR) {
        std::string err = "[Cache] Failed select redis database: "s +
            reply->str;
        freeReplyObject(reply);
        throw std::runtime_error(err);
    }

    freeReplyObject(reply);

#ifdef DEBUG
    Logger::debug(
        "[Cache] Connected to " + ra_str + ":" + std::to_string(rp_int) + ":" +
        std::to_string(cache_db)
    );
#endif

    inited = true;
}

bool Cache::get (int key, std::string& value) noexcept {

#ifdef DEBUG
    Logger::debug("[Cache] GET " + std::to_string(key));
#endif

    if (!inited) {
        Logger::warning("[Cache] not inited");
        return false;
    }

    using namespace std::string_literals;

    cache_mutex.lock();

    void *r = redisCommand(redis_context, "GET %d", key);
    redisReply *reply = static_cast<redisReply *>(r);
    if (!reply) {
        Logger::warning(
            "[Cache] no reply returned for key "s + std::to_string(key)
        );
        cache_mutex.unlock();
        return false;
    }

    else if (reply->type == REDIS_REPLY_ERROR) {
        Logger::warning(
            "[Cache] error reply for key "s + std::to_string(key) +": " +
            reply->str
        );
        freeReplyObject(reply);
        cache_mutex.unlock();
        return false;
    }

    else if (reply->type == REDIS_REPLY_STRING) {
        value = std::string(reply->str, reply->len);
        freeReplyObject(reply);
        cache_mutex.unlock();
        return true;
    }

    else if (reply->type == REDIS_REPLY_NIL) {
#ifdef DEBUG
        Logger::debug(
            "[Cache] no value for key "s + std::to_string(key)
        );
#endif
        freeReplyObject(reply);
        cache_mutex.unlock();
        return false;
    }

    Logger::warning(
        "[Cache] undefined response of type "s + std::to_string(reply->type)
    );

    freeReplyObject(reply);
    cache_mutex.unlock();
    return false;
}

bool Cache::set (int key, std::string value) noexcept {

#ifdef DEBUG
    Logger::debug(
        "[Cache] SET " + std::to_string(key) + " " + value
    );
#endif

    if (!inited) {
        Logger::warning("[Cache] not inited");
        return false;
    }

    using namespace std::string_literals;

    cache_mutex.lock();

    void *r = redisCommand(
        redis_context,
        "SET %d %s",
        key,
        value.c_str()
    );

    redisReply *reply = static_cast<redisReply *>(r);

    if (!reply) {
        Logger::warning("[Cache] Failed to cache value");
        cache_mutex.unlock();
        return false;
    }
    else if (reply->type == REDIS_REPLY_ERROR) {
        Logger::warning(
            "[Cache] Failed to cache value. Error: "s + reply->str
        );
        freeReplyObject(reply);
        cache_mutex.unlock();
        return false;
    }

    freeReplyObject(reply);
    cache_mutex.unlock();
    return true;
}
