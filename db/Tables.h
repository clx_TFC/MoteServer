#ifndef __TABLES_HEAD__
#define __TABLES_HEAD__


#include <string>
#include <sstream>
#include <stdexcept>
#include <vector>

#include <pqxx/pqxx>
#include <pqxx/tuple>

#include "util/Misc.h"

/**
 * Representation of tables and types in the application database.
 */
namespace Tables {

enum ConnectionDiretion {
    A_to_B,
    B_to_A,
    Bi,
};
enum AlertLevel {
    NORMAL,
    WARNING,
    CRITICAL,
};
enum ParamTest {
    EQ,
    LT, LT_EQ,
    GT, GT_EQ,
    IN, NOT_IN,
};

struct Table {

protected:
    // fields whose values are not null and their values
    // std::vector<std::string> set_fields;
    // std::vector<std::string> set_values;

public:
    // name of the table in the database
    std::string name;
    std::string fields;

    Table (std::string nm, std::string flds) : name(nm), fields(flds) {};

    virtual std::string read_sql (std::string where) {
        std::ostringstream oss;
        oss << "select " << this->fields << " from " << this->name << " where "
            << where;
        return oss.str();
    }

    virtual std::string write_sql () {

        std::ostringstream oss;
        oss << "insert into " << this->name
            << this->fields << " values "
            << this->to_string();
        return oss.str();
    }

    virtual std::string write_all_sql () {
        std::ostringstream oss;
        oss << "insert into " << this->name << " values "
            << this->to_string();
        return oss.str();
    }

    /**
     * Build a SQL query by id to read a column from a table
     */
    virtual std::string read_by_id_sql ()  = 0;

    /**
     * Takes the values returned from the database and wraps them in a object
     */
    virtual void wrap_result (const pqxx::tuple) = 0;

    /**
     * Return a string representation of the object
     * Result is in the format "(field_1, field_2, ...)"
     */
    virtual std::string to_string () = 0;

    /**
     * Return a string representation of the object
     * a comma separated list of its column values
     */
    virtual std::string as_csv_line () {
        // most tables don't need this
        return "";
    }

};


struct Mote;
struct MoteError;
struct MoteConfiguration;
struct Sensor;
struct ActionResult;
struct Measurement;
struct Parameter;
struct AlertOccurrence;
struct StatusReport;
struct Packet;

};


#endif // __TABLES_HEAD__
