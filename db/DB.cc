#include "DB.h"

DB::DB () {
    // do nothing
}

DB::~DB () {
    if (db_con) DBPool::release(db_con);
    if (current_txn) delete current_txn;
}


void DB::set_con () {
    if (db_con == 0) {
        boost::promise<DBPool::DBConn *> conn_p;
        boost::future<DBPool::DBConn *>  conn_f = conn_p.get_future();
        DBPool::get(std::move(conn_p));
        db_con = conn_f.get();
    }
}


std::string DB::escape_raw (std::vector<char> bytes, int from, int to) {
    set_con();
    int size = to - from;
    if (size < 1 || size < bytes.size())
        throw std::runtime_error(
            "[DB] Invalid arguments from(" + std::to_string(from) + ") and (" +
            std::to_string(to) + ")"
        );
    unsigned char *_bytes = new unsigned char[from-to];
    for (int i = from; i < to; i++)
        _bytes[i] = bytes[i];
    std::string escaped = db_con->conn->esc_raw(_bytes, bytes.size());
    delete[] _bytes;

    return escaped;
}

std::string DB::escape (std::string str) {
    set_con();
    return db_con->conn->esc(str);
}

pqxx::result DB::exec_sql (std::string sql) {
    set_con();

    pqxx::work txn(*(db_con->conn));

#ifdef DEBUG
    Logger::debug("[DB] SQL to execute: " + sql);
#endif

    pqxx::result r = txn.exec(sql);
    txn.commit();

    return r;
}

void DB::prepare (std::string name, std::string sql) {
    set_con();

#ifdef DEBUG
    Logger::debug("[DB] Preparing statement " + name + ": " + sql);
#endif

    db_con->conn->prepare(name, sql);
}

DB& DB::init_txn () {
    if (current_txn != 0)
        throw std::runtime_error(
            "Some transaction is still open. "
            "You must end it before starting a new one."
        );

    set_con();
    current_txn = new pqxx::work(*(db_con->conn));

#ifdef DEBUG
    Logger::debug("[DB] Transaction started");
#endif

    return *this;
}

void DB::commit_txn () {
    if (current_txn == 0)
        throw std::runtime_error("[DB] end_txn - No transaction was started");

    current_txn->commit();
    delete current_txn;
    current_txn = 0;
#ifdef DEBUG
    Logger::debug("[DB] Transaction ended");
#endif
}

DB& DB::exec_sql_txn (std::string sql) {
    if (current_txn == 0)
        throw std::runtime_error("[DB] exec_sql - No transaction was started");

#ifdef DEBUG
    Logger::debug("[DB] SQL to execute: " + sql);
#endif

    current_txn->exec(sql);
    return *this;
}

DB& DB::exec_sql_txn (std::string sql, pqxx::result& r) {
    if (current_txn == 0)
        throw std::runtime_error("[DB] exec_sql - No transaction was started");
    r = current_txn->exec(sql);
    return *this;
}
