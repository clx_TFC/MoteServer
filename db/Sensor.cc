#include "Sensor.h"

std::string Tables::Sensor::read_by_id_sql () {
    std::ostringstream oss;
    oss << "select " << this->fields << " from " << this->name << " where id = "
        << this->id;
    return oss.str();
}

void Tables::Sensor::wrap_result (const pqxx::tuple t) {
    if (!t[0].is_null())
        this->id = t[0].as<int>();
    if (!t[1].is_null())
        this->output_size = t[1].as<int>();
}

std::string Tables::Sensor::to_string () {
    std::ostringstream oss;
    oss << "(" << this->id << ", " << this->output_size << ")";
    return oss.str();
}
