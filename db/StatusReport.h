#ifndef __STATUSREPORT_HEAD__
#define __STATUSREPORT_HEAD__


#include "Tables.h"

namespace Tables {

    struct StatusReport : Table {

        int mote;
        std::string start_date_time;
        std::string end_date_time;
        int nr_tx_packets;
        int nr_lost_packets;
        int nr_tx_bytes;
        int nr_lost_bytes;

        StatusReport () : Table(
            "mote_status_report",
            "(mote_id, start_date_time, end_date_time, nr_tx_packets, nr_lost_packets, "
            "nr_tx_bytes, nr_lost_bytes)"
        ) {};
        std::string read_by_id_sql ();
        void wrap_result (const pqxx::tuple);
        std::string as_csv_line ();
        std::string to_string ();

    };

}


#endif // __STATUSREPORT_HEAD__
