#include "Hdfs.h"


void Hdfs::init () {
    if (inited) {
        Logger::warning("[Hdfs] already initialized");
        return;
    }
    fs_connect = hdfsConnect("default", 0);
    if (fs_connect == NULL) {
        std::ostringstream err_str;
        err_str << "[Hdfs] Failed to connect to HDFS: " << strerror(errno);
        throw std::runtime_error(err_str.str());
    }

    inited = true;

    Logger::log("[Hdfs] connection successful", Logger::Color::blue);
}


Hdfs::File Hdfs::open_mote_file (int mote_id, FileName filename) {
    std::ostringstream path_name;
    path_name << "mote_" << mote_id;

    switch (filename) {
        case MEASUREMENTS  : path_name << "/measuremets";    break;
        case ACTION_RESULTS: path_name << "/action_results"; break;
        case ALERTS        : path_name << "/alerts";         break;
        case TRIGGERS      : path_name << "/triggers";       break;
        case STATUS_REPORTS: path_name << "/status_reports"; break;
        case ERRORS        : path_name << "/errors";         break;
    }

    File file;
    file.file_path = path_name.str();

    file.file_handle = hdfsOpenFile(
        fs_connect, file.file_path.c_str(), O_APPEND|O_WRONLY, 0, 0, 0
    );

    if (!file.file_handle) {
        std::ostringstream err_str;
        err_str << "[Hdfs] Failed to open file " << file.file_path << ":"
                << strerror(errno);
        throw std::runtime_error(err_str.str());
    }

    Logger::log("[Hdfs] opened file " + file.file_path);

    return file;
}


void Hdfs::close_mote_file (File file) {
    if (hdfsCloseFile(fs_connect, file.file_handle) == -1) {
        std::ostringstream err_str;
        err_str << "[Hdfs] Failed to close file " << file.file_path << ":"
                << strerror(errno);
        Logger::warning(err_str.str());
    }

    Logger::log("[Hdfs] closed file " + file.file_path);
}


void Hdfs::append_to_file (File file, std::string data) {
    tSize num_written_bytes = hdfsWrite(
        fs_connect, file.file_handle, data.c_str(), data.size() // + 1 ?
    );

    if (num_written_bytes == -1) {
        std::ostringstream err_str;
        err_str << "[Hdfs] Failed to write to file " << file.file_path << ":"
                << strerror(errno);
        throw std::runtime_error(err_str.str());
    }

#ifdef DEBUG
    std::ostringstream debug_str;
    debug_str << "[Hdfs] wrote " << num_written_bytes << " out of "
              << data.size() << " bytes to " << file.file_path;
    Logger::debug(debug_str.str());
#endif
}


void Hdfs::flush_file (File file) {
    if (hdfsFlush(fs_connect, file.file_handle) == -1) {
        std::ostringstream err_str;
        err_str << "[Hdfs] Failed to flush file " << file.file_path << ":"
                << strerror(errno);
        throw std::runtime_error(err_str.str());
    }

#ifdef DEBUG
    Logger::debug("[Hdfs] flushed file " + file.file_path);
#endif

}



void Hdfs::open_and_append (int mote_id, FileName filename, std::string data) {
    File file = open_mote_file(mote_id, filename);
    append_to_file(file, data);
    flush_file(file);
    close_mote_file(file);
}
