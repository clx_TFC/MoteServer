#include "MoteConfiguration.h"

#include "Mote.h"


std::string Tables::MoteConfiguration::read_by_id_sql () {
    std::ostringstream oss;
    oss << "select " << this->fields << " from " << this->name << " where name = '"
        << this->name << "' and mote_id = " << this->mote;
    return oss.str();
}

void Tables::MoteConfiguration::wrap_result (const pqxx::tuple t) {
    if (!t[0].is_null())
        this->name = t[0].as<std::string>();
    if (!t[1].is_null())
        this->mote = t[1].as<int>();
    if (!t[2].is_null())
        this->measuremet_period = t[2].as<int>();
    if (!t[3].is_null())
        this->send_data_period = t[3].as<int>();
    if (!t[4].is_null())
        this->status_report_period = t[4].as<int>();
    if (!t[5].is_null())
        this->use_crypto = t[5].as<bool>();
    if (!t[6].is_null())
        this->current_ip = t[6].as<std::string>();
    if (!t[7].is_null())
        this->active = t[7].as<bool>();

}

std::string Tables::MoteConfiguration::to_string () {
    std::ostringstream oss;
    oss << "('" << this->name << "', "
        << this->mote << ", "
        << this->measuremet_period << ", "
        << this->send_data_period << ", "
        << this->status_report_period << ", "
        << this->use_crypto << ", '"
        << this->current_ip << "', "
        << this->active << ")";
    return oss.str();
}
