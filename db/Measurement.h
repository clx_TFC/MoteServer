#ifndef __MEASUREMENT_HEAD__
#define __MEASUREMENT_HEAD__


#include "Tables.h"

#include <sstream>
#include <string>

#include <pqxx/pqxx>


namespace Tables {

    struct Measurement : Table {

        int param;
        int sensor_use_num;
        int sensor;
        int mote;
        std::string date_time;
        std::string value;

        Measurement () : Table(
            "measurement",
            "(parameter_id, sensor_use_num, sensor_id, mote_id, date_time, value)"
        ) {};
        std::string read_by_id_sql ();
        void wrap_result (const pqxx::tuple);
        std::string as_csv_line ();
        std::string to_string ();

    };

};


#endif // __MEASUREMENT_HEAD__
