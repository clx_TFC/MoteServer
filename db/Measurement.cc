#include "Measurement.h"


std::string Tables::Measurement::read_by_id_sql () {
    std::ostringstream oss;
    oss << "select " << this->fields << " from " << this->name
        << " where parameter_id = " << this->param
        << " and sensor_use_num = " << this->sensor_use_num
        << " and sensor_id = " << this->sensor
        << " and mote_id = " << this->mote
        << " and date_time = '" << this->date_time << "'";
    return oss.str();
}

void Tables::Measurement::wrap_result (const pqxx::tuple t) {
    if (!t[0].is_null())
        this->param = t[0].as<int>();
    if (!t[1].is_null())
        this->sensor_use_num = t[1].as<int>();
    if (!t[2].is_null())
        this->sensor = t[2].as<int>();
    if (!t[3].is_null())
        this->mote = t[3].as<int>();
    if (!t[4].is_null())
        this->date_time = t[4].as<std::string>();
    if (!t[5].is_null())
        this->value = t[5].as<std::string>();
}

std::string Tables::Measurement::to_string () {
    std::ostringstream oss;
    oss << "(" << this->param << ", "
        << this->sensor_use_num << ", "
        << this->sensor << ", "
        << this->mote << ", '"
        << this->date_time << "', '"
        << this->value << "')";
    return oss.str();
}


std::string Tables::Measurement::as_csv_line () {
    std::ostringstream oss;
    oss << this->param << ","
        << this->sensor_use_num << ","
        << this->sensor << ","
        << this->mote << ","
        << this->date_time << ","
        << this->value << "\n";
    return oss.str();
}
