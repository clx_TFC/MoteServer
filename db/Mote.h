#ifndef __MOTE_H__
#define __MOTE_H__


#include "Tables.h"


namespace Tables {

    struct Mote : Table {

        int id;
        double location_lat;
        double location_lng;
        bool active;
        std::string current_ip;
        std::string last_status_report;
        std::string crypto_key;
        int crypto_last_nonce_in;
        int crypto_last_nonce_out;
        std::string purpose_description;
        std::string other_data;

        Mote () : Table(
            "mote",
            "(id, location_lat, location_lng, active, current_ip, last_status_report, "
            "crypto_key, crypto_last_nonce_in, crypto_last_nonce_out, "
            "purpose_description, other_data)"
        ) {};
        std::string read_by_id_sql ();
        void wrap_result (const pqxx::tuple);
        std::string to_string ();


        // // void set_id (int);
        // int get_id () { return this->id; }
        //
        // // void set_location_lat (double);
        // double set_location_lat () { return this->location_lat; }
        //
        // // void set_location_lng (double);
        // double set_location_lng () { return this->location_lng; }
        //
        // // void set_crypto_key (std::string);
        // std::string get_crypto_key () { return this->crypto_key; }
        //
        // // void set_purpose_description (std::string);
        // std::string get_purpose_description ()
        //     { return this->purpose_description; }
        //
        // // void set_other_data (std::string);
        // std::string get_other_data () { return this->other_data; }

    };

};


#endif // __MOTE_H__
