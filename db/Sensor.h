#ifndef __SENSOR_H__
#define __SENSOR_H__


#include "Tables.h"

#include <string>
#include <sstream>

#include <pqxx/tuple>

namespace Tables {
    struct Sensor : Table {
    private:
        int id;
        int output_size;

    public:
        Sensor () : Table("sensor", "(id, output_size)") {};
        std::string read_by_id_sql ();
        void wrap_result (const pqxx::tuple);
        std::string to_string ();
    };
}


#endif // __SENSOR_H__
