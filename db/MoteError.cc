#include "MoteError.h"

std::string Tables::MoteError::read_by_id_sql () {
    std::ostringstream oss;
    oss << "select " << this->fields << " from " << this->name
        << " where mote_id = " << this->mote_id
        << " and date_time = " << this->date_time;
    return oss.str();
}

void Tables::MoteError::wrap_result (const pqxx::tuple t) {
    if (!t[0].is_null())
        this->date_time = t[0].as<std::string>();
    if (!t[1].is_null())
        this->mote_id = t[1].as<int>();
    if (!t[2].is_null())
        this->err_code = t[2].as<int>();
    if (!t[3].is_null())
        this->err_msg = t[3].as<std::string>();
}

std::string Tables::MoteError::to_string () {
    std::ostringstream oss;
    oss << "('" << this->date_time << "', "
        << this->mote_id << ", "
        << this->err_code << ", '"
        << this->err_msg << "')";
    return oss.str();
}


std::string Tables::MoteError::as_csv_line () {
    std::ostringstream oss;
    oss << this->date_time << ","
        << this->mote_id << ","
        << this->err_code << ","
        << this->err_msg << "\n";
    return oss.str();
}
