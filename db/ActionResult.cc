#include "ActionResult.h"

std::string Tables::ActionResult::read_by_id_sql () {
    std::ostringstream oss;
    oss << "select " << this->fields << " from " << this->name
        << " where action_num = " << this->action_num
        << " and actuator_use_num = " << this->actuator_use_num
        << " and actuator_id = " << this->actuator
        << " and mote_id = " << this->mote
        << " and date_time = '" << this->date_time << "'";
    return oss.str();
}

void Tables::ActionResult::wrap_result (const pqxx::tuple t) {
    if (!t[0].is_null())
        this->action_num = t[0].as<int>();
    if (!t[1].is_null())
        this->actuator_use_num = t[1].as<int>();
    if (!t[2].is_null())
        this->actuator = t[2].as<int>();
    if (!t[3].is_null())
        this->mote = t[3].as<int>();
    if (!t[4].is_null())
        this->date_time = t[4].as<std::string>();
    if (!t[5].is_null())
        this->result = t[5].as<int>();
}

std::string Tables::ActionResult::to_string () {
    std::ostringstream oss;
    oss << "(" << this->action_num << ", "
        << this->actuator_use_num << ", "
        << this->actuator << ", "
        << this->mote << ", '"
        << this->date_time << "')";
    return oss.str();
}


std::string Tables::ActionResult::as_csv_line () {
    std::ostringstream oss;
    oss << this->action_num << ","
        << this->actuator_use_num << ","
        << this->actuator << ","
        << this->mote << ","
        << this->date_time << "\n";
    return oss.str();
}
