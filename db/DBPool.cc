#include "DBPool.h"

void DBPool::init () {
    using namespace std::string_literals;

    if (inited) {
        Logger::warning("[DBPool] initialized more than once");
        return;
    }

    Conf *config = Conf::getInstance();
    auto _pool_size = config->get("db_pool_size");
    if (_pool_size.IsNull())
        throw std::runtime_error(
            "[DBPool] Configuration for db_pool_size not found"
        );

    // allocate memory for connections
    connections.reserve(_pool_size.as<int>());

    // get database connection paramenters
    auto db_name     = config->get("db_name");
    auto db_username = config->get("db_username");
    auto db_password = config->get("db_password");
    auto db_host     = config->get("db_host");
    auto db_port     = config->get("db_port");

    if (db_name.IsNull()
     || db_username.IsNull()
     || db_password.IsNull()
     || db_port.IsNull()
     || db_host.IsNull())
        throw std::runtime_error(
            "[DB] Database configurations are not set.\n"
            "Fields db_name, db_username, db_password, db_port and db_host"
            " are needed in configuration file."
        );

    con_str =
          " dbname="s   + db_name.as<std::string>()
        + " host="s     + db_host.as<std::string>()
        + " port="s     + db_port.as<std::string>()
        + " user="s     + db_username.as<std::string>()
        + " password="s + db_password.as<std::string>();

    // NOTE: database connections are lazily created

#ifdef DEBUG
    Logger::debug(
        "[DBPool] initialized (poolsize = " +
        std::to_string(_pool_size.as<int>()) + ")"
    );
    Logger::debug("[DBPool] connection string: " + con_str);
#endif

    inited = true;
}


/**
 * Try to get a connection without blocking to wait for one
 * to become available
 *
 * @return the connection or none if nothing found
 */
boost::optional<DBPool::DBConn *> DBPool::get_nonblock () {

    if (!inited)
        throw std::runtime_error("[DBPool] not initialized");

    conn_mutex.lock();

    // look for an open and available connection
    for (auto iter = connections.begin(); iter != connections.end();) {
        auto conn = *iter;
        if (!conn || !(conn->conn)) { // some client maybe deleted it
            iter = connections.erase(iter);
            Logger::warning("[DBPool] null connection found in pool");
            continue;
        }
        if (conn->__priv_available.load()) {
            conn->__priv_available = false;
            conn_mutex.unlock();
            return conn;
        }
        iter++;
    }

    // create a new connections if there is space in the pool
    if (connections.size() < connections.capacity()) {
        // use current size as connection id
        DBConn *conn = new DBConn(connections.size());

#ifdef DEBUG
        auto start = std::chrono::steady_clock::now();
#endif

        conn->conn = new pqxx::connection(con_str);

#ifdef DEBUG
        auto end = std::chrono::steady_clock::now();
        auto tm = std::chrono::duration<double, std::milli>(
            end - start
        ).count();
        std::ostringstream oss;
        oss << "[DBPool] connection no."
            << std::to_string(connections.size()+1)
            << " created in " << tm << " ms";
        Logger::debug(oss.str());
#endif

        conn->__priv_available = false;
        connections.push_back(conn); // add to pool
        conn_mutex.unlock();
        return conn;
    }

    conn_mutex.unlock();

    return boost::none;
}


/**
 * Get a connection. Blocks until one becomes available.
 *
 * @param promise a promise to return the connection in a future
 */
void DBPool::get (boost::promise<DBConn *> promise) {

    if (!inited)
        throw std::runtime_error("[DBPool] not initialized");

    // verify if some connection is available right now
    boost::optional<DBConn *> cn = get_nonblock();
    if (cn) {
        promise.set_value(*cn);
        return;
    }

    cli_mutex.lock();
    try {
        clients.push(std::move(promise));
    } catch (std::exception& ex) {
        using namespace std::string_literals;
        cli_mutex.unlock();
        throw std::runtime_error(
            "[DBPool] unable to add client to queue: "s + ex.what()
        );
    }

#ifdef DEBUG
    Logger::debug(
        "[DBPool] client queue has " + std::to_string(clients.size()) +
        " promises"
    );
#endif

    cli_mutex.unlock();
    return;
}


/**
 * Release a connection.
 *
 * @param conn the connection to release
 */
void DBPool::release (DBConn *conn) {

    if (!inited)
        throw std::runtime_error("[DBPool] not initialized");

    if (!conn || !(conn->conn))
        throw std::runtime_error(
            "[DBPool] release called for a null connection"
        );

    // verify that connection belongs to this pool
    bool belongs = false;
    for (auto cn : connections) {
        if (cn->__priv_id == conn->__priv_id) {
            belongs = true;
            break;
        }
    }
    if (!belongs)
        throw std::runtime_error(
            "[DBPool] received connection that does not belong to this pool"
        );

    // check to see if there is any client waiting
    cli_mutex.lock();
    if (!clients.empty()) {
        boost::promise<DBConn *> conn_p = std::move(clients.front());
        clients.pop();
        cli_mutex.unlock();

#ifdef DEBUG
        Logger::debug(
            "[DBPool] conn no." + std::to_string(conn->__priv_id+1) +
            " used immediatly after release"
        );
#endif

        conn->__priv_available = false; // make sure the value is false
        conn_p.set_value(conn);
        return;
    }
    cli_mutex.unlock();

    // make the connection available
    conn->__priv_available = true;

#ifdef DEBUG
        Logger::debug(
            "[DBPool] conn no." + std::to_string(conn->__priv_id+1) + " released"
        );
#endif

}
