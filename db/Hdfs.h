#ifndef __HDFS_OURICO_HEAD__
#define __HDFS_OURICO_HEAD__


#include <hadoop/hdfs.h>
#include <string>
#include <cstring>
#include <cerrno>
#include <stdexcept>
#include <sstream>

#include "util/Logger.h"



/**
 * Functions to perform operations on HDFS
 */
namespace Hdfs {

namespace {
    hdfsFS fs_connect;
    bool inited = false;
};


/**
 * Each mote will be a directory with 5 files in it.
 * The files are the following:
 */
enum FileName {
    MEASUREMENTS,
    ACTION_RESULTS,
    ALERTS,
    TRIGGERS,
    STATUS_REPORTS,
    ERRORS
};


/**
 * Information associated with an open file
 *
 * @member file_handle the connection handle returned by libhdfs
 * @member file_path path to the file
 */
struct File {
    hdfsFile    file_handle;
    std::string file_path;
};


/**
 * Makes the connection to HDFS
 *
 * @warning expection is thrown in case of error
 */
void init ();


/**
 * Open a file from a certain mote
 *
 * @param mote_id
 * @param filename what file from the mote to open
 *
 * @return handle to the file
 * @warning expection is thrown in case of error
 */
File open_mote_file (int mote_id, FileName filename);


/**
 * Close an open file
 *
 * @param file file handle of the file to close
 * @warning expection is thrown in case of error
 */
void close_mote_file (File file);


/**
 * Only append operations are supported from this module
 *
 * @param file file handle of the file where to write
 * @data content to be written to the file
 *
 * @warning exception is thrown in case of error
 */
void append_to_file (File file, std::string data);


/**
 * Flush data in buffers
 *
 * @param file
 *
 * @warning exception is thrown in case of error
 */
void flush_file (File file);


/**
 * Combine open and append in a single operation
 *
 * @warning exception is thrown in case of error
 */
void open_and_append (int mote_id, FileName filename, std::string data);

};



#endif // __HDFS_OURICO_HEAD__
