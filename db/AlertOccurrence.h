#ifndef __ALERTOCCURRENCE_HEAD__
#define __ALERTOCCURRENCE_HEAD__


#include "Tables.h"

#include <string>
#include <sstream>

#include <pqxx/pqxx>


namespace Tables {

    struct AlertOccurrence : Table {

        int alert_num;
        int mote;
        std::string date_time;

        AlertOccurrence () : Table(
            "alert_occurrence",
            "(alert_id, mote_id, date_time)"
        ) {};
        std::string read_by_id_sql ();
        void wrap_result (const pqxx::tuple);
        std::string to_string ();
        std::string as_csv_line ();

    };

};


#endif // __ALERTOCCURRENCE_HEAD__
