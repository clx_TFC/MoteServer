#include "StatusReport.h"


std::string Tables::StatusReport::read_by_id_sql () {
    std::ostringstream oss;
    oss << "select " << this->fields << " from " << this->name << " where mote_id = "
        << this->mote << " and start_date_time = '"
        << this->start_date_time << "'";
    return oss.str();
}


void Tables::StatusReport::wrap_result (const pqxx::tuple t) {
    if (!t[0].is_null())
        this->mote = t[0].as<int>();
    if (!t[1].is_null())
        this->start_date_time = t[1].as<std::string>();
    if (!t[2].is_null())
        this->end_date_time = t[2].as<std::string>();
    if (!t[3].is_null())
        this->nr_tx_packets = t[3].as<int>();
    if (!t[4].is_null())
        this->nr_lost_packets = t[4].as<int>();
    if (!t[5].is_null())
        this->nr_tx_bytes = t[5].as<int>();
    if (!t[6].is_null())
        this->nr_lost_bytes = t[6].as<int>();
}


std::string Tables::StatusReport::to_string () {
    std::ostringstream oss;
    oss << "(" << this->mote << ", '"
        << this->start_date_time << "', '"
        << this->end_date_time << "', "
        << this->nr_tx_packets << ", "
        << this->nr_lost_packets << ", "
        << this->nr_tx_bytes << ", "
        << this->nr_lost_bytes << ")";
    return oss.str();
}

std::string Tables::StatusReport::as_csv_line () {
    std::ostringstream oss;
    oss << this->mote << ","
        << this->start_date_time << ","
        << this->end_date_time << ","
        << this->nr_tx_packets << ","
        << this->nr_lost_packets << ","
        << this->nr_tx_bytes << ","
        << this->nr_lost_bytes << "\n";
    return oss.str();
}
