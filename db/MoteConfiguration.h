#include "Tables.h"

#include "util/Misc.h"

#include <vector>
#include <string>
#include <pqxx/tuple>

namespace Tables {
    struct MoteConfiguration : Table {

        std::string name;
        int mote;
        int measuremet_period;
        int send_data_period;
        int status_report_period;
        bool use_crypto;
        std::string current_ip;
        bool active;

        MoteConfiguration () : Table(
            "mote_configuration",
            "(mote_id, measuremet_period, send_data_period, status_report_period, "
            "use_crypto, current_ip, active)"
        ) {};
        std::string read_by_id_sql ();
        void wrap_result (const pqxx::tuple);
        std::string to_string ();

        // // void set_name (std::string);
        // std::string get_name () { return this->name; }
        //
        // // void set_mote (int);
        // int get_mote () { return  this->mote; }
        //
        // // void set_measurement_period (int);
        // int get_measurement_period () { return this->measuremet_period; }
        //
        // // void set_send_data_period (int);
        // int get_send_data_period () { return this->send_data_period; }
        //
        // // void set_status_report_period (int);
        // int get_status_report_period () { return this->status_report_period; }
        //
        // // void set_use_crypto (bool);
        // bool get_use_crypto () { return this->use_crypto; }
        //
        // // void set_current_ip (std::string);
        // std::string get_current_ip () { return this->current_ip; }
        //
        // // void set_nr_lost_packets_retry (int);
        // int get_nr_lost_packets_retry () { return this->nr_lost_packets_retry; }
        //
        // // void set_active (bool);
        // int get_active () { return this->active; }

    };
}
