#include "TriggerOccurrence.h"


std::string Tables::TriggerOccurrence::read_by_id_sql () {
    std::ostringstream oss;
    oss << "select " << this->fields << " from " << this->name
        << " where trigger_id = " << this->trigger_num
        << " and mote_id = " << this->mote
        << " and date_time = '" << this->mote << "'";
    return oss.str();
}

void Tables::TriggerOccurrence::wrap_result (const pqxx::tuple t) {
    if (!t[0].is_null())
        this->trigger_num = t[0].as<int>();
    if (!t[1].is_null())
        this->mote = t[1].as<int>();
    if (!t[2].is_null())
        this->date_time = t[2].as<std::string>();
}

std::string Tables::TriggerOccurrence::to_string () {
    std::ostringstream oss;
    oss << "(" << this->trigger_num
        << ", " << this->mote
        << ", '" << this->date_time << "')";
    return oss.str();
}


std::string Tables::TriggerOccurrence::as_csv_line () {
    std::ostringstream oss;
    oss << this->trigger_num
        << "," << this->mote
        << "," << this->date_time << "\n";
    return oss.str();
}
