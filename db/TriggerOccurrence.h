#ifndef __TRIGGEROCCURRENCE_HEAD__
#define __TRIGGEROCCURRENCE_HEAD__


#include "Tables.h"

#include <string>
#include <sstream>

#include <pqxx/pqxx>


namespace Tables {

    struct TriggerOccurrence : Table {

        int trigger_num;
        int mote;
        std::string date_time;

        TriggerOccurrence () : Table(
            "trigger_occurrence",
            "(trigger_id, mote_id, date_time)"
        ) {};
        std::string read_by_id_sql ();
        void wrap_result (const pqxx::tuple);
        std::string as_csv_line ();
        std::string to_string ();

    };

};


#endif // __TRIGGEROCCURRENCE_HEAD__
