#ifndef __OURICO_PACKET_HEAD__
#define __OURICO_PACKET_HEAD__


#include "Tables.h"

#include <string>
#include <sstream>

#include <pqxx/pqxx>


namespace Tables {

    struct Packet : Table {

        std::string date_time;
        int mote_id;
        int packet_type;
        int length;
        int packet_nr;

        Packet () : Table(
            "packet",
            "(date_time, mote_id, packet_type, length, packet_nr)"
        ) {};
        std::string read_by_id_sql ();
        void wrap_result (const pqxx::tuple);
        std::string to_string ();
        std::string as_csv_line ();

    };

};


#endif // __OURICO_PACKET_HEAD__
