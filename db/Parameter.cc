#include "Parameter.h"


std::string Tables::Parameter::read_by_id_sql () {
    std::ostringstream oss;
    oss << "select " << this->fields << " from " << this->name
        << " where id = " << this->id;
    return oss.str();
}


void Tables::Parameter::wrap_result (const pqxx::tuple t) {
    if (!t[0].is_null())
        this->id = t[0].as<int>();
    if (!t[1].is_null())
        this->name = t[1].as<std::string>();
    if (!t[2].is_null())
        this->units = t[2].as<std::string>();
    if (!t[3].is_null())
        this->type = t[3].as<std::string>();
    if (!t[4].is_null())
        this->description = t[4].as<std::string>();
}


std::string Tables::Parameter::to_string () {
    std::ostringstream oss;
    oss << "(" << this->id << ", '"
        << this->name << "', '"
        << this->units << "', '"
        << this->type << "', '"
        << this->description << "')";
    return oss.str();
}
