#ifndef __PARAMETER_HEAD__
#define __PARAMETER_HEAD__


#include "Tables.h"

#include <string>
#include <sstream>

#include <pqxx/pqxx>


namespace Tables {

    struct Parameter : Table {

        int id;
        std::string name;
        std::string units;
        std::string type;
        std::string description;

        Parameter () : Table(
            "parameter",
            "(id, name, units, type, description)"
        ) {};
        std::string read_by_id_sql ();
        void wrap_result (const pqxx::tuple);
        std::string to_string ();

    };

};


#endif // __PARAMETER_HEAD__
