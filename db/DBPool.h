#ifndef __DBPOOL_HEAD__
#define __DBPOOL_HEAD__


#include <vector>
#include <queue>
#include <mutex>
#include <atomic>
#include <string>
#include <chrono>
#include <sstream>

#define BOOST_THREAD_PROVIDES_FUTURE
#include <boost/thread/future.hpp>
#include <boost/optional.hpp>
#include <pqxx/pqxx>

#include "util/Logger.h"
#include "util/Conf.h"


namespace DBPool {

struct DBConn {

    // XXX:  shoul not be visible outside namespace
    std::atomic<bool> __priv_available;
    std::atomic<int>  __priv_id;

    // XXX:  shoul not be visible outside namespace
    DBConn (int i) : __priv_available(true), __priv_id(i) {};

    pqxx::connection *conn;

};

namespace {
    // pool of connections
    std::vector<DBConn *> connections;
    // mutex for the connections data structure
    std::mutex conn_mutex;

    // clients waiting for connections
    std::queue<boost::promise<DBConn *>> clients;
    // mutex for the clients queue
    std::mutex cli_mutex;

    // connection string to use to create connections
    std::string con_str;
    bool inited = false;
};


void init ();

// if a connection is not available the function
// will block until one becomes
void get (boost::promise<DBConn *>);
// if a connection is not available None will be returned
boost::optional<DBPool::DBConn *> get_nonblock ();
// release a connection from a client
void release (DBConn *);

}


#endif // __DBPOOL_HEAD__
