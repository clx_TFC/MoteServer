#!/usr/bin/env python3

from math import *

# lmb = rate of arrival
# mu  = rate of service
# s   = number of servers/workers
def mms_queue_perf (lmb, mu, s) :
    print("lmb = ", lmb)
    print("mu  = ", mu)
    print("s   = ", s)

    lu = lmb/mu
    ro = lmb/(s*mu)
    print("ro = ", ro)
    # probabilty of zero elements in the queue
    p0 = (pow(lu, s)*s*mu)/(factorial(s)*(s*mu - lmb))
    for n in range(s) :
        p0 += (pow(lu, n)/factorial(n))
    p0 = 1/p0
    print("p0 = ", p0)

    # average length of queue
    lq = (p0*ro*pow(lu, s))/(factorial(s)*pow(1-ro, 2))

    print('Average length of queue: {}'.format(lq))
    print('Average wait time      : {}'.format(lq/lmb))


if __name__ == '__main__':
    while True:
        lmb = float(input('lmb: '))
        mu  = float(input('mu : '))
        s   = int(input('s  : '))
        mms_queue_perf(lmb, mu, s)
