#include "Mote.h"


std::string Tables::Mote::read_by_id_sql () {
    std::ostringstream oss;
    oss << "select " << this->fields << " from " << this->name << " where id = "
        << this->id;
    return oss.str();
}

void Tables::Mote::wrap_result (const pqxx::tuple t) {
    if (!t[0].is_null())
        this->id                  = t[0].as<int>();
    if (!t[1].is_null())
        this->location_lat        = t[1].as<double>();
    if (!t[2].is_null())
        this->location_lng        = t[2].as<double>();
    if (!t[3].is_null())
        this->active              = t[2].as<bool>();
    if (!t[4].is_null())
        this->current_ip          = t[4].as<std::string>();
    if (!t[5].is_null())
        this->last_status_report  = t[5].as<std::string>();
    if (!t[6].is_null())
        this->crypto_key          = t[6].as<std::string>();
    if (!t[7].is_null())
        this->crypto_last_nonce_in = t[6].as<int>();
    if (!t[8].is_null())
        this->crypto_last_nonce_out = t[6].as<int>();
    if (!t[9].is_null())
        this->purpose_description = t[7].as<std::string>();
    if (!t[10].is_null())
        this->other_data          = t[8].as<std::string>();
}

std::string Tables::Mote::to_string () {
    std::ostringstream oss;
    oss << "(" << this->id << ", "
        << this->location_lat << ", "
        << this->location_lng << ", "
        << this->active << ", '"
        << this->current_ip << "', '"
        << this->last_status_report << "', '"
        << this->crypto_key << "', "
        << this->crypto_last_nonce_in << ", "
        << this->crypto_last_nonce_out << ", '"
        << this->purpose_description << "', '"
        << this->other_data << "')";
    return oss.str();
}
