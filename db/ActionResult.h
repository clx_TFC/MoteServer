#ifndef __ACTIONRESULT_HEAD__
#define __ACTIONRESULT_HEAD__


#include "Tables.h"

#include <string>
#include <sstream>

#include <pqxx/pqxx>


namespace Tables {

    struct ActionResult : Table {

        int action_num;
        int actuator_use_num;
        int actuator;
        int mote;
        std::string date_time;
        int result;

        ActionResult () : Table(
            "action_result",
            "(action_num, actuator_use_num, actuator, mote, date_time, result)"
        ) {};
        std::string read_by_id_sql ();
        void wrap_result (const pqxx::tuple);
        std::string as_csv_line ();
        std::string to_string ();
    };

};


#endif // __ACTIONRESULT_HEAD__
