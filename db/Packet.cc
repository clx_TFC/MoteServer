#include "Packet.h"


std::string Tables::Packet::read_by_id_sql () {
    std::ostringstream oss;
    oss << "select " << this->fields << " from " << this->name
        << " where date_time = '" << this->date_time << "'";
    return oss.str();
}

void Tables::Packet::wrap_result (const pqxx::tuple t) {
    if (!t[0].is_null())
        this->date_time = t[0].as<std::string>();
    if (!t[1].is_null())
        this->mote_id = t[1].as<int>();
    if (!t[2].is_null())
        this->packet_type = t[2].as<int>();
    if (!t[2].is_null())
        this->length = t[2].as<int>();
    if (!t[2].is_null())
        this->packet_nr = t[2].as<int>();
}


std::string Tables::Packet::to_string () {
    std::ostringstream oss;
    oss << "('" << this->date_time << "', "
        << this->mote_id << ", "
        << this->packet_type << ", "
        << this->length << ", "
        << this->packet_nr << ")";
    return oss.str();
}

std::string Tables::Packet::as_csv_line () {
    std::ostringstream oss;
    oss << this->date_time << ", "
        << this->mote_id << ", "
        << this->packet_type << ", "
        << this->length << ", "
        << this->packet_nr;
    return oss.str();
}
