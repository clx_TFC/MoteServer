#ifndef __MOTEERROR_HEAD__
#define __MOTEERROR_HEAD__

#include "Tables.h"

#include <string>
#include <sstream>

#include <pqxx/pqxx>


namespace Tables {
    struct MoteError : Table {
    public:

        std::string date_time;
        int mote_id;
        int err_code;
        std::string err_msg;

        MoteError () : Table(
            "mote_error",
            "(date_time, mote_id, err_code, err_message)"
        ) {};
        std::string read_by_id_sql ();
        void wrap_result (const pqxx::tuple);
        std::string as_csv_line ();
        std::string to_string ();

        std::string get_date_time () { return this->date_time; }
        int get_mote_id () { return this->mote_id; }
        int get_err_code () { return this->err_code; }
        std::string get_err_msg () { return this->err_msg; }

    };
};


#endif // __MOTEERROR_HEAD__
