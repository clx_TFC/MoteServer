#pragma once
#ifndef __SERVER_SOCKET_HEAD__
#define __SERVER_SOCKET_HEAD__

#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#include "Socket.h"

class ServerSocket : public Socket {
private:
  int backlog;

public:
  ServerSocket (
    const char *_porta,
    int _backlog=100, // nr maximo de conexoes em espera
    Socket::VersaoIP versao  = Socket::VersaoIP::IPV4
  );

  // bloqueia ah espera de uma conexao
  Socket receber_conexao ();

};

#endif // __SERVER_SOCKET_HEAD__
