#include "Socket.h"


const char *Socket::LOCALHOST = "localhost";
const std::size_t Socket::TAM_DADOS_A_RECEBER = 1024;

using namespace std::string_literals;


int Socket::criar_socket(const char *_endereco, const char *_porta, int _familia,
                     int _tipo_socket, Socket::Operacao op)
{

  int _sock_fd;
  // propriedades do endereco
  struct addrinfo dicas, *addr;

  memset(&dicas, 0, sizeof (struct addrinfo));
  // algumas dicas para que se possa obter o endereco que queremos
  dicas.ai_family   = _familia;
  dicas.ai_socktype = _tipo_socket;
  dicas.ai_flags    =
    // AI_PASSIVE significa associar a qq endereco nesta maquina
    // util apenas para servidor
    op == Operacao::SERVIDOR ? AI_PASSIVE : 0;
  // se for socket servidor _endereco deve ser passado como NULL
  _endereco = op == Operacao::SERVIDOR ? NULL : _endereco;

  int res = getaddrinfo(_endereco, _porta, &dicas, &addr);
  if (res != 0)
    throw std::runtime_error("Erro ao obter dados do endereco: "s + gai_strerror(res));

  // ligar a um dos enderecos disponiveis
  struct addrinfo *i;
  for (i = addr; i != NULL; i = i->ai_next) {
    if ((_sock_fd = socket(i->ai_family, i->ai_socktype, i->ai_protocol)) == -1)
      continue;

    if (op == Operacao::SERVIDOR) {
      // associar a um endereco e porta neste computador de modo a ouvir conexoes
      res = bind(_sock_fd, i->ai_addr, i->ai_addrlen);
    } else {
      // conectar a um endereco remoto
      res = connect(_sock_fd, i->ai_addr, i->ai_addrlen);
    }

    if (res == -1) {
      close(_sock_fd);
      continue;
    }

    break;
  }

  if (!i) { // nao se consegui conectar
    freeaddrinfo(addr);
    throw std::runtime_error("Conexão não é possivel: "s + strerror(errno));
  }

  freeaddrinfo(addr);
  return _sock_fd;
}

Socket::Socket (const char *_endereco, const char *_porta, VersaoIP versao,
                ProtocoloTransporte prot)
{
  int _tipo_socket =
    prot == ProtocoloTransporte::TCP ? SOCK_STREAM : SOCK_DGRAM;
  int _familia     =
    versao == VersaoIP::IPV4 ? AF_INET : AF_INET6;
  // um socket cliente
  this->sock_fd = this->criar_socket(_endereco, _porta, _familia, _tipo_socket,
                            Socket::Operacao::CLIENTE);
}

// construtor protegido
Socket::Socket (const char *_porta, VersaoIP versao, ProtocoloTransporte prot)
{
  int _tipo_socket =
    prot == ProtocoloTransporte::TCP ? SOCK_STREAM : SOCK_DGRAM;
  int _familia     =
    versao == VersaoIP::IPV4 ? AF_INET : AF_INET6;
  // um socket servidor
  this->sock_fd = this->criar_socket(NULL, _porta, _familia, _tipo_socket,
                            Socket::Operacao::SERVIDOR);
}

Socket::Socket (int _sock_fd) {
  // verificar se o fd eh valido
  if (fcntl(_sock_fd, F_GETFD) == -1)
    throw std::runtime_error("Não é possivel criar socket a partir de fd inválido");

  this->sock_fd = _sock_fd;
}

Socket::~Socket () {
  // close(this->sock_fd);
}


ssize_t Socket::enviar_mensagem (const char *mensagem, size_t tam_mensagem) {
  if (!mensagem)
    throw std::runtime_error("Argumento inválido passado a [Socket::enviar_mensagem]");

  ssize_t bytes_escritos = 0;

  // if ((bytes_escritos = write(this->sock_fd, mensagem, tam_mensagem)) == -1)
  //   throw std::runtime_error("Erro ao enviar dados: "s + strerror(errno));

  if ((bytes_escritos = send(this->sock_fd, mensagem, tam_mensagem, 0)) == -1)
    throw std::runtime_error("Erro ao enviar dados: "s + strerror(errno));

  return bytes_escritos;
}

ssize_t Socket::enviar_ficheiro (int fd_ficheiro, std::size_t tam_fich) {
  return sendfile(this->sock_fd, fd_ficheiro, NULL, tam_fich);
}

ssize_t Socket::receber_para_ficheiro (const char *nome_ficheiro) {

  Resposta res = this->receber_mensagem();

  if (strncmp("Erro", res.resposta, 4) == 0)
    throw std::runtime_error("Erro enviado pelo servidor: "s + res.resposta);

  // criar novo ficheiro com nome 'nome_ficheiro'
  // e guardar dados recebidos
  std::ofstream ficheiro = std::ofstream(nome_ficheiro, std::ios::binary);
  if (!ficheiro) {
    std::cout << "Erro ao criar novo ficheiro " << nome_ficheiro << std::endl;
    return -1;
  }

  for (int i = 0; i < res.tam_resposta; i++)
    ficheiro << res.resposta[i];

  ficheiro.close();

  return res.tam_resposta;
}

Socket::Resposta Socket::receber_mensagem (std::size_t tam_dados) {
  char *buffer = static_cast<char *>(malloc(tam_dados));
  ssize_t tam_recebido = 0;
  // if ((tam_recebido = read(this->sock_fd, buffer, tam_dados)) == -1)
  //   throw std::runtime_error("Erro ao ler dados: "s + strerror(errno));

  if ((tam_recebido = recv(this->sock_fd, buffer, tam_dados, 0)) == -1) {
      free(buffer);
      throw std::runtime_error("Erro ao ler dados: "s + strerror(errno));
  }
  // descartar os bytes a mais no buffer
  Resposta r;
  r.tam_resposta = tam_recebido;
  r.resposta = static_cast<char *>(malloc(tam_recebido));
  memset(r.resposta, 0, tam_recebido);
  memcpy(r.resposta, buffer, tam_recebido);
  free(buffer);

  return r;
}

void Socket::fechar () {
  close(this->sock_fd);
}

Socket::Info Socket::obter_info () {
  socklen_t tam_addr = sizeof(struct sockaddr_storage);
  struct sockaddr_storage addr;
  // obter e endereco ip e porta associados a este socket
  if ((getsockname(this->sock_fd, (struct sockaddr*) &addr, &tam_addr)) == -1) {
    std::cerr << "Erro ao obter info do socket: " << strerror(errno) << std::endl;
    return Info(NULL, NULL);
  }
  char *endereco = (char *) malloc(NI_MAXHOST);
  char *servico  = (char *) malloc(NI_MAXSERV);
  // obter as representacoes em strings do endereco e do servico associado a porta
  if ((getnameinfo((struct sockaddr *) &addr,
       tam_addr, endereco, NI_MAXHOST, servico, NI_MAXSERV, 0)) == -1) {
    std::cerr << "Erro ao obter info do socket: " << strerror(errno) << std::endl;
    free(endereco);
    free(servico);
    return Info(NULL, NULL);
  }

  return Info(endereco, servico);
}
