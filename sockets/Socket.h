#pragma once
#ifndef __SOCKET_HEAD__
#define __SOCKET_HEAD__

#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/sendfile.h>
#include <netinet/in.h>
#include <netdb.h>

#include <stdexcept>
#include <cstring>
#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>


class Socket {
private:
  // operacao do servidor
  // apenas util para subclasse mudar comportamento do construtor
  enum class Operacao { CLIENTE, SERVIDOR };

  int criar_socket (
    const char *_endereco,
    const char *_porta,
    int _familia,
    int _tipo_socket,
    Operacao op
  );

public:

  enum class VersaoIP : int {
    IPV4 = AF_INET,
    IPV6 = AF_INET6
  };

  enum class ProtocoloTransporte : int {
    TCP = SOCK_STREAM,
    UDP = SOCK_DGRAM,
  };

  const static char *LOCALHOST; // = "localhost"

  // encapsula resposta a uma chamada a receber_mensagem
  struct Resposta {
    char *resposta = nullptr;
    ssize_t tam_resposta;

    Resposta () {};
    Resposta (char *res, ssize_t tam) {
      resposta     = res;
      tam_resposta = tam;
    }

    void dispose () {
        if (resposta) {
            free(resposta);
            resposta = nullptr;
        }
    }
  };

  struct Info {
    char *endereco = nullptr;
    char *porta    = nullptr;

    Info (char *end, char *prt) {
      endereco  = end;
      porta     = prt;
    }

    void dispose () {
        if (endereco) {
            free(endereco);
            endereco = nullptr;
        }
        if (porta) {
            free(porta);
            porta = nullptr;
        }
    }
  };

  // lanca uma excepcao em caso de falha
  Socket (
    const char *_endereco,
    const char *_porta,
    VersaoIP versao          = VersaoIP::IPV4,
    ProtocoloTransporte prot = ProtocoloTransporte::TCP
  );

  // adiciona estrutura a um socket ja aberto
  // lanca uma excepcao em caso de falha
  Socket (int _sock_fd);

  // lanca uma excepcao em caso de falha
  Socket (Socket::Info info) : Socket(info.endereco, info.porta) {};

  ~Socket ();

  // lanca uma excepcao em caso de falha
  ssize_t enviar_mensagem (const char *mensagem, size_t tam_mensagem);
  // wraper em torno do sendfile
  ssize_t enviar_ficheiro (int fd, size_t tam_fich);
  // lanca excepcao em caso de falha
  Resposta receber_mensagem (size_t tam_dados=Socket::TAM_DADOS_A_RECEBER);
  // le dados do socket e escreve-os diretamente para um ficheiro
  // cria o ficheiro se nao existir
  // lanca excepcao
  ssize_t receber_para_ficheiro (const char *nome_ficheiro);
  // info do socket
  Info obter_info ();

  int get_fd () { return sock_fd; }

  void fechar ();

protected:
  // file descriptor do socket (identificador)
  int sock_fd;
  // tamanho default de dados a receber
  const static size_t TAM_DADOS_A_RECEBER;

  // apenas uma subclasse tem acesso a este construtor, caso quiser
  // um socket servidor
  Socket (
    const char *_porta,
    VersaoIP versao,
    ProtocoloTransporte prot
  );
};



#endif // __SOCKET_HEAD__
