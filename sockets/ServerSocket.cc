#include "ServerSocket.h"

#include <stdexcept>
#include <cstring>
#include <cstdlib>

using namespace std::string_literals;

ServerSocket::ServerSocket (const char *_porta, int _backlog, VersaoIP versao)
    : Socket(_porta, versao, Socket::ProtocoloTransporte::TCP)
{
  this->backlog = _backlog;
  if (listen(this->sock_fd, this->backlog) == -1) // passar a aceitar conexoes
    throw std::runtime_error("Erro ao definir socket como servidor: "s + strerror(errno));
}

Socket ServerSocket::receber_conexao () {
  int cliente_sock_fd = accept(this->sock_fd, NULL, NULL);
  if (cliente_sock_fd == -1)
    throw std::runtime_error("Erro ao aceitar conexão: "s + strerror(errno));

  return Socket(cliente_sock_fd);
}
