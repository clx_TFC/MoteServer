CC    = clang++
FLAGS = -std=c++14 -Wall -Werror -O3 -I . -DDEBUG
OUT   = out
TESTS = tests
EXT_LIBS = -lyaml-cpp -lpqxx -pthread -lcpp_redis -ltacopie \
            -lboost_system -lboost_date_time -lboost_thread \
			-levpp -lglog -lgflags -levent -lcryptopp

CP=$(shell hadoop classpath --glob)

PROT_DEPS = $(OUT)/Protocol.o $(OUT)/Crypto.o
SOCK_DEPS = $(OUT)/Socket.o $(OUT)/ServerSocket.o
DB_DEPS   = $(OUT)/DB.o $(OUT)/DBPool.o\
			$(OUT)/Mote.o $(OUT)/MoteError.o\
            $(OUT)/MoteConfiguration.o $(OUT)/Sensor.o \
			$(OUT)/ActionResult.o $(OUT)/Measurement.o \
			$(OUT)/Parameter.o $(OUT)/Packet.o \
			$(OUT)/AlertOccurrence.o $(OUT)/TriggerOccurrence.o \
			$(OUT)/StatusReport.o
UTIL_DEPS = $(OUT)/Conf.o $(OUT)/Misc.o $(OUT)/Logger.o
INT_SERVER_COMM_DEPS = $(OUT)/OutChannel.o \
		       $(OUT)/Redis.o $(OUT)/MoteMessageRequests.o\
               $(OUT)/ErrorOutChannel.o

SERVER_DEPS = $(OUT)/Server.o $(OUT)/Handlers.o $(OUT)/MoteStatus.o

DEPS = $(SOCK_DEPS) \
	   $(PROT_DEPS) \
	   $(DB_DEPS) \
	   $(UTIL_DEPS) \
	   $(INT_SERVER_COMM_DEPS) \
	   $(SERVER_DEPS)

all: clean util_libs socket_libs protocol_libs \
	 int_server_comm_libs db_libs server_libs \
	 main

main:
	$(CC) $(FLAGS) $(EXT_LIBS) \
	 main.cc \
	 $(DEPS) -o $(OUT)/main

socket_libs:
	$(CC) $(FLAGS) -c sockets/Socket.cc -o $(OUT)/Socket.o
	$(CC) $(FLAGS) -c sockets/ServerSocket.cc -o $(OUT)/ServerSocket.o

protocol_libs:
	$(CC) $(FLAGS) -c protocol/Protocol.cc -o $(OUT)/Protocol.o
	$(CC) $(FLAGS) -c protocol/Crypto.cc -o $(OUT)/Crypto.o

db_libs:
	$(CC) $(FLAGS) -c db/DBPool.cc -o $(OUT)/DBPool.o
	$(CC) $(FLAGS) -c db/DB.cc -o $(OUT)/DB.o
	$(CC) $(FLAGS) -c db/Mote.cc -o $(OUT)/Mote.o
	$(CC) $(FLAGS) -c db/MoteError.cc -o $(OUT)/MoteError.o
	$(CC) $(FLAGS) -c db/MoteConfiguration.cc -o \
	 $(OUT)/MoteConfiguration.o
	$(CC) $(FLAGS) -c db/Sensor.cc -o $(OUT)/Sensor.o
	$(CC) $(FLAGS) -c db/Packet.cc -o $(OUT)/Packet.o
	$(CC) $(FLAGS) -c db/ActionResult.cc -o $(OUT)/ActionResult.o
	$(CC) $(FLAGS) -c db/Measurement.cc -o $(OUT)/Measurement.o
	$(CC) $(FLAGS) -c db/Parameter.cc -o $(OUT)/Parameter.o
	$(CC) $(FLAGS) -c db/AlertOccurrence.cc -o $(OUT)/AlertOccurrence.o
	$(CC) $(FLAGS) -c db/TriggerOccurrence.cc -o $(OUT)/TriggerOccurrence.o
	$(CC) $(FLAGS) -c db/StatusReport.cc -o $(OUT)/StatusReport.o

util_libs:
	$(CC) $(FLAGS) -c util/Conf.cc -o $(OUT)/Conf.o
	$(CC) $(FLAGS) -c util/Misc.cc -o $(OUT)/Misc.o
	$(CC) $(FLAGS) -c util/Logger.cc -o $(OUT)/Logger.o

server_libs:
	$(CC) $(FLAGS) -c server/Server.cc -o $(OUT)/Server.o
	$(CC) $(FLAGS) -c server/Handlers.cc -o $(OUT)/Handlers.o
	$(CC) $(FLAGS) -c mote_status/MoteStatus.cc -o $(OUT)/MoteStatus.o


int_server_comm_libs:
	$(CC) $(FLAGS) -c inter_comm/OutChannel.cc \
	 -o $(OUT)/OutChannel.o
	$(CC) $(FLAGS) -c inter_comm/ErrorOutChannel.cc \
	 -o $(OUT)/ErrorOutChannel.o
	$(CC) $(FLAGS) -c inter_comm/Redis.cc \
	  -o $(OUT)/Redis.o
	$(CC) $(FLAGS) -c inter_comm/MoteMessageRequests.cc \
	  -o $(OUT)/MoteMessageRequests.o

tests_protocol:
	 $(CC) $(FLAGS) $(TESTS)/protocol_tests.cc -l pqxx -l yaml-cpp \
	 $(OUT)/Protocol.o $(OUT)/DB.o $(OUT)/Conf.o -o $(TESTS)/ptests

test_crypto:
	$(CC) $(FLAGS) $(TESTS)/crypto_test.cc $(UTIL_DEPS) $(PROT_DEPS) $(DB_DEPS) \
	 $(EXT_LIBS) -o $(TESTS)/crypto_test


tests_inter_comm:
	$(CC) $(FLAGS) $(TESTS)/inter_comm_tests.cc \
	 $(EXT_LIBS)\
	 $(INT_SERVER_COMM_DEPS) $(UTIL_DEPS) -o $(TESTS)/ttest

tests_db:
	$(CC) $(FLAGS) $(TESTS)/db_tests.cc \
	 $(EXT_LIBS) $(OUT)/DB.o $(OUT)/DBPool.o $(OUT)/Cache.o \
	 $(UTIL_DEPS) -o $(TESTS)/dbtest

tests_db_pool:
	$(CC) $(FLAGS) $(TESTS)/db_pool_tests.cc \
	 $(EXT_LIBS) $(OUT)/DBPool.o $(OUT)/Cache.o \
	 $(UTIL_DEPS) -o $(TESTS)/dbp

tests_packet_generator:
	$(CC) $(FLAGS) $(TESTS)/packet_generator.cc \
	 -lyaml-cpp -lpqxx -lhiredis\
	 $(SOCK_DEPS) $(UTIL_DEPS) $(PROT_DEPS) $(DB_DEPS)\
	 -o $(TESTS)/pg

tests_sock:
	$(CC) $(FLAGS) $(TESTS)/sock.cc \
	 -lyaml-cpp \
	 $(SOCK_DEPS) $(UTIL_DEPS) \
	 -o $(TESTS)/sock

run_tests_inter_comm:
	./$(TESTS)/ttest 2 2

clean:
	rm -f $(OUT)/*

run:
	./out/main
