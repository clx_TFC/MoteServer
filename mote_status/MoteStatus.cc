#include "mote_status/MoteStatus.h"


void MoteStatus::init () {
    Conf *config = Conf::getInstance();
    auto _mote_status_check_period = config->get("mote_status_check_period");

    if (_mote_status_check_period.IsNull())
        throw std::runtime_error(
            "[MoteStatus] mote_status_check_period must be set in configuration file"
        );

    mote_status_check_period = _mote_status_check_period.as<int>();

    // set timer to check motes that haven't sent status reports
    io_service = new boost::asio::io_service();
    timer      = new boost::asio::deadline_timer(*io_service);
    set_status_check_timer();
    // run the timer in its own thread
    boost::thread([&](){ io_service->run(); });

    Logger::log("[MoteStatus] initialized", Logger::Color::blue);
    Logger::log(
        "[MoteStatus] with mote_status_check_period of " +
        std::to_string(mote_status_check_period) + " seconds"
    );

}


void MoteStatus::check_mote_status (const boost::system::error_code& e) noexcept {

    if (e) {
        Logger::err("[MoteStatus] check_mote_status timer failed/aborted: " + e.message());
        return;
    }

#ifdef DEBUG
    Logger::debug("[MoteStatus] check mote status ");
    auto start = std::chrono::steady_clock::now();
#endif

    using namespace std::string_literals;
    try {

        std::string sql =
            // set the inactive state to motes that havent reported
            // in max_time_with_no_report time and that had active state
            "update mote set active = false where "
            "(max_time_with_no_report is not null) and "
            "(now() - last_status_report) > max_time_with_no_report "
            "and active = true returning id";

        DB db;
        pqxx::result r = db.exec_sql(sql);
        // report the new state of the motes
        if (r.size() != 0) {
            OutChannel out;

            for (pqxx::result::size_type i = 0; i < r.size(); i++) {
                int id = r[i][0].as<int>();
                std::string msg =
                    "{\"msg\": \"mote_inactive\""s + "," +
                    "\"mote\": '" + std::to_string(id) + "}";
                out.write(id, msg);
            }
        }
        Logger::debug(
            "[MoteStatus] " + std::to_string(r.size()) + " motes deactivated"
        );

    } catch (std::exception& ex) {
        Logger::err("[MoteStatus] check_mote_status: "s + ex.what());
    }

    try {
        set_status_check_timer();
    } catch (std::exception& ex) {
        Logger::err("[MoteStatus] failed to reset timer. No further checks will occur");
    }

#ifdef DEBUG
    auto end = std::chrono::steady_clock::now();
    auto tm = std::chrono::duration<double, std::milli>(end-start).count();
    std::ostringstream oss;
    oss << tm;
    Logger::debug(
        "[MoteStatus] check mote status executed in " + oss.str() + " ms"
    );
#endif
}



void MoteStatus::set_status_check_timer () {
    if (timer == 0)
        throw std::runtime_error(
            "[MoteStatus] not initialized. set_status_check_timer failed"
        );

    timer->cancel(); // cancel any existing wait
    timer->expires_from_now(boost::posix_time::seconds(mote_status_check_period));
    timer->async_wait(&MoteStatus::check_mote_status);
    Logger::log("[MoteStatus] next status report check set");
}
