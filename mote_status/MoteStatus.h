#ifndef __MOTE_STATUS_HEAD__
#define __MOTE_STATUS_HEAD__

#include <boost/any.hpp>
#include <boost/thread/thread.hpp>
#include <boost/asio.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include "util/Conf.h"
#include "util/Logger.h"

#include "db/DB.h"

#include "inter_comm/OutChannel.h"
#include "inter_comm/ErrorOutChannel.h"


namespace MoteStatus {

namespace {
    boost::asio::io_service *io_service = 0;
    boost::asio::deadline_timer *timer = 0;
    int mote_status_check_period;
};

void init ();

/**
 * This function runs periodically to check what motes haven't sent
 * status reports in a certain amount of time. If that time exceeds
 * the maximum that was configured for it then it will be set to the
 * inactive state and clients will be reported.
 */
void check_mote_status (const boost::system::error_code&) noexcept;

/**
 * Sets timer to call check_mote_status
 */
void set_status_check_timer ();

}


#endif // __MOTE_STATUS_HEAD__
