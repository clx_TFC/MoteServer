#ifndef __MISC_H__
#define __MISC_H__

#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <stdexcept>
#include <chrono>
#include <ctime>

#include "Logger.h"


namespace Misc {

// TODO: document functions

std::vector<std::string> split (const std::string, char);
std::string join (std::vector<std::string>, char);
// parse date time in format:
// nr of minutes since 0h00mn 01/01/2018
std::string parse_date_time (std::vector<char>);
// form uint from bytes in array using number
// of bytes indicated
int bytes_to_int (std::vector<char>, int);
// get group of bytes from and integer
std::vector<char> int_to_bytes (int, int);
// form unsigned long form bytes in array using number
// of bytes indicated
unsigned long bytes_to_ulong (std::vector<char>, int);
// return current date and time as a string
std::string curr_date_time_str ();

void print_bytes (const char *, int, int=6);
void print_bytes (std::map<std::string, std::vector<char>>, int=6);

/**
 * Generate a string representation of a byte array
 * @param byte_array
 * @param from index where to start reading byte_array
 * @oaram to index where to stop reading the byte_array (exclusive)
 */
std::string byte_string (const char *, int, int) noexcept;

/**
 * Generate a string representation of a byte array
 * @param byte_array
 * @param from index where to start reading byte_array
 * @oaram to index where to stop reading the byte_array (exclusive)
 */
std::string byte_string (const std::vector<char>, int, int) noexcept;

}

#endif // __MISC_H__
