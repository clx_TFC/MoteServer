#ifndef __CONF_HEAD__
#define __CONF_HEAD__


#include <string>
#include <yaml-cpp/yaml.h>


/**
 * To read current configurations. Singleton class.
 */
class Conf {
private:
    const std::string CONF_FILE_PATH = "./config.yml";
    YAML::Node config;

    static Conf *singleInstance;
    Conf ();

public:
    static Conf *getInstance ();

    // get the value of the field named #item
    YAML::Node get (std::string item);
};


#endif // __CONF_HEAD__
