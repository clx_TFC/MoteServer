#ifndef __LOGGER_HEAD__
#define __LOGGER_HEAD__


#include <iostream>
#include <string>
#include <sstream>
#include <ctime>
#include <iomanip>


#include <mutex>

namespace Logger {

enum Color {
    color_reset  = 0,
    black        = 30,
    red          = 31,
    green        = 32,
    yellow       = 33,
    blue         = 34,
    white        = 37,
    bright_black = 90
};

enum Modifier {
    reset     = 0,
    bold      = 1,
    italic    = 3,
    underline = 4,
    none      = 5
};

namespace { std::mutex log_mutex; }

void err     (std::string, Modifier=bold) noexcept;
void debug   (std::string, Modifier=none) noexcept;
void warning (std::string, Modifier=none) noexcept;
void log     (std::string, Color=white, Modifier=none) noexcept;

}


#endif // __LOGGER_HEAD__
