#include "Conf.h"

Conf *Conf::singleInstance = 0;

Conf::Conf () {
    config = YAML::LoadFile(CONF_FILE_PATH);
}

Conf *Conf::getInstance () {
    if (singleInstance == 0)
        singleInstance = new Conf();
    return singleInstance;
}

YAML::Node Conf::get (std::string item) {
    return config[item];
}
