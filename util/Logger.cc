#include "Logger.h"


void Logger::log (std::string msg, Logger::Color color, Logger::Modifier mod) noexcept {

    auto t = std::time(nullptr);
    auto tm = *std::localtime(&t);

    log_mutex.lock();
    std::cout << "{ " << std::put_time(&tm, "%d-%m-%Y %H:%M:%S") << " } ";

    if (mod != Modifier::none)
        std::cout << "\033[" << mod << "m";

    std::cout << "\033[" << color << "m"
              << msg
              << "\033[" << Color::color_reset << "m";

    if (mod != Modifier::none)
        std::cout << "\033[" << Modifier::reset << "m";

    std::cout << std::endl;

    log_mutex.unlock();
}

void Logger::err (std::string msg, Logger::Modifier mod) noexcept {
    log(msg, Color::red, mod);
}

void Logger::warning (std::string msg, Logger::Modifier mod) noexcept {
    log(msg, Color::yellow, mod);
}

void Logger::debug (std::string msg, Logger::Modifier mod) noexcept {
    log(msg, Color::bright_black, mod);
}
