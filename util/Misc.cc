#include "Misc.h"

std::vector<std::string> Misc::split (const std::string str, char divisor) {
    const int len = str.length();
    std::string nova = "";
    std::vector<std::string> res;

    for (int i = 0; i < len; i++) {
        if (str[i] == divisor) {
            res.push_back(nova);
            nova = "";
            continue;
        }
        else {
            nova += str[i];
        }
    }

    res.push_back(nova);
    return res;
}

std::string Misc::join (std::vector<std::string> strs, char glue) {
    int strs_len = strs.size();
    std::ostringstream oss;

    for (int i = 0; i < strs_len; i++) {
        oss << strs[i];
        if (i != strs_len-1) oss << glue;
    }

    return oss.str();
}

std::string Misc::parse_date_time (std::vector<char> dt) {
    // the date_time is represent in the 5 least
    // significat bytes:
    // bits 0 to 8 represent the year (counting from 2000, e.g. 2015 = 15)
    // bits 8 to 12 represent the month (4 bits)
    // bits 12 to 17 represent the day of the month (5 bits)
    // bits 17 to 22 represent the hour (5 bits)
    // bits 24 to 30 represent the minute (6 bits)
    // bits 32 to 38 (6 bits) the seconds

    ushort year  =  (0xff & dt[0]) + 2000;
    ushort month =   0x0f & dt[1];
    ushort day   = ((0x01 & dt[2]) << 4) + ((0xf0 & dt[1]) >> 4);
    ushort hour  =   0x3e & dt[2];
    ushort mnts  =   0x3f & dt[3];
    ushort secs  =   0x3f & dt[4];

    std::ostringstream oss;
    oss << std::setfill('0');
    oss << std::setw(4) << year  << "-"
        << std::setw(2) << month << "-"
        << std::setw(2) << day   << " "
        << std::setw(2) << hour  << ":"
        << std::setw(2) << mnts  << ":"
        << std::setw(2) << secs;
    Logger::debug("[Misc] parsed date time " + oss.str());
    return oss.str();
}

int Misc::bytes_to_int (std::vector<char> bytes, int nr_bytes) {
    if (bytes.size() < nr_bytes)
        throw std::runtime_error(
            "[Misc#bytes_to_int] byte array does not have enough bytes"
        );
    if (nr_bytes > 4)
        throw std::runtime_error(
            "[Misc#bytes_to_int] nr_bytes should be at most 4"
        );

    int res = 0;
    for (int b = 0; b < nr_bytes; b++)
        res += ((0xff & bytes[b]) << (8*(nr_bytes-b-1)));

    return res;
}

unsigned long Misc::bytes_to_ulong (std::vector<char> bytes, int nr_bytes) {
    if (bytes.size() < nr_bytes)
        throw std::runtime_error(
            "[Misc#bytes_to_ulong] byte array does not have enough bytes"
        );
    if (nr_bytes > 8)
        throw std::runtime_error(
            "[Misc#bytes_to_ulong] nr_bytes should be at most 8"
        );

    unsigned long res = 0;
    for (int b = 0; b < nr_bytes; b++)
        res += ((0x0ffL & bytes[b]) << (8*(nr_bytes-b-1)));

    return res;
}

std::string Misc::curr_date_time_str () {
    auto t   = std::time(nullptr);
    auto tm  = *std::localtime(&t);
    auto now = std::chrono::system_clock::now();
    auto ms  =
        std::chrono::duration_cast<std::chrono::milliseconds>(
            now.time_since_epoch()
        ) % 1000;

    std::ostringstream oss;
    oss << std::put_time(&tm, "%Y-%m-%d %H:%M:%S");
    oss << '.' << std::setfill('0') << std::setw(3) << ms.count();
    return oss.str();
}


std::vector<char> Misc::int_to_bytes (int _int, int nr_bytes) {
    if (nr_bytes > 4)
        throw std::runtime_error(
            "[Misc#int_to_bytes] nr_bytes should be at most 4"
        );

    std::vector<char> bytes(nr_bytes);
    for (int i = 0; i < nr_bytes; i++)
        bytes[i] = 0xff & ((_int) >> (8*(nr_bytes-i-1)));

    return bytes;
}


void Misc::print_bytes (const char *bytes, int len, int row) {
    int bytes_in_row = 0;
    std::ostringstream oss;
    oss << std::hex << std::setfill('0');
    for (int i = 0; i < len; i++) {
        oss << std::setw(2) << (0xff & bytes[i]);
        bytes_in_row++;
        if (bytes_in_row % row == 0) {
            Logger::debug(oss.str());
            oss.str("");
            oss.clear();
        }
    }
    if (oss.str() != "")
        Logger::debug(oss.str());
}


void Misc::print_bytes (std::map<std::string, std::vector<char>> bytes, int row) {
    int bytes_in_row = 0;
    std::ostringstream oss;
    oss << std::hex << std::setfill('0');
    for (auto& s : bytes) {
        std::vector<char> bts = s.second;
        Logger::debug(" * " + s.first + ":");
        for (int i = 0; i < bts.size(); i++) {
            std::cout << std::setw(2) << (0xff & bts[i]);
            bytes_in_row++;
            if (bytes_in_row % row == 0) {
                Logger::debug(oss.str());
                oss.str("");
                oss.clear();
            }
        }
    }
    if (oss.str() != "")
        Logger::debug(oss.str());
}


std::string Misc::byte_string (const char * byte_array, int from, int to) noexcept {
    std::ostringstream oss;
    oss << std::hex << std::setfill('0');
    for (int i = from; i < to; i++) {
        oss << std::setw(2) << (0xff & byte_array[i]);
    }

    return oss.str();
}

std::string Misc::byte_string (const std::vector<char> byte_array, int from, int to) noexcept {
    std::ostringstream oss;
    oss << std::hex << std::setfill('0');
    for (int i = from; i < to; i++) {
        oss << std::setw(2) << (0xff & byte_array[i]);
    }

    return oss.str();
}
