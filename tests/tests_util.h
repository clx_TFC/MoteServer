#include <iostream>
#include <iomanip>
#include <chrono>
#include <cstdlib>
#include <sstream>


namespace tests_util {

std::chrono::steady_clock::time_point start;

void timer_start () { start = std::chrono::steady_clock::now(); }
std::chrono::steady_clock::duration timer_end () {
    return std::chrono::steady_clock::now() - start;
}

void print_bytes (char *buff, int from, int to) {
    std::cout << std::hex << std::setfill('0');
    for (int i = from; i < to; i++) {
        std::cout << std::setw(2) << (0xff & buff[i]);
    }
    std::cout << "\n" << std::dec;
}

}
