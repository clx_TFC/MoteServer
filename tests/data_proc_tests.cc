
#include "sns_data_proc/DataProcessor.h"

#include "db/Parameter.h"
#include "db/SensorDataProcessor.h"
#include "db/Measurement.h"

#include <vector>
#include <cstdlib>
#include <chrono>

#include "tests_util.h"

char rand_char () { return static_cast<char>(rand() % 255); }

void script_processor_test () {
    std::vector<char> bytes = {
        rand_char(), rand_char(), rand_char(), rand_char()
    };
    Tables::SensorDataProcessor proc;
    proc.name = "user_operation";
    proc.script_path = "some_random_py.py";
    Tables::Parameter param_1;
    param_1.name = "param_1";
    param_1.id   = 1;
    std::vector<Tables::Parameter> params = { param_1 };

    std::vector<Tables::Measurement> measurements =
        DataProcessor::process_data(bytes, proc, params);

    for (auto& ms : measurements)
        std::cout << " * measuremet => " << ms.to_string() << "\n";
}

int main (int argc, char **argv) {

    srand(time(NULL));
    int nr_tests = std::stoi(argv[1]);

    for (int i = 0; i < nr_tests; i++) {
        std::cout << "\nTest#" << i << "\n";

        auto start = std::chrono::steady_clock::now();

        script_processor_test();

        auto end = std::chrono::steady_clock::now();
        auto tm = std::chrono::duration<double, std::milli>(end-start).count();
        std::cout << " * " << tm << "ms to execute processing\n";

    }

    return 0;
}
