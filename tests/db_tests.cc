#include "util/Logger.h"
#include "db/DB.h"
#include "db/Cache.h"
#include "db/DBPool.h"

#include <cstdlib>
#include <chrono>

#include <boost/functional/hash.hpp>

int id = 0;

void insert () {
    int c = rand() % 10;
    std::string sql = "insert into test values (" + std::to_string(id++) +
        ", 'random', 'random_val', " + std::to_string(c) +
        ", 13, 'random', 'xpto', 50)";
    DB db;
    db.exec_sql(sql);
}

void insert (DB& db, bool txn=false) {
    int c = rand() % 10;
    std::string sql = "insert into test values (" + std::to_string(id++) +
        ", 'random', 'random_val', " + std::to_string(c) +
        ", 13, 'random', 'xpto', 50)";
    // if (!txn)
        db.exec_sql(sql);
    // else
    //     db.exec_sql_txn(sql);
}

pqxx::result read (std::string where) {
    std::string sql = "select * from test where " + where;
    DB db;
    return db.exec_sql(sql);
}

pqxx::result read (std::string where, DB& db, bool txn=false) {
    std::string sql = "select * from test where " + where;
    if (!txn)
        return db.exec_sql(sql);
    else {
        pqxx::result r;
        db.exec_sql_txn(sql, r);
        return r;
    }
}

void test_cache (int nr_cycles) {

    using namespace std::string_literals;
    char b_str[200];

    for (int i = 0; i < nr_cycles; i++) {
        int nr_insert_cycles = rand() % 5;
        int nr_read_cycles = rand() % 5;

        DB db;

        while (nr_insert_cycles-- > 0) {
            auto start = std::chrono::steady_clock::now();
            int c = rand() % 10;
            std::string sql = "insert into test values (" +
                std::to_string(++id) +
                ", 'random', 'random_val', " + std::to_string(c) +
                ", 13, 'random', 'xpto', 50)";
            db.exec_sql(sql);
            // std::string key = "id="s + std::to_string(id);
            // boost::hash<std::string> string_hasher;
            // Cache::set(string_hasher(key), sql);
            auto end = std::chrono::steady_clock::now();
            auto tm = std::chrono::duration<double, std::milli>(
                end - start
            ).count();
            std::cout << " *I " << tm << "ms insert\n";
        }

        while (nr_read_cycles-- > 0) {
            auto start = std::chrono::steady_clock::now();
            int id = rand() % 100;
            std::string sql = "select * from test where "s +
                " id = " + std::to_string(id);
            boost::hash<std::string> string_hasher;
            std::string res;
            std::string key = "id="s + std::to_string(id);
            if (Cache::get(string_hasher(key), res))
                std::cout << " * Cache hit\n";
            else {
                db.exec_sql(sql);
                std::cout << " * Cache fail\n";
                Cache::set(string_hasher(key), std::string(b_str, 200));
            }
            auto end = std::chrono::steady_clock::now();
            auto tm = std::chrono::duration<double, std::milli>(
                end - start
            ).count();
            std::cout << " *R " << tm << "ms read\n";
        }

    }
}


int main (int argc, char **argv) {

    if (argc == 1) {
        Logger::err("nr_cycles is needed");
        return 1;
    }

    // Cache::init();
    DBPool::init();
    srand(time(NULL));
    int nr_cycles = atoi(argv[1]);
    // test_cache(nr_cycles);

    std::string wheres[] = {
        " 1 = 1 ", " id = 5 "
    };

    for (int i = 0; i < nr_cycles; i++) {
        int nr_insert_cycles = rand() % 20;
        int nr_read_cycles = rand() % 20;
        // int insert_in_txn = rand() % 2;
        // int read_in_txn = rand() % 2;

        Logger::log(
            "Test#" + std::to_string(i+1) + ": " +
            "nr_insert_cycles = " + std::to_string(nr_insert_cycles) + "; "
            "nr_read_cycles = " + std::to_string(nr_read_cycles),
            Logger::Color::blue,
            Logger::Modifier::bold
        );

        // db.init_txn();
        // DB db;
        while (nr_insert_cycles-- > 0) {
            auto start = std::chrono::steady_clock::now();
            insert();
            auto end = std::chrono::steady_clock::now();
            auto tm = std::chrono::duration<double, std::milli>(
                end - start
            ).count();
            std::cout << " *I " << tm << "ms insert\n";
        }
        // db.commit_txn();

        // DB db_r;

        while (nr_read_cycles-- > 0) {
            auto start = std::chrono::steady_clock::now();
            read(wheres[rand() % 2]);
            auto end = std::chrono::steady_clock::now();
            auto tm = std::chrono::duration<double, std::milli>(
                end - start
            ).count();
            std::cout << " *R " << tm << "ms read\n";
        }
    }

    return 0;
}
