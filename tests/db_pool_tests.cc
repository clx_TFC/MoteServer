#include "db/DBPool.h"
#include "util/Logger.h"

#include <cstdlib>
#include <stdexcept>

#include <boost/thread.hpp>
#define BOOST_THREAD_PROVIDES_FUTURE
#include <boost/thread/future.hpp>
#include <boost/optional.hpp>
#include <pqxx/pqxx>


void test_thread () {
    Logger::debug("requested connection");
    boost::promise<DBPool::DBConn *> conn_p;
    boost::future<DBPool::DBConn *>  conn_f = conn_p.get_future();
    DBPool::get(std::move(conn_p));
    Logger::debug("waiting for response...");
    DBPool::DBConn *conn = conn_f.get();
    Logger::debug("receive conn with id " + std::to_string(conn->id));
    sleep(1);
    DBPool::release(conn);
}

int main (int argc, char **argv) {
    DBPool::init();
    int nr_threads = atoi(argv[1]);
    std::vector<boost::thread> threads(nr_threads);
    for (int i = 0; i < nr_threads; i++) {
        threads[i] = boost::thread { test_thread };
    }
    for (int i = 0; i < nr_threads; i++)
        threads[i].join();
}
