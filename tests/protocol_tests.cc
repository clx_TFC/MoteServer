// #include <gtest/gtest.h>
#include <cstdlib>
#include <chrono>
#include <stdexcept>

#include <boost/any.hpp>

#include "tests_util.h"
#include "protocol/Protocol.h"


const int sensor_count = 3;
int sensors[sensor_count] = { 1, 2, 3 };
int sensors_out_sizes[sensor_count] = { 4, 4, 5 };

char rand_char () { return static_cast<char>(rand() % 255); }

void set_mote_id (char *pt, int id) {
    pt[0] = (id >> 8); pt[1] = (id);
}
void set_packet_type (char *pt, int type) { pt[2] = type; }
void set_packet_len (char *pt, int len) {
    pt[3] = (len >> 8); pt[4] = (len);
}

void generate_random_packet (char *pt, size_t packet_len) {
    for (int i = 0; i < packet_len; i++)
        pt[i] = rand_char();
}

size_t generate_ack_packet (char *pt) {
    set_packet_type(pt, 0); // packet_type
    set_packet_len(pt, 0); // packet_len
    return 0;
}

size_t generate_err_packet (char *pt) {
    set_packet_type(pt, 1); // packet_type
    set_packet_len(pt, 6); // packet_len
    // date time
    pt[5] = rand_char();
    pt[6] = rand_char();
    pt[7] = rand_char();
    pt[8] = rand_char();
    pt[9] = rand_char();
    // error code
    pt[10] = rand_char();
    return 6;
}

size_t generate_meas_packet (char *pt, size_t buffer_capacity) {
    set_packet_type(pt, 2);
    // date_time
    pt[5] = rand_char();
    pt[6] = rand_char();
    pt[7] = rand_char();
    pt[8] = rand_char();
    pt[9] = rand_char();
    int pos = 10;
    // measurement values
    std::cout << " * sensors: ";
    for (int i = 0; i < sensor_count; i++) {
        int out_size = sensors_out_sizes[i];
        if (pos + out_size + 1 >= buffer_capacity)
            break;
        pt[pos++] = sensors[i];
        for (int k = 0; k < out_size; k++)
            pt[pos + k] = rand_char();
        pos += out_size;
        std::cout << "(" << sensors[i] << ", " << out_size << ") ";
    }
    std::cout << "\n";
    set_packet_len(pt, pos-5);
    return pos-5;
}

size_t generate_status_report_packet (char *pt) {
    set_packet_type(pt, 3); // packet_type
    set_packet_len(pt, 21); // packet_len
    for (int i = 5; i < 26; i++)
        pt[i] = rand_char();
    return 21;
}

size_t generate_action_result_packet (char *pt) {
    set_packet_type(pt, 4);
    set_packet_len(pt, 3);
    pt[5] = rand_char();
    pt[6] = rand_char();
    pt[7] = rand_char();
    return 3;
}

size_t generate_init_packet (char *pt) {
    set_packet_type(pt, 5);
    set_packet_len(pt, 4);
    pt[5] = rand_char();
    pt[6] = rand_char();
    pt[7] = rand_char();
    pt[8] = rand_char();
    return 4;
}

size_t generate_conf_change_packet (char *pt) {
    return generate_init_packet(pt);
}

void print_packet (Protocol::Packet pt) {
    std::cout << " * packet = {\n";
    for (auto const& mnt : pt.fields) {
        unsigned long val = boost::any_cast<unsigned long>(mnt.second);
        std::cout << "     ." << mnt.first << " = " << val << "\n";
    }
    std::cout << "   }\n";
}

void print_meas_packet (Protocol::Packet pt) {
    auto date_time = boost::any_cast<unsigned long>(
        pt.fields["date_time"]
    );
    std::map<int, unsigned long> measurements =
        boost::any_cast<std::map<int, unsigned long>>(
            pt.fields["measurements"]
        );
    std::cout << " * packet = {\n"
              << "     .date_time = " << std::to_string(date_time) << "\n";
    for (auto const& mnt : measurements) {
        std::cout << "     .sns#" << mnt.first << " = " << mnt.second << "\n";
    }
    std::cout << "   }\n";
}

void print_bytes (char *pt, size_t len) {
    std::cout << " * header = 0x";
    tests_util::print_bytes(pt, 0, 5);
    std::cout << " * payload = 0x";
    tests_util::print_bytes(pt, 5, len);
}

int main (int argc, char **argv) {

    srand(time(NULL));

    int nr_tests = std::stoi(argv[1]);
    int max_packet_len = std::stoi(argv[2]);

    char *pt = static_cast<char*>(malloc(max_packet_len));
    set_mote_id(pt, 1);
    Protocol protocol;


    for (int i = 0; i < nr_tests; i++) {
        int pt_type = 2;
        std::cout << "\nTest#" << i << " with packet of type "
                  << pt_type << ":\n";
        size_t pt_len = -1;
        switch (pt_type) {
            case 0:
                pt_len = generate_ack_packet(pt);
                break;
            case 1:
                pt_len = generate_err_packet(pt);
                break;
            case 2:
                pt_len = generate_meas_packet(pt, max_packet_len);
                break;
            case 3:
                pt_len = generate_status_report_packet(pt);
                break;
            case 4:
                pt_len = generate_init_packet(pt);
                break;
            case 5:
                pt_len = generate_conf_change_packet(pt);
                break;
        }
        if (pt_len == -1) {
            std::cout << " * skipped\n";
            continue;
        }

        pt_len += 5;
        print_bytes(pt, pt_len);
        auto start = std::chrono::steady_clock::now();
        Protocol::Packet packet;
        try {
            packet = protocol.parse_mote_packet(pt, pt_len);
        } catch (std::exception& e) {
            std::cout << "Erro: " << e.what() << "\n";
            continue;
        }
        auto end = std::chrono::steady_clock::now();
        auto tm = std::chrono::duration<double, std::milli>(end-start).count();
        std::cout << " * " << tm << "ms to parse packet\n";
        if (pt_type == 2)
            print_meas_packet(packet);
        else
            print_packet(packet);

    }

    return 0;
}
