#include "sockets/ServerSocket.h"
#include "sockets/Socket.h"

#include "util/Logger.h"
#include "util/Misc.h"


int main (int argc, char **argv) {

    ServerSocket server = ServerSocket("30000");
    while (true) {
        Socket conn = server.receber_conexao();
        Socket::Resposta data = conn.receber_mensagem(512);
        Logger::debug("received data: ");
        Misc::print_bytes(data.resposta, data.tam_resposta);
        conn.fechar();
        data.dispose();
    }

    return 0;
}
