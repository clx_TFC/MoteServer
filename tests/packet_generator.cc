#include <iostream>
#include <iomanip>
#include <vector>
#include <unistd.h>

#include "tests_util.h"
#include "util/Misc.h"
#include "util/Logger.h"

#include "protocol/Protocol.h"

#include "sockets/Socket.h"


std::vector<std::pair<int, char *>> saved_packets;

void save_packet (char *pt, int plen) {
    std::cout << "save packet?[y/n] ";
    char c;
    std::cin >> c;
    if (c == 'y') {
        char *saved = new char[plen];
        memcpy(saved, pt, plen);
        saved_packets.push_back({ plen, saved });
    }
}

char rand_char () { return static_cast<char>(rand() % 255); }

void read_bytes (char *buf, int from, int to) {
    int x;
    for (int i = from; i < to; i++) {
        x = 0;
        std::cout << ">>> ";
        std::cin >> x;
        buf[i] = 0xff & x;
    }
}

char *read_packet (tsl::ordered_map<std::string, int> fields, int type, const int plen, int *ptsize) {
    char *pt = new char[plen + 6];

    std::cout << ">> mote_id (2 bytes)\n";
    read_bytes(pt, 0, 2);
    pt[2] = 0xff & (type);
    pt[3] = 0xff & (plen >> 8);
    pt[4] = 0xff & (plen);
    pt[5] = 0;

    int off = 6;
    for (auto& f : fields) {
        std::cout << ">> " << f.first << " (" << f.second << " bytes)\n";
        read_bytes(pt, off, off + f.second);
        off += f.second;
    }

    Misc::print_bytes(pt, 6 + plen, 6);
    *ptsize = plen + 6;
    return pt;
}

char *read_meas_packets (int *ptsize) {
    return nullptr;
}

void send_packet (char *packet, int plen) {
    try {
        Socket sk = Socket("localhost", "30000");
        sk.enviar_mensagem(packet, plen);
        sk.fechar();
    } catch (std::exception& ex) {
        Logger::err(ex.what());
    }
}

void random_packets () {
    int n;
    std::cout << ">>> ";
    std::cin >> n;
    while (n-- > 0) {
        const int rand_size = rand() % 100;
        char *pt = new char[rand_size];
        for (int i = 0; i < rand_size; i++) pt[i] = rand_char();
        Logger::debug("generated " + std::to_string(rand_size) + " bytes");
        Misc::print_bytes(pt, rand_size, 10);
        send_packet(pt, rand_size);
        delete[] pt;
    }
}

char *custom_packet (int *ptsize) {
    return nullptr;
}


int main (int argc, char **argv) {

    int op;

    while (true) {
        std::cout << "Packet type:\n"
                  << "1.  ACK\n"
                  << "2.  ERROR\n"
                  << "3.  MEASUREMENTS\n"
                  << "4.  STATUS_REPORT\n"
                  << "5.  ACTION_RESULT\n"
                  << "6.  INIT\n"
                  << "7.  CONF_CHANGE\n"
                  << "8.  ALERT\n"
                  << "9.  TRIGGER\n"
                  << "10. random\n"
                  << "11. other\n"
                  << "12. use saved\n"
                  << "> ";
        std::cin >> op;
        char *packet;
        int plen;
        switch (op) {
            case 1:
                packet = read_packet(
                    Protocol::ACK_FIELDS, Protocol::ACK, 0, &plen
                );
                break;
            case 2:
                packet = read_packet(
                    Protocol::ERROR_FIELDS, Protocol::ERROR, 6, &plen
                );
                break;
            case 3:
                packet = read_meas_packets(&plen);
                break;
            case 4:
                packet = read_packet(
                    Protocol::STATUS_REPORT_FIELDS, Protocol::STATUS_REPORT, 22,
                    &plen
                );
                break;
            case 5:
                packet = read_packet(
                    Protocol::ACTION_RESULT_FIELDS, Protocol::ACTION_RESULT, 9,
                    &plen
                );
                break;
            case 6:
                packet = read_packet(
                    Protocol::INIT_FIELDS, Protocol::INIT, 4, &plen
                );
                break;
            case 7:
                packet = read_packet(
                    Protocol::CONF_CHANGE_FIELDS, Protocol::CONF_CHANGE, 4, &plen
                );
                break;
            case 8:
                packet = read_packet(
                    Protocol::ALERT_FIELDS, Protocol::ALERT, 6, &plen
                );
                break;
            case 9:
                packet = read_packet(
                    Protocol::TRIGGER_FIELDS, Protocol::TRIGGER, 10, &plen
                );
                break;
            case 10:
                random_packets();
                continue;
            case 11:
                packet = custom_packet(&plen);
                break;
            case 12:
                int which;
                std::cout << ">> ";
                std::cin >> which;
                packet = saved_packets[which].second;
                plen  = saved_packets[which].first;
                break;
            default:
                std::cout << "Undefined option\n";
                continue;
        }

        try {
            Socket sk = Socket("localhost", "30000");
            sk.enviar_mensagem(packet, plen);
            sk.fechar();
        } catch (std::exception& ex) {
            Logger::err(ex.what());
        }

        save_packet(packet, plen);
        delete[] packet;
    }

    return 0;
}
