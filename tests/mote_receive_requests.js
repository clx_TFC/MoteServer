const Net = require('net')

function init (socket) {
    let packet = [
        0, 1, // mote id
        0,    // packet nr
        6,    // packet type
        0, 4, // payload length
        0, 0, 0, 0
    ]

    socket.write(Buffer.from(packet))
}


const socket = Net.createConnection({ host: 'localhost', port: 30000 }, () => {
    console.log('connection established')
    init(socket)
})
socket.on('data', (data) => {
    console.log('Received: ', data)
    console.log(`With ${Buffer.byteLength(data)} bytes`)
    // socket.end()
})
