#!/bin/python3

import csv
import socket
import math
import time

mote_id   = 1
packet_nr = 0

salinity_param_id     = 5
conductivity_param_id = 6
water_level_param_id  = 9
tds_param_id          = 8

# water quality sensor interface number
wq_interface_num      = 1
# water level sensor interface number
wl_interface_num      = 0

flags                 = 1


def date_time_now () :
    now = time.localtime()
    byte_1 = now.tm_year - 2000
    byte_2 = (now.tm_mon & 0x0f) + ((now.tm_mday << 4) & 0xf0)
    byte_3 = ((now.tm_mday >> 4) & 0x01) + (now.tm_hour & 0x3e)
    byte_4 = (now.tm_min & 0x3f)
    byte_5 = (now.tm_sec & 0x3f)
    print('byte_1 = {}'.format(byte_1))
    print('byte_2 = {}'.format(byte_2))
    print('byte_3 = {}'.format(byte_3))
    print('byte_4 = {}'.format(byte_4))
    print('byte_5 = {}'.format(byte_5))
    return (
        byte_1.to_bytes(1, 'big') +
        byte_2.to_bytes(1, 'big') +
        byte_3.to_bytes(1, 'big') +
        byte_4.to_bytes(1, 'big') +
        byte_5.to_bytes(1, 'big')
    )


def make_packet (data_line) :

    packet = bytearray()
    water_level  = math.floor(float(data_line[2]) * 10000).to_bytes(4, 'big')
    conductivity = math.floor(float(data_line[4]) * 10000).to_bytes(4, 'big')
    salinity     = math.floor(float(data_line[5]) * 10000).to_bytes(4, 'big')
    tds          = math.floor(float(data_line[6]) * 10000).to_bytes(4, 'big')

    header = (
        mote_id.to_bytes(2, 'big')   + # mote id
        packet_nr.to_bytes(1, 'big') + # packet number
        (3).to_bytes(1, 'big')       + # packet type - measurement
        (24).to_bytes(2, 'big')        # packet length in bytes
    )
    # fl = 1 if packet_nr % 10 is 0 else flags
    packet = (
        header +
        flags.to_bytes(1, 'big') +
        date_time_now() +
        wq_interface_num.to_bytes(1, 'big') +
        # in the order needed - salinity -> conductivity -> tds
        salinity + conductivity + tds +
        wl_interface_num.to_bytes(1, 'big') +
        water_level
    )
    return packet


if __name__ == '__main__':
    data_file = csv.reader(open('./measurements.csv'))
    sk = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sk.connect(('localhost', 30000))
    for data_line in data_file:
        packet = make_packet(data_line)
        print('the packet {}'.format(packet))
        sk.sendall(packet)
        ack = sk.recv(512)
        print('received ack {}'.format(str(ack)))
        time.sleep(30)
    sk.close()
