#include "inter_comm/ErrorOutChannel.h"
#include "inter_comm/OutChannel.h"

#include "tests_util.h"

#include "util/Logger.h"

#include <thread>
#include <cstdlib>
#include <chrono>
#include <vector>


void err_channel_test (int nr_cycles, int id) {
    ErrorOutChannel *err_channel = ErrorOutChannel::getInstance();
    std::string _id = std::to_string(id);
    while (nr_cycles-- > 0) {
        err_channel->write("some random error");
        Logger::log("[" + _id + "] " + "wrote msg");
        int t = rand() % 6;
        Logger::log(
            "[" + _id + "] " + "going to sleep for " +
                std::to_string(t) + " seconds"
        );
        std::this_thread::sleep_for(std::chrono::seconds(t));
    }
    Logger::log("[" + _id + "] " + " gone");
}

void out_channel_test (int nr_cycles) {

}


int main (int argc, char **argv) {

    srand(time(NULL));
    std::thread err_thread { ErrorOutChannel::initialize };

    int nr_threads = atoi(argv[1]);
    int nr_cycles  = atoi(argv[2]);

    std::vector<std::thread> test_threads;
    for (int i = 0; i < nr_threads; i++) {
        test_threads.push_back(std::thread(err_channel_test, nr_cycles, i));
    }

    for (auto& t : test_threads) t.join();
    err_thread.join();
    Logger::warning("err_thread gone!!!", Logger::Modifier::bold);
}
