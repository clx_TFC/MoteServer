#include "protocol/Protocol.h"
#include "protocol/Crypto.h"

#include "util/Misc.h"
#include "util/Logger.h"

#include <map>
#include <vector>
#include <iostream>

int ai_len = 11;
std::map<std::string, std::vector<char>> action_instr = {
    { "interface", { (char)0x01                                     } },
    { "action"   , { (char)0x12, (char)0x13, (char)0x54, (char)0x66 } }
};
int sa_len = 10;
std::map<std::string, std::vector<char>> set_alert = {
    { "alert_num", { (char)0x02, (char)0x02, (char)0x02, (char)0x02 } }
};
int ra_len = 10;
std::map<std::string, std::vector<char>> remove_alert = {
    { "alert_num", { (char)0x12, (char)0x41, (char)0xd2, (char)0xff } }
};
int st_len = 10;
std::map<std::string, std::vector<char>> set_trigger = {
    { "trigger_num", { (char)0x56, (char)0x44, (char)0xf6, (char)0x0c } }
};
int rt_len = 10;
std::map<std::string, std::vector<char>> remove_trigger = {
    { "trigger_num", { (char)0xcc, (char)0x40, (char)0x11, (char)0x0f } }
};
int as_len = 7;
std::map<std::string, std::vector<char>> activate_sensor = {
    { "interface", { (char)0xde } }
};
int ds_len = 7;
std::map<std::string, std::vector<char>> deactivate_sensor = {
    { "interface", { (char)0xed } }
};
int aa_len = 7;
std::map<std::string, std::vector<char>> activate_actuator = {
    { "interface", { (char)0x33 } }
};
int da_len = 7;
std::map<std::string, std::vector<char>> deactivate_actuator = {
    { "interface", { (char)0x55 } }
};
int rm_len = 6;
std::map<std::string, std::vector<char>> request_measurements = {};
int rs_len = 6;
std::map<std::string, std::vector<char>> request_status_report = {};
int a_len = 6;
std::map<std::string, std::vector<char>> ack = {};
int na_len = 6;
std::map<std::string, std::vector<char>> nack = {};
int cs_len = 19;
std::map<std::string, std::vector<char>> conf_set = {
    { "measurement_period", { (char)0x31, (char)0xab, (char)0xaa, (char)0xc1 } },
    { "send_data_period"  , { (char)0x12, (char)0x41, (char)0xd2, (char)0x07 } },
    { "report_period"     , { (char)0xb4, (char)0x63, (char)0x99, (char)0x80 } },
    { "comm_encrypt"      , { (char)0x92 } },
};


int main (int argc, char** argv) {

    int id = 213;

    Protocol protocol;

    char* sa = new char[sa_len];
    char* ra = new char[ra_len];
    char* st = new char[st_len];
    char* rt = new char[rt_len];
    char* as = new char[as_len];
    char* ds = new char[ds_len];
    char* aa = new char[aa_len];
    char* da = new char[da_len];
    char* ai = new char[ai_len];
    char* rm = new char[rm_len];
    char* rs = new char[rs_len];
    char* na = new char[na_len];
    char* a  = new char[a_len];
    char* cs = new char[cs_len];

    protocol.create_server_packet(id, Protocol::ALERT_SET, set_alert, sa, sa_len);
    protocol.create_server_packet(id, Protocol::ALERT_REMOVE, remove_alert, ra, ra_len);
    protocol.create_server_packet(id, Protocol::TRIGGER_SET, set_trigger, st, st_len);
    protocol.create_server_packet(id, Protocol::TRIGGER_REMOVE, remove_trigger, rt, rt_len);
    protocol.create_server_packet(id, Protocol::SENSOR_ACTIVATE, activate_sensor, as, as_len);
    protocol.create_server_packet(id, Protocol::SENSOR_DEACTIVATE, deactivate_sensor, ds, ds_len);
    protocol.create_server_packet(id, Protocol::ACTUATOR_ACTIVATE, activate_actuator, aa, aa_len);
    protocol.create_server_packet(id, Protocol::ACTUATOR_DEACTIVATE, deactivate_sensor, da, da_len);
    protocol.create_server_packet(id, Protocol::ACTION_INSTR, action_instr, ai, ai_len);
    protocol.create_server_packet(id, Protocol::REQUEST_STATUS_REPORT, request_status_report, rs, rs_len);
    protocol.create_server_packet(id, Protocol::REQUEST_MEASUREMENTS, request_measurements, rm, rm_len);
    protocol.create_server_packet(id, Protocol::ACK, ack, a, a_len);
    protocol.create_server_packet(id, Protocol::NACK, nack, na, na_len);
    protocol.create_server_packet(id, Protocol::CONF_SET, conf_set, cs, cs_len);


    Logger::log("Set Alert");
    Misc::print_bytes(sa, sa_len);
    Logger::log("Remove Alert");
    Misc::print_bytes(ra, ra_len);
    Logger::log("Set Trigger");
    Misc::print_bytes(st, st_len);
    Logger::log("Remove Trigger");
    Misc::print_bytes(rt, rt_len);
    Logger::log("Activate Sensor");
    Misc::print_bytes(as, as_len);
    Logger::log("Deactivate Sensor");
    Misc::print_bytes(ds, ds_len);
    Logger::log("Activate Trigger");
    Misc::print_bytes(aa, aa_len);
    Logger::log("Deactivate Trigger");
    Misc::print_bytes(da, da_len);
    Logger::log("Action Instruction");
    Misc::print_bytes(ai, ai_len);
    Logger::log("Request Measurements");
    Misc::print_bytes(rm, rm_len);
    Logger::log("Request Status Report");
    Misc::print_bytes(rs, rs_len);
    Logger::log("Nack");
    Misc::print_bytes(na, na_len);
    Logger::log("Ack");
    Misc::print_bytes(a, a_len);
    Logger::log("Set Configuration");
    Misc::print_bytes(cs, cs_len);

    while (true) {
        std::string key;
        std::cout << "Key: ";
        std::getline(std::cin, key);
        try {

            std::string xx = Crypto::aes_ccm_encrypt(sa, sa_len, key);
            std::string yy = Crypto::aes_ccm_decrypt(xx.data(), xx.size(), key);
            assert(sa_len == yy.size());
            assert(std::string(sa, sa_len) == yy);
            xx = Crypto::aes_ccm_encrypt(ra, ra_len, key);
            yy = Crypto::aes_ccm_decrypt(xx.data(), xx.size(), key);
            assert(ra_len == yy.size());
            assert(std::string(ra, ra_len) == yy);

            xx = Crypto::aes_ccm_encrypt(st, st_len, key);
            yy = Crypto::aes_ccm_decrypt(xx.data(), xx.size(), key);
            assert(st_len == yy.size());
            assert(std::string(st, st_len) == yy);
            xx = Crypto::aes_ccm_encrypt(rt, rt_len, key);
            yy = Crypto::aes_ccm_decrypt(xx.data(), xx.size(), key);
            assert(rt_len == yy.size());
            assert(std::string(rt, rt_len) == yy);

            xx = Crypto::aes_ccm_encrypt(as, as_len, key);
            yy = Crypto::aes_ccm_decrypt(xx.data(), xx.size(), key);
            assert(as_len == yy.size());
            assert(std::string(as, as_len) == yy);
            xx = Crypto::aes_ccm_encrypt(ds, ds_len, key);
            yy = Crypto::aes_ccm_decrypt(xx.data(), xx.size(), key);
            assert(ds_len == yy.size());
            assert(std::string(ds, ds_len) == yy);

            xx = Crypto::aes_ccm_encrypt(aa, aa_len, key);
            yy = Crypto::aes_ccm_decrypt(xx.data(), xx.size(), key);
            assert(aa_len == yy.size());
            assert(std::string(aa, aa_len) == yy);
            xx = Crypto::aes_ccm_encrypt(da, da_len, key);
            yy = Crypto::aes_ccm_decrypt(xx.data(), xx.size(), key);
            assert(da_len == yy.size());
            assert(std::string(da, da_len) == yy);

            xx = Crypto::aes_ccm_encrypt(rm, rm_len, key);
            yy = Crypto::aes_ccm_decrypt(xx.data(), xx.size(), key);
            assert(rm_len == yy.size());
            assert(std::string(rm, rm_len) == yy);

            xx = Crypto::aes_ccm_encrypt(rs, rs_len, key);
            yy = Crypto::aes_ccm_decrypt(xx.data(), xx.size(), key);
            assert(rs_len == yy.size());
            assert(std::string(rs, rs_len) == yy);

            xx = Crypto::aes_ccm_encrypt(ai, ai_len, key);
            yy = Crypto::aes_ccm_decrypt(xx.data(), xx.size(), key);
            assert(ai_len == yy.size());
            assert(std::string(ai, ai_len) == yy);

            xx = Crypto::aes_ccm_encrypt(a, a_len, key);
            yy = Crypto::aes_ccm_decrypt(xx.data(), xx.size(), key);
            assert(a_len == yy.size());
            assert(std::string(a, a_len) == yy);
            xx = Crypto::aes_ccm_encrypt(na, na_len, key);
            yy = Crypto::aes_ccm_decrypt(xx.data(), xx.size(), key);
            assert(na_len == yy.size());
            assert(std::string(na, na_len) == yy);

            xx = Crypto::aes_ccm_encrypt(cs, cs_len, key);
            yy = Crypto::aes_ccm_decrypt(xx.data(), xx.size(), key);
            assert(cs_len == yy.size());
            assert(std::string(cs, cs_len) == yy);

        } catch (std::exception& err) {
            Logger::err(std::string(err.what()));
        }
    }

}
