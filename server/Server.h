#ifndef __OURICO_SERVER_HEAD__
#define __OURICO_SERVER_HEAD__

#include <map>

#include <evpp/tcp_server.h>
#include <evpp/buffer.h>
#include <evpp/slice.h>
#include <evpp/any.h>
#include <evpp/tcp_conn.h>

#include <db/DB.h>
#include <db/Packet.h>

#include "util/Conf.h"
#include "util/Misc.h"
#include "util/Logger.h"

#include "protocol/Protocol.h"
#include "protocol/Crypto.h"

#include "server/Handlers.h"
#include "server/ServerExcept.h"

#include "inter_comm/MoteMessageRequests.h"


namespace Server {


namespace {
    std::map<int, std::shared_ptr<evpp::TCPConn>> connections;
}


/**
 * Called when a new message is received
 */
void on_message (const evpp::TCPConnPtr&, evpp::Buffer*);

/**
 * Called when a new connection is established or an existing connection
 * is destroyed
 */
void on_connection (const evpp::TCPConnPtr&);


/**
 * Send a message to a currently connected mote
 *
 * @param mote_id the id of the mote
 * @param packet the message to send
 * @param packet_len length of message in bytes
 */
void send_message_to_mote (int mote_id, const char* packet, size_t packet_len);


/**
 * Start the server
 */
void init ();


/**
 * Utility functions to send ack and nack messages
 */
void ack  (const evpp::TCPConnPtr&, const Protocol::Packet&) noexcept;
void nack (const evpp::TCPConnPtr&, const Protocol::Packet&) noexcept;
void nack (const evpp::TCPConnPtr&) noexcept;
void save_packet (const Protocol::Packet&) noexcept;

};


#endif // __OURICO_SERVER_HEAD__
