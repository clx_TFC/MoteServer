#ifndef __SERVERCORE_HEAD__
#define __SERVERCORE_HEAD__


#include "protocol/Crypto.h"
#include "protocol/Protocol.h"

#include "db/DB.h"
#include "db/MoteError.h"
#include "db/ActionResult.h"
#include "db/Measurement.h"
#include "db/Parameter.h"
#include "db/AlertOccurrence.h"
#include "db/TriggerOccurrence.h"
#include "db/StatusReport.h"
#include "db/Packet.h"

#include "util/Conf.h"
#include "util/Logger.h"
#include "util/Misc.h"

#include "inter_comm/OutChannel.h"
#include "inter_comm/ErrorOutChannel.h"

#include "ServerExcept.h"

#include <stdexcept>
#include <string>
#include <thread>
#include <vector>
#include <tuple>
#include <chrono>

#include <yaml-cpp/yaml.h>


namespace Handlers {

bool handle_init_mote (Protocol::Packet&, const char*, size_t, std::string*) noexcept;

void handle_disconnect (int);

bool handle_response (Protocol::Packet&, const char*, size_t, bool) noexcept;

bool handle_error            (Protocol::Packet&, const char*, size_t) noexcept;
bool handle_measurements     (Protocol::Packet&, const char*, size_t) noexcept;
bool handle_action_result    (Protocol::Packet&, const char*, size_t) noexcept;
bool handle_status_report    (Protocol::Packet&, const char*, size_t) noexcept;
bool handle_change_mote_conf (Protocol::Packet&, const char*, size_t) noexcept;
bool handle_alert            (Protocol::Packet&, const char*, size_t) noexcept;
bool handle_trigger          (Protocol::Packet&, const char*, size_t) noexcept;


bool handle_sensor_change_state   (Protocol::Packet&, const char*, size_t, bool) noexcept;
bool handle_actuator_change_state (Protocol::Packet&, const char*, size_t, bool) noexcept;
bool handle_alert_change_state    (Protocol::Packet&, const char*, size_t, bool) noexcept;
bool handle_trigger_change_state  (Protocol::Packet&, const char*, size_t, bool) noexcept;
bool handle_reset                 (Protocol::Packet&, const char*, size_t) noexcept;
bool handle_conf_set              (Protocol::Packet&, const char*, size_t) noexcept;
bool handle_action_exec           (Protocol::Packet&, const char*, size_t) noexcept;


};


#endif // __SERVERCORE_HEAD__
