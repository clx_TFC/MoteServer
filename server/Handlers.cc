#include "server/Handlers.h"


void store_measurements(
    DB&,
    std::vector<std::tuple<int, int, std::vector<char>>>,
    int,
    std::string
);



bool Handlers::handle_response
(Protocol::Packet& packet, const char* raw_packet, size_t packet_len, bool ack) noexcept {
    using namespace std::string_literals;

    try {

        OutChannel out;
        std::string msg =
            "{\"msg\": "s + (ack ? "\"ack\"" : "\"nack\"") + "," +
            "\"mote_id\": " + std::to_string(packet.mote_id) + "}";
        out.write(packet.mote_id, msg);

        return true;

    } catch (ServerExcept& ex) {

        Logger::err(
            "[Handlers] Failed to report mote response to operation: "s + ex.what()
        );

        std::string pt = Misc::byte_string(raw_packet, 0, packet_len);
        std::string msg = "{\"mote_id\": " + std::to_string(packet.mote_id) + "," +
            "\"msg\": \"packet_error\"," +
            "\"packet\": \"" + pt + "\"," +
            "\"err\": \"" + ex.what() + "\"}";
        ErrorOutChannel::write(packet.mote_id, msg);

        return false;

    } catch (std::exception& ex) {

        Logger::err(
            "[Handlers] Failed to report mote response to operation: "s + ex.what()
        );

        std::string pt = Misc::byte_string(raw_packet, 0, packet_len);
        std::string msg = "{\"mote_id\": " + std::to_string(packet.mote_id) + "," +
            "\"msg\": \"packet_error\"," +
            "\"packet\": \"" + pt + "\"," +
            "\"err\": \"Falha a processar pacote\"}";
        ErrorOutChannel::write(packet.mote_id, msg);

        return false;
    }
}



/**
 * Called when a connection with a mote is terminated
 */
void Handlers::handle_disconnect (int mote_id) {
    using namespace std::string_literals;

    try {
        std::string sql =
            "update mote set active = false where id = " + std::to_string(mote_id);

        DB db;
        db.exec_sql(sql);
        OutChannel out;
        std::string msg =
            "{\"msg\": \"mote_inactive\""s + "," +
            "\"mote_id\": " + std::to_string(mote_id) + "}";
        out.write(mote_id, msg);

#ifdef DEBUG
        Logger::debug("[Handlers] mote " + std::to_string(mote_id) + " deactivated");
#endif

    } catch (std::exception& ex) {
        Logger::err(
            "[Handlers] Failed to set mote deactivated state: "s + ex.what()
        );
    }
}



/**
 * Handle Protocol::PacketType::ERROR messages
 */
bool Handlers::handle_error
(Protocol::Packet& packet, const char *raw_packet, size_t packet_len) noexcept {
    using namespace std::string_literals;

    try {

        /// store error to database
        Tables::MoteError mote_error;
        std::vector<char> err_code =
            boost::any_cast<std::vector<char>>(packet.fields["err_code"]);
        mote_error.mote_id = packet.mote_id;
        mote_error.date_time = Misc::parse_date_time(
            boost::any_cast<std::vector<char>>(packet.fields["date_time"])
        );
        mote_error.err_code = 0xff & err_code[0];
        mote_error.err_msg  = "to define";

        DB db;
        db.exec_sql(mote_error.write_sql());

        /// report message to application server
        OutChannel out;
        std::string msg =
            "{\"msg\": \"mote_error,\""s
            "\"mote_id\": " + std::to_string(packet.mote_id) + "," +
            "\"err_code\": " + std::to_string(mote_error.err_code) + "," +
            "\"err_msg\": \"" + mote_error.err_msg + "\"," +
            "\"date_time\": \"" + mote_error.date_time + "\"}";
        out.write(packet.mote_id, msg);

        return true;

    } catch (ServerExcept& ex) {

        Logger::err(
            "[Handlers] Failed to report mote error: "s + ex.what()
        );

        std::string pt = Misc::byte_string(raw_packet, 0, packet_len);
        std::string msg = "{\"mote_id\": " + std::to_string(packet.mote_id) + "," +
            "\"msg\": \"packet_error\"," +
            "\"packet\": \"" + pt + "\"," +
            "\"err\": \"" + ex.what() + "\"}";
        ErrorOutChannel::write(packet.mote_id, msg);

        return false;

    } catch (std::exception& ex) {

        Logger::err(
            "[Handlers] Failed to report mote error: "s + ex.what()
        );

        std::string pt = Misc::byte_string(raw_packet, 0, packet_len);
        std::string msg = "{\"mote_id\": " + std::to_string(packet.mote_id) + "," +
            "\"msg\": \"packet_error\"," +
            "\"packet\": \"" + pt + "\"," +
            "\"err\": \"Falha a processar pacote\"}";
        ErrorOutChannel::write(packet.mote_id, msg);

        return false;
    }
}


/**
 * Handle Protocol::PacketType::MEASUREMENTS messages
 */
bool Handlers::handle_measurements
(Protocol::Packet& packet, const char* raw_packet, size_t packet_len) noexcept {
    using namespace std::string_literals;


    try {

        std::vector<std::tuple<int, int, std::vector<char>>> measurements =
            boost::any_cast<std::vector<std::tuple<int, int, std::vector<char>>>>(
                packet.fields["measurements"]
            );

        if (measurements.size() == 0)
            throw ServerExcept("Pacote sem medições");

        int flags = Misc::bytes_to_int(
            boost::any_cast<std::vector<char>>(packet.fields["flags"]),
            Protocol::MEASUREMENTS_FIELDS.at("flags")
        );

        std::string date_time = Misc::parse_date_time(
            boost::any_cast<std::vector<char>>(
                packet.fields["date_time"]
            )
        );

        DB db;
        db.init_txn();
        store_measurements(db, measurements, packet.mote_id, date_time);
        db.commit_txn();


        // notify application server about async measurements
        if (flags & Protocol::MEASUREMENTS_PACKET_FLAGS.at("async")) {
            OutChannel out;
            std::string msg =
                "{\"msg\": \"measurements\""s + "," +
                "\"mote_id\": " + std::to_string(packet.mote_id) + "," +
                "\"date_time\": \"" + date_time + "\"}";
            out.write(packet.mote_id, msg);
        }

        return true;

    } catch (ServerExcept& ex) {

        Logger::err(
            "[Handlers] store measurements failed: "s + ex.what()
        );

        std::string pt = Misc::byte_string(raw_packet, 0, packet_len);
        std::string msg = "{\"mote_id\": " + std::to_string(packet.mote_id) + "," +
            "\"msg\": \"packet_error\"," +
            "\"packet\": \"" + pt + "\"," +
            "\"err\": \"" + ex.what() + "\"}";
        ErrorOutChannel::write(packet.mote_id, msg);

        return false;

    } catch (std::exception& err) {

        Logger::err(
            "[Handlers] store measurements failed: "s + err.what()
        );

        std::string pt = Misc::byte_string(raw_packet, 0, packet_len);
        std::string msg = "{\"mote_id\": " + std::to_string(packet.mote_id) + "," +
            "\"msg\": \"packet_error\"," +
            "\"packet\": \"" + pt + "\"," +
            "\"err\": \"Falha a processar pacote\"}";
        ErrorOutChannel::write(packet.mote_id, msg);

        return false;
    }
}


/**
 * Handle Protocol::PacketType::ACTION_RESULT messages
 */
bool Handlers::handle_action_result
(Protocol::Packet& packet, const char* raw_packet, size_t packet_len) noexcept {
    using namespace std::string_literals;

    try {

        DB db;

        Tables::ActionResult action_res;
        action_res.mote = packet.mote_id;

        int interface = Misc::bytes_to_int(
            boost::any_cast<std::vector<char>>(packet.fields["interface"]),
            Protocol::ACTION_INSTR_FIELDS.at("interface")
        );
        // get the actuator_use associated with
        // this interface
        pqxx::result r = db.exec_sql(
            "select num, actuator_id from actuator_use "s +
            " where interface_num = " + std::to_string(interface)
        );

        if (r.size() == 0)
            throw ServerExcept(
                "Não foi encontrado nenhum atuador na interface "s +
                std::to_string(interface) + " do mote " +
                std::to_string(packet.mote_id)
            );

        if (r.size() != 1)
            Logger::warning(
                "[Handlers] More than one actuator found in interface "s +
                std::to_string(interface) + " of mote " +
                std::to_string(packet.mote_id)
            );

        action_res.actuator         = r[0][0].as<int>();
        action_res.actuator_use_num = r[0][1].as<int>();

        action_res.action_num = Misc::bytes_to_int(
            boost::any_cast<std::vector<char>>(packet.fields["action_num"]),
            Protocol::ACTION_INSTR_FIELDS.at("action_num")
        );
        action_res.result = Misc::bytes_to_int(
            boost::any_cast<std::vector<char>>(packet.fields["result"]),
            Protocol::ACTION_INSTR_FIELDS.at("result")
        );
        action_res.date_time = Misc::parse_date_time(
            boost::any_cast<std::vector<char>>(packet.fields["date_time"])
        );

        db.exec_sql(action_res.write_sql());

        // report action result to application server
        OutChannel out;
        // create msg in yaml format
        std::string msg =
            "{\"msg\": \"action_result\""s + "," +
            "\"mote_id\": " + std::to_string(action_res.mote) + "," +
            "\"actuator_id\": " + std::to_string(action_res.actuator) + "," +
            "\"actuator_use_num\": " +
                std::to_string(action_res.actuator_use_num) + "," +
            "\"action_num\": " + std::to_string(action_res.action_num) + "," +
            "\"date_time\": \"" + action_res.date_time + "\"}";
        out.write(packet.mote_id, msg);

        return true;

    } catch (ServerExcept& ex) {

        Logger::err(
            "[Handlers] Failed to report mote action result: "s + ex.what()
        );

        std::string pt = Misc::byte_string(raw_packet, 0, packet_len);
        std::string msg = "{\"mote_id\": " + std::to_string(packet.mote_id) + "," +
            "\"msg\": \"packet_error\"," +
            "\"packet\": \"" + pt + "\"," +
            "\"err\": \"" + ex.what() + "\"}";
        ErrorOutChannel::write(packet.mote_id, msg);

        return false;

    } catch (std::exception& ex) {

        Logger::err(
            "[Handlers] Failed to report mote action result: "s + ex.what()
        );

        std::string pt = Misc::byte_string(raw_packet, 0, packet_len);
        std::string msg = "\"mote_id\": " + std::to_string(packet.mote_id) + "," +
            "\"msg\": \"packet_error\"," +
            "\"packet\": \"" + pt + "\"," +
            "\"err\": \"Falha a processar pacote\"}";
        ErrorOutChannel::write(packet.mote_id, msg);

        return false;
    }

}


/**
 * Handle Protocol::PacketType::INIT messages
 */
bool Handlers::handle_init_mote (
    Protocol::Packet& packet,
    const char* raw_packet,
    size_t packet_len,
    std::string* conf_packet
) noexcept {

    using namespace std::string_literals;

    try {

        std::vector<char> ip_bytes = boost::any_cast<std::vector<char>>(
            packet.fields.at("current_ip")
        );
        std::string ip = std::to_string(0xff & ip_bytes[0]) + "." +
            std::to_string(0xff & ip_bytes[1]) + "." +
            std::to_string(0xff & ip_bytes[2]) + "." +
            std::to_string(0xff & ip_bytes[3]);

        std::string update_state_sql = "update mote set active = true, "s +
            "current_ip = '" + ip + "' where id = " +
            std::to_string(packet.mote_id);

        std::string get_conf_sql =
            "select measurement_period, send_data_period, "s +
            "status_report_period, use_crypto, name " +
            "from mote_configuration " +
            "where active = true and mote_id = " + std::to_string(packet.mote_id);

        DB db;
        pqxx::result r = db.exec_sql(get_conf_sql);
        if (r.size() == 0) {

            Logger::warning(
                "[Handlers] no configuration found for mote " +
                std::to_string(packet.mote_id)
            );

            throw ServerExcept(
                "Não foi encontrada nenhuma configuração para o mote " +
                std::to_string(packet.mote_id)
            );
        }

        if (r.size() != 1)
            Logger::warning(
                "[Protocol] More than one active configuration found for "s +
                "mote " + std::to_string(packet.mote_id)
            );

        std::map<std::string, std::vector<char>> packet_data;
        packet_data["measurement_period"] = Misc::int_to_bytes(
            r[0][0].as<int>(), Protocol::CONF_SET_FIELDS.at("measurement_period")
        );
        packet_data["send_data_period"] = Misc::int_to_bytes(
            r[0][1].as<int>(), Protocol::CONF_SET_FIELDS.at("send_data_period")
        );
        packet_data["report_period"] = Misc::int_to_bytes(
            r[0][2].as<int>(), Protocol::CONF_SET_FIELDS.at("report_period")
        );
        packet_data["comm_encrypt"] = Misc::int_to_bytes(
            r[0][3].as<bool>() ? 1 : 0, Protocol::CONF_SET_FIELDS.at("comm_encrypt")
        );


        Protocol pt;
        try {
            *conf_packet = pt.create_server_packet(
                packet.mote_id,
                Protocol::PacketType::CONF_SET,
                packet_data
            );
        } catch (std::exception& ex) {
            Logger::warning(
                "[Handlers] failed to create new configuration packet: "s + ex.what()
            );
            throw ex;
        }

        db.exec_sql(update_state_sql);
        OutChannel out;
        std::string msg =
            "{\"msg\": \"mote_active\""s + "," +
            "\"mote_id\": " + std::to_string(packet.mote_id) + "}";
        out.write(packet.mote_id, msg);

        Logger::log(
            "[Handlers] mote init'ed  with configuration " + r[0][4].as<std::string>()
        );

        return true;

    } catch (ServerExcept& ex) {

        Logger::err(
            "[Handlers] init mote failed: "s + ex.what()
        );

        std::string pt = Misc::byte_string(raw_packet, 0, packet_len);
        std::string msg = "{\"mote_id\": " + std::to_string(packet.mote_id) + "," +
            "\"msg\": \"packet_error\"," +
            "\"packet\": \"" + pt + "\"," +
            "\"err\": \"" + ex.what() + "\"}";
        ErrorOutChannel::write(packet.mote_id, msg);

        return false;

    } catch (std::exception& ex) {

        Logger::err(
            "[Handlers] init mote failed: "s + ex.what()
        );

        std::string pt = Misc::byte_string(raw_packet, 0, packet_len);
        std::string msg = "{\"mote_id\": " + std::to_string(packet.mote_id) + "," +
            "\"msg\": \"packet_error\"," +
            "\"packet\": \"" + pt + "\"," +
            "\"err\": \"Falha a processar pacote\"}";
        ErrorOutChannel::write(packet.mote_id, msg);

        return false;
    }
}


/**
 * Handle Protocol::PacketType::STATUS_REPORT messages
 */
bool Handlers::handle_status_report
(Protocol::Packet& packet, const char* raw_packet, size_t packet_len) noexcept {
    using namespace std::string_literals;

    try {

        Tables::StatusReport sr;
        sr.start_date_time = Misc::parse_date_time(
            boost::any_cast<std::vector<char>>(
                packet.fields.at("start_date_time")
            )
        );
        sr.end_date_time = Misc::parse_date_time(
            boost::any_cast<std::vector<char>>(
                packet.fields.at("end_date_time")
            )
        );
        sr.mote = packet.mote_id;
        sr.nr_tx_packets = Misc::bytes_to_int(
            boost::any_cast<std::vector<char>>(packet.fields.at("nr_tx_packets")),
            Protocol::STATUS_REPORT_FIELDS.at("nr_tx_packets")
        );
        sr.nr_lost_packets = Misc::bytes_to_int(
            boost::any_cast<std::vector<char>>(packet.fields.at("nr_lost_packets")),
            Protocol::STATUS_REPORT_FIELDS.at("nr_lost_packets")
        );
        sr.nr_tx_bytes = Misc::bytes_to_int(
            boost::any_cast<std::vector<char>>(packet.fields.at("nr_tx_bytes")),
            Protocol::STATUS_REPORT_FIELDS.at("nr_tx_bytes")
        );
        sr.nr_lost_bytes = Misc::bytes_to_int(
            boost::any_cast<std::vector<char>>(packet.fields.at("nr_lost_bytes")),
            Protocol::STATUS_REPORT_FIELDS.at("nr_lost_bytes")
        );

        DB db;
        db.init_txn()
          // update last report time
          .exec_sql_txn(
            "update mote set last_status_report = '" +
            Misc::curr_date_time_str() + "' where id = " + std::to_string(packet.mote_id)
          )
          .exec_sql_txn(sr.write_sql()) // save status report
          .commit_txn();

        OutChannel out;
        std::string msg =
            "{\"msg\": \"status_report\""s + "," +
            "\"mote_id\": " + std::to_string(packet.mote_id) + "," +
            "\"start_date_time\": \"" + sr.start_date_time + "\"," +
            "\"end_date_time\": \"" + sr.end_date_time + "\"}";
        out.write(packet.mote_id, msg);

        return true;

    } catch (ServerExcept& ex) {
        Logger::err(
            "[Handlers] store status report failed: "s + ex.what()
        );

        std::string pt = Misc::byte_string(raw_packet, 0, packet_len);
        std::string msg = "{\"mote_id\": " + std::to_string(packet.mote_id) + "," +
            "\"msg\": \"packet_error\"," +
            "\"packet\": \"" + pt + "\"," +
            "\"err\": \"" + ex.what() + "\"}";
        ErrorOutChannel::write(packet.mote_id, msg);

        return false;

    } catch (std::exception& ex) {

        Logger::err(
            "[Handlers] store status report failed: "s + ex.what()
        );

        std::string pt = Misc::byte_string(raw_packet, 0, packet_len);
        std::string msg = "{\"mote_id\": " + std::to_string(packet.mote_id) + "," +
            "\"msg\": \"packet_error\"," +
            "\"packet\": \"" + pt + "\"," +
            "\"err\": \"Falha a processar pacote\"}";
        ErrorOutChannel::write(packet.mote_id, msg);

        return false;
    }
}


/**
 * Handle Protocol::PacketType::CONF_CHANGE messages
 */
bool Handlers::handle_change_mote_conf
(Protocol::Packet& packet, const char* raw_packet, size_t packet_len) noexcept {
    using namespace std::string_literals;

    try {

        std::vector<char> ip_bytes = boost::any_cast<std::vector<char>>(
            packet.fields.at("current_ip")
        );
        std::string ip = std::to_string(0xff & ip_bytes[0]) + "." +
            std::to_string(0xff & ip_bytes[1]) + "." +
            std::to_string(0xff & ip_bytes[2]) + "." +
            std::to_string(0xff & ip_bytes[3]);

        std::string sql = "update mote set active = true, "s +
            "current_ip = '" + ip + "' where id = " +
            std::to_string(packet.mote_id);

        DB db;
        db.exec_sql(sql);

        return true;

    } catch (ServerExcept& ex) {

        Logger::err(
            "[Handlers] change mote configuration failed: "s + ex.what()
        );

        std::string pt = Misc::byte_string(raw_packet, 0, packet_len);
        std::string msg = "{\"mote_id\": " + std::to_string(packet.mote_id) + "," +
            "\"msg\": \"packet_error\"," +
            "\"packet\": \"" + pt + "\"," +
            "\"err\": \"" + ex.what() + "\"}";
        ErrorOutChannel::write(packet.mote_id, msg);

        return false;

    } catch (std::exception& ex) {

        Logger::err(
            "[Handlers] change mote configuration failed: "s + ex.what()
        );

        std::string pt = Misc::byte_string(raw_packet, 0, packet_len);
        std::string msg = "{\"mote_id\": " + std::to_string(packet.mote_id) + "," +
            "\"msg\": \"packet_error\"," +
            "\"packet\": \"" + pt + "\"," +
            "\"err\": \"Falha a processar pacote\"}";
        ErrorOutChannel::write(packet.mote_id, msg);

        return false;
    }

}


/**
 * Handle Protocol::PacketType::ALERT messages
 */
bool Handlers::handle_alert
(Protocol::Packet& packet, const char* raw_packet, size_t packet_len) noexcept {

    using namespace std::string_literals;

    try {

        std::vector<std::tuple<int, int, std::vector<char>>> measurements =
            boost::any_cast<std::vector<std::tuple<int, int, std::vector<char>>>>(
                packet.fields["measurements"]
            );

        if (measurements.size() == 0)
            throw ServerExcept("Pacote sem medições");


        int alert_num = Misc::bytes_to_int(
            boost::any_cast<std::vector<char>>(packet.fields.at("alert_num")),
            Protocol::ALERT_FIELDS.at("alert_num")
        );

        std::string date_time = Misc::parse_date_time(
            boost::any_cast<std::vector<char>>(
                packet.fields["date_time"]
            )
        );
        Tables::AlertOccurrence oc;
        oc.alert_num = alert_num;
        oc.mote = packet.mote_id;
        oc.date_time = date_time;


        DB db;
        db.init_txn();
        store_measurements(db, measurements, packet.mote_id, date_time);
        db.exec_sql_txn(oc.write_sql())
          .commit_txn();

        // report alert to application server
        OutChannel out;
        std::string msg =
            "{\"msg\": \"alert\""s + "," +
            "\"mote_id\": " + std::to_string(packet.mote_id) + "," +
            "\"date_time\": \"" + date_time + "\","
            "\"alert_id\": " + std::to_string(alert_num) + "}";
        out.write(packet.mote_id, msg);

        return true;

    } catch (ServerExcept& ex) {

        Logger::err(
            "[Handlers] report alert failed: "s + ex.what()
        );

        std::string pt = Misc::byte_string(raw_packet, 0, packet_len);
        std::string msg = "{\"mote_id\": " + std::to_string(packet.mote_id) + "," +
            "\"msg\": \"packet_error\"," +
            "\"packet\": \"" + pt + "\"," +
            "\"err\": \"" + ex.what() + "\"}";
        ErrorOutChannel::write(packet.mote_id, msg);

        return false;

    } catch (std::exception& err) {

        Logger::err(
            "[Handlers] report alert failed: "s + err.what()
        );

        std::string pt = Misc::byte_string(raw_packet, 0, packet_len);
        std::string msg = "{\"mote_id\": " + std::to_string(packet.mote_id) + "," +
            "\"msg\": \"packet_error\"," +
            "\"packet\": \"" + pt + "\"," +
            "\"err\": \"Falha a processar pacote\"}";
        ErrorOutChannel::write(packet.mote_id, msg);

        return false;
    }
}


/**
 * Handle Protocol::PacketType::TRIGGER messages
 */
bool Handlers::handle_trigger
(Protocol::Packet& packet, const char* raw_packet, size_t packet_len) noexcept {

    using namespace std::string_literals;

    try {

        DB db;

        std::vector<std::tuple<int, int, std::vector<char>>> measurements =
            boost::any_cast<std::vector<std::tuple<int, int, std::vector<char>>>>(
                packet.fields["measurements"]
            );

        if (measurements.size() == 0)
            throw ServerExcept("Pacote sem medições");

        // to store the action result of the trigger
        Tables::ActionResult action_res;
        action_res.mote = packet.mote_id;

        int interface = Misc::bytes_to_int(
            boost::any_cast<std::vector<char>>(packet.fields["interface"]),
            Protocol::TRIGGER_FIELDS.at("interface")
        );
        // get the actuator_use associated with
        // this interface
        pqxx::result r = db.exec_sql(
            "select num, actuator from actuator_use "s +
            " where interface_num = " + std::to_string(interface)
        );

        if (r.size() == 0)
            throw ServerExcept(
                "Não foi encontrado nenhum atuador na interface "s +
                std::to_string(interface) + " do mote " +
                std::to_string(packet.mote_id)
            );

        if (r.size() != 1)
            Logger::warning(
                "[Handlers] More than one actuator found in interface "s +
                std::to_string(interface) + " of mote " +
                std::to_string(packet.mote_id)
            );

        action_res.actuator         = r[0][0].as<int>();
        action_res.actuator_use_num = r[0][1].as<int>();

        action_res.action_num = Misc::bytes_to_int(
            boost::any_cast<std::vector<char>>(packet.fields["action_num"]),
            Protocol::ACTION_INSTR_FIELDS.at("action_num")
        );
        action_res.result = Misc::bytes_to_int(
            boost::any_cast<std::vector<char>>(packet.fields["result"]),
            Protocol::ACTION_INSTR_FIELDS.at("result")
        );
        action_res.date_time = Misc::parse_date_time(
            boost::any_cast<std::vector<char>>(packet.fields["date_time"])
        );

        Tables::TriggerOccurrence to;
        to.trigger_num = Misc::bytes_to_int(
            boost::any_cast<std::vector<char>>(packet.fields["trigger_num"]),
            Protocol::ACTION_INSTR_FIELDS.at("trigger_num")
        );
        to.mote = packet.mote_id;
        to.date_time = action_res.date_time;

        db.init_txn();
        store_measurements(db, measurements, packet.mote_id, action_res.date_time);
        db.exec_sql_txn(to.write_sql())
          .exec_sql_txn(action_res.write_sql())
          .commit_txn();

        // report action result to application server
        OutChannel out;
        // create msg in yaml format
        std::string msg =
            "{\"msg\": \"trigger\""s + "," +
            "\"mote_id\": " + std::to_string(action_res.mote) + "," +
            "\"actuator_id\": " + std::to_string(action_res.actuator) + "," +
	        "\"actuator_use_num\": " + std::to_string(action_res.actuator_use_num) + "," +
            "\"action_num\": " + std::to_string(action_res.action_num) + "," +
            "\"date_time\": \"" + action_res.date_time + "\"}";
        out.write(packet.mote_id, msg);

        return true;

    } catch (ServerExcept& err) {

        Logger::err(
            "[Handlers] report trigger failed: "s + err.what()
        );

        std::string pt = Misc::byte_string(raw_packet, 0, packet_len);
        std::string msg = "{\"mote_id\": " + std::to_string(packet.mote_id) + "," +
            "\"msg\": \"packet_error\"," +
            "\"packet\": \"" + pt + "\"," +
            "\"err\": " + err.what() + "}";
        ErrorOutChannel::write(packet.mote_id, msg);

        return false;

    } catch (std::exception& err) {

        Logger::err(
            "[Handlers] report trigger failed: "s + err.what()
        );

        std::string pt = Misc::byte_string(raw_packet, 0, packet_len);
        std::string msg = "{\"mote_id\": " + std::to_string(packet.mote_id) + "," +
            "\"msg\": \"packet_error\"," +
            "\"packet\": \"" + pt + "\"," +
            "\"err\": \"Falha a processar pacote\"}";
        ErrorOutChannel::write(packet.mote_id, msg);

        return false;
    }
}



/**
 * Handle Protocol::PacketType::SENSOR_ACTIVATE and
 * Protocol::PacketType::SENSOR_DEACTIVATE
 * Those messages are sent from the mote to confirm that it deactivated/activated
 * a sensor
 */
bool Handlers::handle_sensor_change_state
(Protocol::Packet& packet, const char* raw_packet, size_t packet_len, bool activated) noexcept {

    using namespace std::string_literals;

    try {

        int interface_num = Misc::bytes_to_int(
            boost::any_cast<std::vector<char>>(packet.fields.at("interface")),
            Protocol::SENSOR_ACTIVATE_FIELDS.at("interface")
        );
        DB db;
        std::string sql =
            "update sensor_use set active = "s + (activated ? "true" : "false") +
            " where interface_num = " + std::to_string(interface_num) +
            " and mote_id = " + std::to_string(packet.mote_id);
        db.exec_sql(sql);

        OutChannel out;
        std::string msg =
            "{\"msg\": \"sensor_change_state\", "s +
            "\"mote_id\": " + std::to_string(packet.mote_id) + ", " +
            "\"interface_num\": " + std::to_string(interface_num) + ", " +
            "\"active\": " + (activated ? "true" : "false") + "}";
        out.write(packet.mote_id, msg);

        return true;

    } catch (ServerExcept& err) {

        Logger::err(
            "[Handlers] change sensor state failed: "s + err.what()
        );

        std::string pt = Misc::byte_string(raw_packet, 0, packet_len);
        std::string msg = "{\"mote_id\": " + std::to_string(packet.mote_id) + "," +
            "\"msg\": \"packet_error\"," +
            "\"packet\": \"" + pt + "\"," +
            "\"err\": " + err.what() + "}";
        ErrorOutChannel::write(packet.mote_id, msg);

        return false;

    } catch (std::exception& err) {

        Logger::err(
            "[Handlers] change sensor state failed: "s + err.what()
        );

        std::string pt = Misc::byte_string(raw_packet, 0, packet_len);
        std::string msg = "{\"mote_id\": " + std::to_string(packet.mote_id) + "," +
            "\"msg\": \"packet_error\"," +
            "\"packet\": \"" + pt + "\"," +
            "\"err\": \"Falha a processar pacote\"}";
        ErrorOutChannel::write(packet.mote_id, msg);

        return false;
    }
}


/**
 * Handle Protocol::PacketType::ACTUATOR_ACTIVATE and
 * Protocol::PacketType::ACTUATOR_DEACTIVATE
 * Those messages are sent from the mote to confirm that it deactivated/activated
 * a actuator
 */
bool Handlers::handle_actuator_change_state
(Protocol::Packet& packet, const char* raw_packet, size_t packet_len, bool activated) noexcept {

    using namespace std::string_literals;

    try {

        int interface_num = Misc::bytes_to_int(
            boost::any_cast<std::vector<char>>(packet.fields.at("interface")),
            Protocol::ACTUATOR_ACTIVATE_FIELDS.at("interface")
        );
        DB db;
        std::string sql =
            "update actuator_use set active = "s + (activated ? "true" : "false") +
            " where interface_num = " + std::to_string(interface_num) +
            " and mote_id = " + std::to_string(packet.mote_id);
        db.exec_sql(sql);

        OutChannel out;
        std::string msg =
            "{\"msg\": \"actuator_change_state\", "s +
            "\"mote_id\": " + std::to_string(packet.mote_id) + ", " +
            "\"interface_num\": " + std::to_string(interface_num) + ", " +
            "\"active\":" + (activated ? "true" : "false") + "}";
        out.write(packet.mote_id, msg);

        return true;

    } catch (ServerExcept& err) {

        Logger::err(
            "[Handlers] change actuator state failed: "s + err.what()
        );

        std::string pt = Misc::byte_string(raw_packet, 0, packet_len);
        std::string msg = "{\"mote_id\": " + std::to_string(packet.mote_id) + "," +
            "\"msg\": \"packet_error\"," +
            "\"packet\": \"" + pt + "\"," +
            "\"err\": " + err.what() + "}";
        ErrorOutChannel::write(packet.mote_id, msg);

        return false;

    } catch (std::exception& err) {

        Logger::err(
            "[Handlers] change actuator state failed: "s + err.what()
        );

        std::string pt = Misc::byte_string(raw_packet, 0, packet_len);
        std::string msg = "{\"mote_id\": " + std::to_string(packet.mote_id) + "," +
            "\"msg\": \"packet_error\"," +
            "\"packet\": \"" + pt + "\"," +
            "\"err\": \"Falha a processar pacote\"}";
        ErrorOutChannel::write(packet.mote_id, msg);

        return false;
    }
}


bool Handlers::handle_alert_change_state
(Protocol::Packet& packet, const char* raw_packet, size_t packet_len, bool activated) noexcept {

    using namespace std::string_literals;

    try {

        int alert_id = Misc::bytes_to_int(
            boost::any_cast<std::vector<char>>(packet.fields.at("alert_num")),
            Protocol::ALERT_SET_FIELDS.at("alert_num")
        );
        DB db;
        std::string sql =
            "update alert_use set active = "s + (activated ? "true" : "false") +
            " where alert_id = " + std::to_string(alert_id) +
            " and mote_id = " + std::to_string(packet.mote_id);
        db.exec_sql(sql);

        OutChannel out;
        std::string msg =
            "{\"msg\": \"alert_change_state\", "s +
            "\"mote_id\": " + std::to_string(packet.mote_id) + ", " +
            "\"alert_id\": " + std::to_string(alert_id) + ", " +
            "\"active\":" + (activated ? "true" : "false") + "}";
        out.write(packet.mote_id, msg);

        return true;

    } catch (ServerExcept& err) {

        Logger::err(
            "[Handlers] change alert state failed: "s + err.what()
        );

        std::string pt = Misc::byte_string(raw_packet, 0, packet_len);
        std::string msg = "{\"mote_id\": " + std::to_string(packet.mote_id) + "," +
            "\"msg\": \"packet_error\"," +
            "\"packet\": \"" + pt + "\"," +
            "\"err\": " + err.what() + "}";
        ErrorOutChannel::write(packet.mote_id, msg);

        return false;

    } catch (std::exception& err) {

        Logger::err(
            "[Handlers] change alert state failed: "s + err.what()
        );

        std::string pt = Misc::byte_string(raw_packet, 0, packet_len);
        std::string msg = "{\"mote_id\": " + std::to_string(packet.mote_id) + "," +
            "\"msg\": \"packet_error\"," +
            "\"packet\": \"" + pt + "\"," +
            "\"err\": \"Falha a processar pacote\"}";
        ErrorOutChannel::write(packet.mote_id, msg);

        return false;
    }
}


bool Handlers::handle_trigger_change_state
(Protocol::Packet& packet, const char* raw_packet, size_t packet_len, bool activated) noexcept {

    using namespace std::string_literals;

    try {

        int trigger_id = Misc::bytes_to_int(
            boost::any_cast<std::vector<char>>(packet.fields.at("trigger_num")),
            Protocol::TRIGGER_SET_FIELDS.at("trigger_num")
        );
        DB db;
        std::string sql =
            "update trigger_use set active = "s + (activated ? "true" : "false") +
            " where trigger_id = " + std::to_string(trigger_id) +
            " and mote_id = " + std::to_string(packet.mote_id);
        db.exec_sql(sql);

        OutChannel out;
        std::string msg =
            "{\"msg\": \"trigger_change_state\", "s +
            "\"mote_id\": " + std::to_string(packet.mote_id) + ", " +
            "\"trigger_id\": " + std::to_string(trigger_id) + ", " +
            "\"active\":" + (activated ? "true" : "false") + "}";
        out.write(packet.mote_id, msg);

        return true;

    } catch (ServerExcept& err) {

        Logger::err(
            "[Handlers] change trigger state failed: "s + err.what()
        );

        std::string pt = Misc::byte_string(raw_packet, 0, packet_len);
        std::string msg = "{\"mote_id\": " + std::to_string(packet.mote_id) + "," +
            "\"msg\": \"packet_error\"," +
            "\"packet\": \"" + pt + "\"," +
            "\"err\": " + err.what() + "}";
        ErrorOutChannel::write(packet.mote_id, msg);

        return false;

    } catch (std::exception& err) {

        Logger::err(
            "[Handlers] change trigger state failed: "s + err.what()
        );

        std::string pt = Misc::byte_string(raw_packet, 0, packet_len);
        std::string msg = "{\"mote_id\": " + std::to_string(packet.mote_id) + "," +
            "\"msg\": \"packet_error\"," +
            "\"packet\": \"" + pt + "\"," +
            "\"err\": \"Falha a processar pacote\"}";
        ErrorOutChannel::write(packet.mote_id, msg);

        return false;
    }
}


bool Handlers::handle_reset
(Protocol::Packet& packet, const char* raw_packet, size_t packet_len) noexcept {

    using namespace std::string_literals;

    try {

        OutChannel out;
        std::string msg =
            "{\"msg\": \"reset\", "s +
            "\"mote_id\": " + std::to_string(packet.mote_id) + "}";
        out.write(packet.mote_id, msg);

        return true;

    } catch (ServerExcept& err) {

        Logger::err(
            "[Handlers] handle reset failed: "s + err.what()
        );

        std::string pt = Misc::byte_string(raw_packet, 0, packet_len);
        std::string msg = "{\"mote_id\": " + std::to_string(packet.mote_id) + "," +
            "\"msg\": \"packet_error\"," +
            "\"packet\": \"" + pt + "\"," +
            "\"err\": " + err.what() + "}";
        ErrorOutChannel::write(packet.mote_id, msg);

        return false;

    } catch (std::exception& err) {

        Logger::err(
            "[Handlers] handle reset failed: "s + err.what()
        );

        std::string pt = Misc::byte_string(raw_packet, 0, packet_len);
        std::string msg = "{\"mote_id\": " + std::to_string(packet.mote_id) + "," +
            "\"msg\": \"packet_error\"," +
            "\"packet\": \"" + pt + "\"," +
            "\"err\": \"Falha a processar pacote\"}";
        ErrorOutChannel::write(packet.mote_id, msg);

        return false;
    }
}


bool Handlers::handle_conf_set
(Protocol::Packet& packet, const char* raw_packet, size_t packet_len) noexcept {

    using namespace std::string_literals;

    try {

        int measurement_period = Misc::bytes_to_int(
            boost::any_cast<std::vector<char>>(packet.fields.at("measurement_period")),
            Protocol::CONF_SET_FIELDS.at("measurement_period")
        );
        int send_data_period = Misc::bytes_to_int(
            boost::any_cast<std::vector<char>>(packet.fields.at("send_data_period")),
            Protocol::CONF_SET_FIELDS.at("send_data_period")
        );
        int report_period = Misc::bytes_to_int(
            boost::any_cast<std::vector<char>>(packet.fields.at("report_period")),
            Protocol::CONF_SET_FIELDS.at("report_period")
        );
        int comm_encrypt = Misc::bytes_to_int(
            boost::any_cast<std::vector<char>>(packet.fields.at("comm_encrypt")),
            Protocol::CONF_SET_FIELDS.at("comm_encrypt")
        );


        DB db;
        std::string sql =
            // TODO: XXX: activate configuration by ID!!!!
            "update mote_configuration set active = true "s +
            " where measurement_period = " + std::to_string(measurement_period) +
            " and send_data_period = " + std::to_string(send_data_period) +
            " and status_report_period = " + std::to_string(report_period) +
            " and use_crypto = " + (comm_encrypt == 1 ? "true" : "false") +
            " and mote_id = " + std::to_string(packet.mote_id);
        db.exec_sql(sql);

        OutChannel out;
        std::string msg =
            "{\"msg\": \"conf_set\", "s +
            "\"mote_id\": " + std::to_string(packet.mote_id) + "}";
        out.write(packet.mote_id, msg);

        return true;

    } catch (ServerExcept& err) {

        Logger::err(
            "[Handlers] handle response to set configuration failed: "s + err.what()
        );

        std::string pt = Misc::byte_string(raw_packet, 0, packet_len);
        std::string msg = "{\"mote_id\": " + std::to_string(packet.mote_id) + "," +
            "\"msg\": \"packet_error\"," +
            "\"packet\": \"" + pt + "\"," +
            "\"err\": " + err.what() + "}";
        ErrorOutChannel::write(packet.mote_id, msg);

        return false;

    } catch (std::exception& err) {

        Logger::err(
            "[Handlers] handle response to set configuration failed: "s + err.what()
        );

        std::string pt = Misc::byte_string(raw_packet, 0, packet_len);
        std::string msg = "{\"mote_id\": " + std::to_string(packet.mote_id) + "," +
            "\"msg\": \"packet_error\"," +
            "\"packet\": \"" + pt + "\"," +
            "\"err\": \"Falha a processar pacote\"}";
        ErrorOutChannel::write(packet.mote_id, msg);

        return false;
    }
}


bool Handlers::handle_action_exec
(Protocol::Packet& packet, const char* raw_packet, size_t packet_len) noexcept {

    using namespace std::string_literals;

    try {

        int interface_num = Misc::bytes_to_int(
            boost::any_cast<std::vector<char>>(packet.fields.at("interface")),
            Protocol::ACTION_INSTR_FIELDS.at("interface")
        );
        int action_num = Misc::bytes_to_int(
            boost::any_cast<std::vector<char>>(packet.fields.at("action")),
            Protocol::ACTION_INSTR_FIELDS.at("action")
        );

        OutChannel out;
        std::string msg =
            "{\"msg\": \"action_instr\", "s +
            "\"interface_num\": " + std::to_string(interface_num) + ", " +
            "\"action_num\": " + std::to_string(action_num) + ", " +
            "\"mote_id\": " + std::to_string(packet.mote_id) + "}";
        out.write(packet.mote_id, msg);

        return true;

    } catch (ServerExcept& err) {

        Logger::err(
            "[Handlers] handle reset failed: "s + err.what()
        );

        std::string pt = Misc::byte_string(raw_packet, 0, packet_len);
        std::string msg = "{\"mote_id\": " + std::to_string(packet.mote_id) + "," +
            "\"msg\": \"packet_error\"," +
            "\"packet\": \"" + pt + "\"," +
            "\"err\": " + err.what() + "}";
        ErrorOutChannel::write(packet.mote_id, msg);

        return false;

    } catch (std::exception& err) {

        Logger::err(
            "[Handlers] handle reset failed: "s + err.what()
        );

        std::string pt = Misc::byte_string(raw_packet, 0, packet_len);
        std::string msg = "{\"mote_id\": " + std::to_string(packet.mote_id) + "," +
            "\"msg\": \"packet_error\"," +
            "\"packet\": \"" + pt + "\"," +
            "\"err\": \"Falha a processar pacote\"}";
        ErrorOutChannel::write(packet.mote_id, msg);

        return false;
    }
}



// process measurement data and store results
void store_measurements (
    DB& db,
    std::vector<std::tuple<int, int, std::vector<char>>> measurements,
    int mote_id,
    std::string date_time
) {

    using namespace std::string_literals;

    pqxx::result r;

    std::string get_params = "get_params";
    std::string get_params_sql =
        // get the parameters associated with this sensor
        "select p.id, p.size from "s +
        "(sensor as sns join parameter as p " +
        "on sns.id = p.sensor_id) where sns.id = $1 order by position asc";
    db.prepare(get_params, get_params_sql);

    // // open the mote's measurements file
    // Hdfs::File meas_file = Hdfs::open_mote_file(mote_id, Hdfs::FileName::MEASUREMENTS);

    for (auto const& ms : measurements) {

        int sensor_use_num      = std::get<0>(ms);
        int sensor_id           = std::get<1>(ms);
        std::vector<char> bytes = std::get<2>(ms);

        db.exec_prepared_txn(get_params, r, sensor_id);
        if (r.size() == 0)
            throw ServerExcept(
                "Não foram encontrados parâmetros do sensor "s +
                std::to_string(sensor_id) + " do mote " +
                std::to_string(mote_id)
            );

        // save measurements
        int curr_idx = 0;
        for (pqxx::result::size_type i = 0; i < r.size(); i++) {
            int param_size     = r[i][1].as<int>();
            Tables::Measurement meas;
            meas.param = r[i][0].as<int>(); // param id
            // add the byte sequence, in hex, corresponding to this parameter
            meas.value          =
                Misc::byte_string(bytes, curr_idx, curr_idx + param_size);
            curr_idx           += param_size;
            meas.sensor_use_num = sensor_use_num;
            meas.sensor         = sensor_id;
            meas.mote           = mote_id;
            meas.date_time      = date_time;
            db.exec_sql_txn(meas.write_sql());
            // Hdfs::append_to_file(meas_file, meas.as_csv_line());
        }
    }

    // Hdfs::flush_file(meas_file);
    // Hdfs::close_mote_file(meas_file);
}
