#ifndef __SERVER_EXCEPT_HEAD__
#define __SERVER_EXCEPT_HEAD__

#include <stdexcept>

class ServerExcept : public std::exception {
private:
    std::string err_msg;
public:
    ServerExcept (std::string em) : err_msg(em) {};
    char const *what () const throw() {
        return err_msg.c_str();
    }
};


#endif // __SERVER_EXCEPT_HEAD__
