#include "server/Server.h"



void Server::init () {
    Conf *config = Conf::getInstance();
    auto server_port = config->get("server_port");
    auto server_thread_num = config->get("server_thread_num");
    if (server_port.IsNull() || server_thread_num.IsNull())
        throw std::runtime_error(
            "[Server] Server core configurations are not set.\n"
            "Fields server_port and server_thread_num must be set in configuration file."
        );

    auto port       = server_port.as<std::string>();
    auto thread_num = server_thread_num.as<int>();

    evpp::EventLoop loop;
    evpp::TCPServer server(&loop, "0.0.0.0:" + port, "MoteServer", thread_num);
    server.SetConnectionCallback(&Server::on_connection);
    server.SetMessageCallback(&Server::on_message);

    Logger::log("[Server] being started in port " + port, Logger::Color::blue);

    server.Init();
    server.Start();
    loop.Run();
}


void Server::on_message (const evpp::TCPConnPtr& connection, evpp::Buffer* buffer) {
    Logger::debug("[Server] received a new message");

    evpp::Slice slice = buffer->NextAll();
    const char *data = slice.data();
    size_t data_len  = slice.size();
    Misc::print_bytes(data, data_len);

    Protocol::Packet packet;

    try {
        Protocol protocol;
        packet = protocol.parse_mote_packet(data, data_len);
    } catch (std::exception& err) {
        using namespace std::string_literals;
        Logger::err("[Server] Failed to parse packet: "s + err.what());
        nack(connection);
        return;
    }

    bool status_ok = false;
    bool respond   = true;
    std::string new_conf;

    switch (packet.type) {

    case Protocol::ACK:
        respond = false;
        break;

    case Protocol::NACK:
        Handlers::handle_response(packet, data, data_len, false);
        respond = false;
        break;

    case Protocol::ERROR:
        status_ok = Handlers::handle_error(packet, data, data_len);
        respond = false;
        break;

    case Protocol::MEASUREMENTS:
        status_ok = Handlers::handle_measurements(packet, data, data_len);
        break;

    case Protocol::STATUS_REPORT:
        status_ok = Handlers::handle_status_report(packet, data, data_len);
        break;

    case Protocol::ACTION_RESULT:
        status_ok = Handlers::handle_action_result(packet, data, data_len);
        break;

    case Protocol::INIT:
        status_ok = Handlers::handle_init_mote(packet, data, data_len, &new_conf);
        if (status_ok) {
            // send the initial configuration for the mote
            connection->Send(new_conf.data(), new_conf.size());

            MoteMessageRequests::mote_connected(packet.mote_id);
            connection->set_context(evpp::Any(packet.mote_id));
            connections.insert(std::pair<int, const evpp::TCPConnPtr&>(
                packet.mote_id, std::shared_ptr<evpp::TCPConn>(connection)
            ));
            Logger::log(
                "[Server] stored connection from mote " + std::to_string(packet.mote_id)
            );

        } else {
            nack(connection, packet);
            Logger::log(
                "[Server] connection from mote " + std::to_string(packet.mote_id) + " not stored"
            );
        }
        break;

    case Protocol::CONF_CHANGE:
        status_ok = Handlers::handle_change_mote_conf(packet, data, data_len);
        break;

    case Protocol::ALERT:
        status_ok = Handlers::handle_alert(packet, data, data_len);
        break;

    case Protocol::TRIGGER:
        status_ok = Handlers::handle_trigger(packet, data, data_len);
        break;

    case Protocol::ALERT_SET: case Protocol::ALERT_REMOVE:
        Handlers::handle_alert_change_state(
            packet, data, data_len, packet.type == Protocol::ALERT_SET
        );
        respond = false;
        break;

    case Protocol::TRIGGER_SET: case Protocol::TRIGGER_REMOVE:
        Handlers::handle_trigger_change_state(
            packet, data, data_len, packet.type == Protocol::TRIGGER_SET
        );
        respond = false;
        break;

    case Protocol::ACTUATOR_ACTIVATE: case Protocol::ACTUATOR_DEACTIVATE:
        Handlers::handle_actuator_change_state(
            packet, data, data_len, packet.type == Protocol::ACTUATOR_ACTIVATE
        );
        respond = false;
        break;

    case Protocol::SENSOR_ACTIVATE: case Protocol::SENSOR_DEACTIVATE:
        Handlers::handle_sensor_change_state(
            packet, data, data_len, packet.type == Protocol::SENSOR_ACTIVATE
        );
        respond = false;
        break;

    case Protocol::ACTION_INSTR:
        Handlers::handle_action_exec(packet, data, data_len);
        respond = false;
        break;

    case Protocol::RESET:
        Handlers::handle_reset(packet, data, data_len);
        respond = false;
        break;

    case Protocol::CONF_SET:
        Handlers::handle_conf_set(packet, data, data_len);
        respond = false;
        break;

    case Protocol::REQUEST_MEASUREMENTS: case Protocol::REQUEST_STATUS_REPORT:
        respond = false;
        break;

    default:
        Logger::err(
            "[ServerCore] Unexpected packet type " + std::to_string(packet.type)
        );
        break;
    }

    if (packet.type != Protocol::PacketType::INIT && respond) {
        auto entry = connections.find(packet.mote_id);
        if (entry == connections.end()) {
            Logger::warning(
                "[Server] connection from mote " + std::to_string(packet.mote_id) +
                " not found in store"
            );

            // if (status_ok) {
            //     ack(connection, packet);
            // } else {
            //     nack(connection, packet);
            // }

        } else {
            std::shared_ptr<evpp::TCPConn> conn = entry->second;
            Logger::log(
                "[Server] got connection of mote " + std::to_string(packet.mote_id) +
                " from store"
            );

            // if (status_ok) {
            //     ack(conn, packet);
            // } else {
            //     nack(conn, packet);
            // }
        }
    }

    save_packet(packet);
}


void Server::on_connection (const evpp::TCPConnPtr& connection) {
    if (connection->IsConnected()) {
        Logger::log("[Server] received a new connection");
        connection->set_context(evpp::Any(-1));
    } else {
        Logger::log("[Server] connection being closed");
        int mote_id = evpp::any_cast<int>(connection->context());
        if (mote_id != -1) {
            MoteMessageRequests::mote_disconnected(mote_id);
            if (connections.erase(mote_id) != 0) {
                Logger::log("[Server] connection removed from store");
            } else {
                Logger::warning("[Server] connection should be in store, but was not");
            }
            Handlers::handle_disconnect(mote_id);
        } else {
            Logger::warning("[Server] connection was not init'ed");
        }
    }
}


/**
 * Send a Protocol::PacketType::ACK packet to a mote
 */
void Server::ack (const evpp::TCPConnPtr& connection, const Protocol::Packet& pct) noexcept {
    try {
        Protocol pt;
        std::string packet = pt.create_server_ack(pct.mote_id, pct.nr);
        connection->Send(packet.data(), packet.size());
#ifdef DEBUG
        Logger::debug("[Server] sent ack for packet no. " + std::to_string(pct.nr));
#endif
    } catch (std::exception& ex) {
        Logger::warning(
            "[Server] failed to send ACK " +  std::to_string(pct.nr) +
            " to mote " + std::to_string(pct.mote_id)
        );
    }
}


/**
 * Send a Protocol::PacketType::NACK packet to a mote
 */
void Server::nack (const evpp::TCPConnPtr& connection, const Protocol::Packet& pct) noexcept {
    try {
        Protocol pt;
        std::string packet = pt.create_server_nack(pct.mote_id, pct.nr);
        connection->Send(packet.data(), packet.size());
#ifdef DEBUG
        Logger::debug("[Server] sent NACK for packet no." + std::to_string(pct.nr));
#endif
    } catch (std::exception& ex) {
        Logger::warning(
            "[Server] failed to send nack " +  std::to_string(pct.nr) +
            " to mote " + std::to_string(pct.mote_id)
        );
    }
}


/**
 * Send a Protocol::PacketType::NACK without knowing mote id of packet number
 */
void Server::nack (const evpp::TCPConnPtr& connection) noexcept {
    try {
        Protocol pt;
        std::string packet = pt.create_server_nack(-1, 0);
        connection->Send(packet.data(), packet.size());
#ifdef DEBUG
        Logger::debug("[Server] sent NACK");
#endif

    } catch (std::exception& ex) {
        Logger::warning("[Server] failed to send nack");
    }
}


void Server::send_message_to_mote (int mote_id, const char* packet, size_t packet_len) {
    std::shared_ptr<evpp::TCPConn> connection = connections[mote_id];
    if (connection == nullptr) {
        Handlers::handle_disconnect(mote_id);
        throw ServerExcept("Mote is not currently connected");
    } else if (!connection->IsConnected()) {
        Handlers::handle_disconnect(mote_id);
        connections.erase(mote_id);
        throw ServerExcept("Mote is not currently connected");
    }
    connection->Send(packet, packet_len);
}



void Server::save_packet (const Protocol::Packet& packet) noexcept {
    try {
        DB db;
        Tables::Packet new_packet;
        new_packet.mote_id     = packet.mote_id;
        new_packet.date_time   = Misc::curr_date_time_str();
        new_packet.length      = packet.payload_len + Protocol::MOTE_PACKET_HEAD_LEN;
        new_packet.packet_type = packet.type;
        new_packet.packet_nr   = packet.nr;
        db.exec_sql(new_packet.write_sql());
        Logger::log(
            "[ServerCore] Packet (" + new_packet.date_time + ", " +
            std::to_string(new_packet.mote_id) + ") logged to DB"
        );
    } catch (std::exception& ex) {
        Logger::err("[ServerCore] Failed to log packet to DB");
    }
}
